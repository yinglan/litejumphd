//
//  ChooseScene.m
//  FatJumper
//
//  Created by in-blue  on 10-10-12.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ChooseScene.h"


@implementation ChooseScene

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	ChooseScene *layer = [ChooseScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	
	
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) 
	{
		int num = [[NSUserDefaults standardUserDefaults] integerForKey: @"totalNum"];
		[self initWithNum:num];	
	
	}
	return self;
}


- (id)initWithNum:(int)num
{
	
	m_num = num;
	

	if (isPad)
	{
		CCSprite *haha = [CCSprite spriteWithFile:@"123456.png"];
		[self addChild:haha z:11];
		[haha setPosition:ccp(384,512)];
	}
	
	p_bg = [CCSprite spriteWithFile:@"choose_bg_board.png"];
	[self addChild:p_bg z:1];
	[p_bg setPosition:convertPoint(ccp(161,240))];
	

	p_bg_front_1 = [CCSprite spriteWithFile:@"choose_front_board_black.png"];
	[p_bg addChild:p_bg_front_1 z:-4];
	[p_bg_front_1 setPosition:ccp(157,332)];
	if (isPad)
	{
		[p_bg_front_1 setPosition:ccp(314,664)];
	}
	
	p_bg_front_2 = [CCSprite spriteWithFile:@"choose_front_board_red.png"];
	[p_bg addChild:p_bg_front_2 z:-1];
	[p_bg_front_2 setPosition:ccp(157.5,225.5)];
	if (isPad)
	{
		[p_bg_front_2 setPosition:ccp(315,451)];
	}
	
	p_bg_front_3 = [CCSprite spriteWithFile:@"black_board.png"];
	[p_bg addChild:p_bg_front_3 z:-1];
	[p_bg_front_3 setPosition:ccp(157.5,225.5)];
	if (isPad)
	{
		[p_bg_front_3 setPosition:ccp(315,451)];
	}
	
	m_player_bg[0].p_bg= [CCSprite spriteWithFile:@"choose_bg_darkgreen_bubble.png"];
	[p_bg addChild:m_player_bg[0].p_bg z:-3];
	[m_player_bg[0].p_bg setPosition:ccp(161,108)];
	if (isPad)
	{
		[m_player_bg[0].p_bg setPosition:ccp(322,216)];
	}
	
	m_player_bg[2].p_bg= [CCSprite spriteWithFile:@"choose_bg_yellow_bubble.png"];
	[p_bg addChild:m_player_bg[2].p_bg z:-3];
	[m_player_bg[2].p_bg setPosition:ccp(161,108)];
	if (isPad)
	{
		[m_player_bg[2].p_bg setPosition:ccp(322,216)];
	}
	
	
	m_player_bg[3].p_bg= [CCSprite spriteWithFile:@"choose_bg_lightgreen_bubble.png"];
	[p_bg addChild:m_player_bg[3].p_bg z:-3];
	[m_player_bg[3].p_bg setPosition:ccp(161,108)];
	if (isPad)
	{
		[m_player_bg[3].p_bg setPosition:ccp(322,216)];
	}
	
	
	m_player_bg[1].p_bg= [CCSprite spriteWithFile:@"choose_bg_green_bubble.png"];
	[p_bg addChild:m_player_bg[1].p_bg z:-3];
	[m_player_bg[1].p_bg setPosition:ccp(161,108)];//309
	if (isPad)
	{
		[m_player_bg[1].p_bg setPosition:ccp(322,216)];
	}
	
	m_player[0].p_player = [CCSprite spriteWithFile:@"normalman.png"];
	[p_bg addChild:m_player[0].p_player z:-2];
	[m_player[0].p_player setPosition:ccp(161,99)];//329
	if (isPad)
	{
		[m_player[0].p_player setPosition:ccp(322,198)];
	}
	
	
	m_player[2].p_player = [CCSprite spriteWithFile:@"rocker.png"];
	[p_bg addChild:m_player[2].p_player z:-2];
	[m_player[2].p_player setPosition:ccp(161,99)];//329
	if (isPad)
	{
		[m_player[2].p_player setPosition:ccp(322,198)];
	}
	
	m_player[1].p_player = [CCSprite spriteWithFile:@"oldwoman.png"];
	[p_bg addChild:m_player[1].p_player z:-2];
	[m_player[1].p_player setPosition:ccp(161,99)];
	if (isPad)
	{
		[m_player[1].p_player setPosition:ccp(322,198)];
	}
	
	m_player[3].p_player = [CCSprite spriteWithFile:@"doc.png"];
	[p_bg addChild:m_player[3].p_player z:-2];
	[m_player[3].p_player setPosition:ccp(161,99)];//329
	if (isPad)
	{
		[m_player[3].p_player setPosition:ccp(322,198)];
	}
	
	p_select_icon = [CCSprite spriteWithFile:@"select_icon.png"];
	[p_bg addChild:p_select_icon z:2];
	[p_select_icon setPosition:ccp(105,140)];//	
	if(isPad)
	[p_select_icon setPosition:ccp(210,280)];
	
	p_lock_icon = [CCSprite spriteWithFile:@"lock_icon.png"];
	[p_bg addChild:p_lock_icon z:2];
	[p_lock_icon setPosition:ccp(105,145)];//	
	if(isPad)
		[p_lock_icon setPosition:ccp(210,280)];
	
	choose = [CCSprite spriteWithFile:@"player_selected.png"];
	[p_bg addChild:choose z:-1];
	[choose setPosition:ccp(155,225)];
	
	if(isPad)
		[choose setPosition:ccp(310,450)];
//	
	lock = [CCSprite spriteWithFile:@"player_unlock.png"];
	[p_bg addChild:lock z:-1];
	[lock setPosition:ccp(155,225)];
	if(isPad)
		[lock setPosition:ccp(310,450)];

//	


	
	p_green_board = [CCSprite spriteWithFile:@"green_board.png"];
	[self addChild:p_green_board z:3];
	[p_green_board setPosition:convertPoint(ccp(160,720))];//330	


	
	p_purchase_back1 = [CCMenuItemImage itemFromNormalImage:@"back_button.png"
									  selectedImage:@"back_button_down.png"
											 target:self
										   selector:@selector(callback:)];
	

	
	
	
	p_purchase_unlock = [CCMenuItemImage itemFromNormalImage:@"unlock_button.png"
											  selectedImage:@"unlock_button_down.png"
													 target:self
												   selector:@selector(unlockPlayer)];

	
	
	[p_purchase_back1 setPosition:convertPoint(ccp(60,-195))];
	[p_purchase_back2 setPosition:convertPoint(ccp(70,-195))];
	[p_purchase_buy setPosition:convertPoint(ccp(-64,-195))];
	[p_purchase_unlock setPosition:convertPoint(ccp(-65,-195))];
	
	CCMenu *greem_menu = [CCMenu menuWithItems:p_purchase_back1,p_purchase_unlock,nil];
	[p_green_board addChild:greem_menu z:2];
	if (isPad)
	{	
		greem_menu.position = ccp(250,432);	
	}	
	
	
	p_getcoins = [CCSprite spriteWithFile:@"coins_get.png"];
	[self addChild:p_getcoins z:3];
	[p_getcoins setPosition:convertPoint(ccp(40,455))];
	p_getcoins.visible = NO;
	
	if (isPad)
	{
		coinsLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:32 itemHeight:78 startCharMap:'.'];

	}
	else
	{
		coinsLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:16 itemHeight:39 startCharMap:'.'];

	}

	[self addChild:coinsLabel z:3 tag:kCoinLabel];
	coinsLabel.position = convertPoint(ccp(70,427));
	coinsLabel.visible = NO;
	
	
	if (isPad)
	{
		coinsLabel2 = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:32 itemHeight:78 startCharMap:'.'];
		
	}
	else
	{
		coinsLabel2 = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:16 itemHeight:39 startCharMap:'.'];
		
	}	
	[self addChild:coinsLabel2 z:3 tag:kCoinLabel2];
	coinsLabel2.position = convertPoint(ccp(170,242));
	coinsLabel2.visible = NO;
	
	
//	//刚开始的时候firstTime并没有保存 读到位NO
	firstTime = [[NSUserDefaults standardUserDefaults] boolForKey:@"onetimeskey"];
	if (firstTime == NO)
	{
		m_player[0].islock = NO;
		m_player[0].choosen = YES;
		m_player[1].islock = YES;
		m_player[1].choosen = NO;	
		m_player[2].islock = YES;
		m_player[2].choosen = NO;
		m_player[3].islock = YES;
		m_player[3].choosen = NO;
		firstTime = YES;
		m_ac = 1;
		[[NSUserDefaults standardUserDefaults] setBool:firstTime forKey:@"onetimeskey"];
		[[NSUserDefaults standardUserDefaults] setInteger:m_ac forKey:@"ac"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[0].islock forKey:@"lockKey1"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[1].islock forKey:@"lockKey2"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[2].islock forKey:@"lockKey3"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[3].islock forKey:@"lockKey4"];
		
		[[NSUserDefaults standardUserDefaults] setBool:m_player[0].choosen forKey:@"choosenKey1"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[1].choosen forKey:@"choosenKey2"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[2].choosen forKey:@"choosenKey3"];
		[[NSUserDefaults standardUserDefaults] setBool:m_player[3].choosen forKey:@"choosenKey4"];
	}
	
	else
	{
		m_ac = [[NSUserDefaults standardUserDefaults] integerForKey:@"ac"];
		
		m_player[0].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"lockKey1"];
		m_player[1].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"lockKey2"];
		m_player[2].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"lockKey3"];
		m_player[3].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"lockKey4"];
		
		m_player[0].choosen = [[NSUserDefaults standardUserDefaults] boolForKey:@"choosenKey1"];
		m_player[1].choosen = [[NSUserDefaults standardUserDefaults] boolForKey:@"choosenKey2"];
		m_player[2].choosen = [[NSUserDefaults standardUserDefaults] boolForKey:@"choosenKey3"];
		m_player[3].choosen = [[NSUserDefaults standardUserDefaults] boolForKey:@"choosenKey4"];
	}
	m_index = [[NSUserDefaults standardUserDefaults] integerForKey:@"playerKey"];
	player = [[NSUserDefaults standardUserDefaults] integerForKey:@"playerKey"];
	
	[self initSound];
	[self InitBackground];
	[self initItem];
	
	
	
	
	[self judge:m_index];
	
	
	
	return self;
}



#pragma mark -
- (void)initItem
{
	p_arrowL = [CCMenuItemImage itemFromNormalImage:@"Arrow_button_L.png"
												  selectedImage:@"Arrow_button_L_down.png"
														 target:self
													   selector:@selector(moveLeft:)];
	
	p_arrowR = [CCMenuItemImage itemFromNormalImage:@"Arrow_button_R.png"
												  selectedImage:@"Arrow_button_R_down.png"
														 target:self
													   selector:@selector(moveRight:)];
//
	p_select = [CCMenuItemImage itemFromNormalImage:@"select_button.png"
												selectedImage:@"select_button_down.png"
													   target:self
													   selector:@selector(selectPlayer:)];

	p_unlock = [CCMenuItemImage itemFromNormalImage:@"unlock_button.png"
												  selectedImage:@"unlock_button_down.png"
												  disabledImage:@"unlock_d_button.png"
														 target:self
													   selector:@selector(showLayer:)];
	[p_unlock setIsEnabled:NO];
	
	
	p_backto = [CCMenuItemImage itemFromNormalImage:@"back_button.png"
												selectedImage:@"back_button_down.png"
													   target:self
													 selector:@selector(back:)];
	
	[p_arrowL setPosition:convertPoint(ccp(-132,101))];
	[p_arrowR setPosition:convertPoint(ccp(126,101))];
	[p_select setPosition:convertPoint(ccp(30,-100))];
	[p_unlock setPosition:convertPoint(ccp(30,-100))];
	[p_backto setPosition:convertPoint(ccp(30,-160))];

	CCMenu *menu = [CCMenu menuWithItems:p_select,p_unlock,p_arrowL,p_arrowR,p_backto,nil];
	[p_bg addChild:menu z:2];
	if (isPad)
	{	
		menu.position = ccp(255,450);	
	}	
	
	price = [CCSprite spriteWithFile:@"money_icon2.png"];
	[p_bg addChild:price z:-1];
	[price setPosition:ccp(120,223)];
	if(isPad)
		[price setPosition:ccp(240,446)];
	
	
	CCSprite *p_yellow =  [CCSprite spriteWithFile:@"choose_bg_yellow.png"];
	[self addChild:p_yellow z:0];
	[p_yellow setPosition:convertPoint(ccp(160,103))];
	
	p_small_lock = [CCSprite spriteWithFile:@"small_lock.png"];
	[self addChild:p_small_lock z:1];
	[p_small_lock setPosition:convertPoint(ccp(245,430))];
	p_small_lock.visible = NO;
	
	p_small_open = [CCSprite spriteWithFile:@"small_open.png"];
	[self addChild:p_small_open z:1];
	[p_small_open setPosition:ccp(245,430)];
	p_small_open.visible = NO;
	
	p_small_select = [CCSprite spriteWithFile:@"small_select.png"];
	[self addChild:p_small_select z:1];
	[p_small_select setPosition:convertPoint(ccp(245,430))];
	p_small_select.visible = NO;
	if (isPad)
	{
		numLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:32 itemHeight:78 startCharMap:'.'];

	}
	else
	{
		numLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:16 itemHeight:39 startCharMap:'.'];

	}

	[self addChild:numLabel z:1 tag:kNumLabel];
	numLabel.position = convertPoint(ccp(170,210));
	numLabel.visible = NO;
	
	[self showPlayer:m_index];
	[self showPrice:m_index];
	return;

}

#pragma mark -
- (void)judge:(int)m_tag
{
 
	if (m_player[m_tag].islock == YES)
	{
		p_unlock.visible = YES;
		[p_unlock setIsEnabled:NO];
		p_select.visible = NO;
		[p_select setIsEnabled:NO];
		lock.visible = YES;
		p_select_icon.visible = NO;
		p_lock_icon.visible = YES;
		p_bg_front_2.visible = YES;
		p_bg_front_3.visible = NO;
		choose.visible = NO;
		lock.visible = NO;
		numLabel.visible = NO;
		price.visible = NO;
		p_small_lock.visible = YES;
		p_small_open.visible = NO;
		p_small_select.visible = NO;
		
	}
	else
	{	
		
		p_lock_icon.visible = NO;
		p_small_lock.visible = NO;
		p_bg_front_3.visible = YES;
		p_bg_front_2.visible = NO;
		p_unlock.visible = NO;
		[p_unlock setIsEnabled:NO];
		p_select.visible = YES;
		[p_select setIsEnabled:YES];
		p_select_icon.visible = YES;
		numLabel.visible = NO;
		price.visible = NO; 
	    
		if (player == m_tag)
		{
			lock.visible = NO;
			choose.visible = YES;
			p_small_select.visible = YES;
			p_small_open.visible = NO;
			
		}
		else
		{
			lock.visible = YES;
			choose.visible = NO;
			p_small_select.visible = NO;
			p_small_open.visible = YES;
			
		}	
		
	}


}

#pragma mark -
- (void)moveLeft:(id)sender
{

	[[SimpleAudioEngine sharedEngine] playEffect:@"ChoosestageL.aif"];
	
	if (isPad)
	{
		[m_player_bg[m_index].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(302,200)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		[m_player[m_index].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(302,218)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		
	}
	else
	{
		[m_player_bg[m_index].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,100)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		[m_player[m_index].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,109)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		
	}

	
	[[SimpleAudioEngine sharedEngine] playEffect:@"Changeplayer.aif"];
	m_index--;
	if (m_index < 0)
	{
		m_index = 3;
	}
	[self showPlayer:m_index];
	[self showPrice:m_index];
	[self judge:m_index];
	
     return;
}

#pragma mark -
-(void)moveRight:(id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"ChoosestageR.aif"];
    if (isPad)
	{
		[m_player_bg[m_index].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(302,200)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		[m_player[m_index].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(302,218)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		
		
	}
	else
	{
		
		[m_player_bg[m_index].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,100)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		[m_player[m_index].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,109)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
		
	}
	[[SimpleAudioEngine sharedEngine] playEffect:@"Changeplayer.aif"];
	m_index++;
	if (m_index > 3)
	{
		m_index = 0;
	}
	
	[self showPlayer:m_index];
	[self showPrice:m_index];
	[self judge:m_index];
	
	
	
}

#pragma mark -
- (void)showPrice:(int)m_tag
{

	m_player[0].price = 5000;
	m_player[1].price = 5000;
	m_player[2].price = 5000;
	m_player[3].price = 5000;
	m_price = m_player[m_index].price;
	
	p_Str = [NSString stringWithFormat:@"%03d",m_price];
	numLabel = (CCLabelAtlas *)[self getChildByTag:kNumLabel];
	[numLabel setString:p_Str];
	numLabel.visible = NO;

	if (m_tag != 0)
		unlockText.visible = YES;
	else 
		unlockText.visible = NO;
}

#pragma mark -
- (void)showPlayer:(int)m_tag
{
	//人物的转换
	
	switch (m_tag)
	{
		case 0:
		{
			if (isPad)
			{
				[m_player_bg[0].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(322,560)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[0].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(322,600)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];

			}
			else 
			{
				[m_player_bg[0].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(161,280)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[0].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,300)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];

			}


		}
			break;
		case 1:
		{
			if (isPad)
			{
				[m_player_bg[1].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(322,560)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[1].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(322,600)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
				
			}
			else 
			{
				[m_player_bg[1].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(161,280)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[1].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,300)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
				
			}
		}
			break;
	
		case 2:
		{
			if (isPad)
			{
				[m_player_bg[2].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(322,560)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[2].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(322,600)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
				
			}
			else 
			{
				[m_player_bg[2].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(161,280)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[2].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,300)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
				
			}			
		}
			break;	
		case 3:
		{
			if (isPad)
			{
				[m_player_bg[3].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(322,560)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[3].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(322,600)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
				
			}
			else 
			{
				[m_player_bg[3].p_bg runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(161,280)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,50)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];	
				[m_player[3].p_player runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.25 position:ccp(161,300)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-17)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,53)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-24)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,16)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-4)],nil]];
				
			}
		}
			break;
			

			
		default:
			break;
	}

	return;
}


#pragma mark -
- (void)back:(id)sender
{
	[[NSUserDefaults standardUserDefaults] setInteger:m_ac forKey:@"ac"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [MainScene scene]];

}

#pragma mark -
- (void)selectPlayer:(id)sender
{
	
	switch (m_index)
	{
		case 0:
			player = m_index;

			break;
		case 1:
			player = m_index;
			
			break;
		case 2:
			player = m_index;

			break;	
		case 3:
			player = m_index;
			
			break;		
		default:
			break;	
	}
	choose.visible = YES;
	[[NSUserDefaults standardUserDefaults] setInteger:player forKey:@"playerKey"];
	switch (player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"doc.aif"];
		}
			break;
		default:
			break;
	}
	[self judge:m_index];
   [[CCDirector sharedDirector] replaceScene: [MainScene scene]];
}


#pragma mark -
- (void)callback:(id)sender
{
	
	[self judge:m_index];
	[p_green_board runAction:[CCMoveTo actionWithDuration:0.1 position:convertPoint(ccp(160,720))]];

	p_getcoins.visible = NO;
	coinsLabel.visible = NO;
	coinsLabel2.visible = NO;
	[p_select setIsEnabled:YES];
	[p_unlock setIsEnabled:NO];
	[p_backto setIsEnabled:YES];	
	
	[p_purchase_unlock setIsEnabled:NO];
	[p_purchase_buy setIsEnabled:NO];
	[p_purchase_back1 setIsEnabled:NO];	
	[p_purchase_back2 setIsEnabled:NO];
	p_black.visible = NO;
	return;

}

#pragma mark -
- (void)unlockPlayer
{
		
	m_num = m_num - m_player[m_index].price;
	if (m_num < 0)
	{
		m_num = 0;
	}
	
	if (m_num > 0)
	{
		switch (m_index)
		{
			case 0:
				m_player[0].islock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_player[0].islock forKey:@"lockKey1"];
				m_ac++;
				break;
			case 1:
				m_player[1].islock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_player[1].islock forKey:@"lockKey2"];
 				m_ac++;
 				break;
			case 2:
				m_player[2].islock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_player[2].islock forKey:@"lockKey3"];
 				m_ac++;
 				break;
			case 3:
				m_player[3].islock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_player[3].islock forKey:@"lockKey4"];
 				m_ac++;
 				break;
			default:
				break;
		}
		
		p_Str2 = [NSString stringWithFormat:@"%d",m_price];
		[coinsLabel setString:p_Str2];
		
		p_Str3 = [NSString stringWithFormat:@"%d",m_num];
		[coinsLabel2 setString:p_Str3];
		
		[[NSUserDefaults standardUserDefaults] setInteger:m_num forKey: @"totalNum"];
		
		[self judge:m_index];
		[p_green_board runAction:[CCMoveTo actionWithDuration:0.1 position:convertPoint(ccp(160,720))]];
		
		p_getcoins.visible = NO;
		coinsLabel.visible = NO;
		coinsLabel2.visible = NO;
		[p_select setIsEnabled:YES];
		[p_unlock setIsEnabled:NO];
		[p_backto setIsEnabled:YES];	
		
		[p_purchase_unlock setIsEnabled:NO];
		[p_purchase_buy setIsEnabled:NO];
		[p_purchase_back1 setIsEnabled:NO];	
		[p_purchase_back2 setIsEnabled:NO];
		p_black.visible = NO;
		
		[[SimpleAudioEngine sharedEngine] playEffect:@"Unlock.aif"];
	}
	

	return;
}

#pragma mark -
- (void)showLayer:(id)sender
{


	
	[p_purchase_unlock setIsEnabled:YES];
	[p_purchase_buy setIsEnabled:YES];
	[p_purchase_back1 setIsEnabled:YES];	
	[p_purchase_back2 setIsEnabled:YES];	
	
	[p_select setIsEnabled:NO];
	[p_unlock setIsEnabled:NO];
	[p_backto setIsEnabled:NO];	
	[p_green_board runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.1 position:convertPoint(ccp(160,240))],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-27)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,27)],[CCCallFunc actionWithTarget:self selector:@selector(showLayer2)],nil]];

    return;
	
}

- (void)showLayer2
{
	p_getcoins.visible = YES;
	coinsLabel.visible = YES;
	coinsLabel2.visible = YES;
	
	p_Str2 = [NSString stringWithFormat:@"%03d",m_num];
	coinsLabel = (CCLabelAtlas *)[self getChildByTag:kCoinLabel];
	[coinsLabel setString:p_Str2];
	
	p_Str3 = [NSString stringWithFormat:@"%03d",m_price];
	coinsLabel2 = (CCLabelAtlas *)[self getChildByTag:kCoinLabel2];
	[coinsLabel2 setString:p_Str3];
	
	p_black.visible = YES;
    return;
}


#pragma mark -
- (void)InitBackground
{
	p_backmapNode = [CCNode node];
	[self addChild:p_backmapNode z:-1];
	
	p_backNum = [[NSMutableArray alloc] initWithCapacity:10];
	
	for (int i=0;i<6;i++)
	{
		p_backSprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"stage2_scense_bg_%02d.png",i+1]];
		[p_backSprite setPosition:ccp(160,479*i+240)];
		if (isPad) 
			[p_backSprite setPosition:convertPoint(ccp(160,479*i+240))];	
		[p_backmapNode addChild:p_backSprite z:-1];
		[p_backNum addObject:p_backSprite];
		[p_backSprite.texture setAliasTexParameters];
	}
	
	id backgoUp = nil;
	if (isPad) 
	{
		backgoUp = [CCRepeatForever actionWithAction:
					[CCSequence actions:
					 [CCMoveBy actionWithDuration:240 position:ccp(0,-479*2)],
					 [CCCallFunc actionWithTarget:self selector:@selector(backCallback)],
					 nil]];		
	}
	else 
	{
		backgoUp = [CCRepeatForever actionWithAction:
					[CCSequence actions:
					 [CCMoveBy actionWithDuration:120 position:ccp(0,-479)],
					 [CCCallFunc actionWithTarget:self selector:@selector(backCallback)],
					 nil]];
	}
	
	p_backSpeed = [CCSpeed actionWithAction:backgoUp speed:1];
	[p_backmapNode runAction:p_backSpeed];
	p_backSpeed.speed = 20;
	
	
	p_black = [CCSprite spriteWithFile:@"choose_board_black.png"];
	[self addChild:p_black z:2];
	[p_black setPosition:convertPoint(ccp(160,240))];
	p_black.visible = NO;
	
	unlockText = [CCSprite spriteWithFile:@"unlock_text.png"];
	unlockText.position = convertPoint(ccp(160,238));
	[self addChild:unlockText z:40];
	unlockText.visible = NO;
}

-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ChoosestageL.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Changeplayer.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ChoosestageR.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Replacescene.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bro.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"old.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"rock.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"doc.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Unlock.aif"];
}

#pragma mark -
- (void)backCallback
{
	m_callbackCount_back++;
	CCSprite *tempMap = [p_backNum objectAtIndex:m_mapCurrentIndex_back%6];
	if (isPad) 
		tempMap.position = ccp(tempMap.position.x,tempMap.position.y + 956*6);
	else 
		tempMap.position = ccp(tempMap.position.x,tempMap.position.y + 478*6);
	
	m_mapCurrentIndex_back++;		
	[tempMap.texture setAliasTexParameters];	
	
	
}

#pragma mark -

- (void)dealloc
{
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	[p_backNum release];
	p_backNum = nil;
	[super dealloc];
	
}


@end
