//
//  MainScene.h
//  FatJumper
//
//  Created by in-blue  on 10-10-11.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import "StageMenu.h"
#import "ChooseScene.h"
#import "GameScene.h"
#import "GameOver.h"
#import "GameWin.h"
#import "StageMenuUn.h"

@interface MainScene : CCLayer 
{
	BOOL endless;
	int m_player;
	CCSprite *p_backSprite;
	NSMutableArray *p_backNum;
	int m_callbackCount_back;
	int m_mapCurrentIndex_back;
	CCSpeed *p_backSpeed;
	CCNode *p_backmapNode;
	CCMenuItem *start;
	CCMenuItem *unlimted;
	CCMenuItem *open;
	CCMenuItem *label;
	CCSprite *p_mainmenuback; 
	CCSprite *p_mainmenufront; 
	CCSprite *p_player;
	CCSprite *p_title_anim;
	CCSprite *p_title;
	CCSprite *p_cocpyright;
	int m_x1;
	int m_x2;
	CCSprite *p_loading_BG;
	CCSprite *p_loading_mouth;
	CCSprite *p_loading_teeth;
	CCSprite *p_loading_eyes;
	BOOL firstTime;
	BOOL alert;
	
	CCSprite*		m_pTipBackGround;
	CCSprite*		m_pTipInfo;
	CCSprite*		m_pTipGameCenterBack;
	CCSprite*		m_pTipOpenFaintBack;
	CCMenuItem*		m_pBtnGCLeaderBoards,*m_pBtnGCAchievement;
	CCMenuItem*		m_pBtnOFLeaderBoards,*m_pBtnOFAchievement;
	bool			m_bIsShowGCAndOF; 
}
+(id) scene;
- (id)initWithPlayer:(int)player;
- (void)InitItem; 
- (void)InitBackground;
- (void)Loading;
- (void)Start;
- (void)initSound;
@end
