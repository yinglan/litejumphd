
//  GameStory.m
//  FatJumper
//
//  Created by  apple on 10-9-7.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GameStory.h"
#import "GameDataDefined.h"

#define IPAD_OBSTRUCTE_TOP		857
#define IPAD_OBSTRUCTE_BOTTOM	858

@interface GameStory (PrivateMethods)
-(void) skipL:(id)sender;
-(void) skipR:(id)sender;
@end

@implementation GameStory


+(id) scene
{
	CCScene *scene = [CCScene node];
	GameStory *layer = [GameStory node];
	[scene addChild: layer];
	return scene;
}

-(id) init
{
	if( (self=[super init] )) 
	{
		m_bIsTuched = false;
		for (int i = 0; i < STORY_PIC_COUNT; i++) {
			p_Story[i] = nil;
		}
		self.isTouchEnabled = YES;
		m_picIndex = 0;
		slideOnce = YES;
		m_bIsTuched = true;
		m_bIsShowLight = false;
		gameStart = NO;
		m_bLeftTurnPage = false;
		m_bSkipAll = false;
		//[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"GameStory.aif" loop:YES];
		int player = [[NSUserDefaults standardUserDefaults] integerForKey: @"playerKey"];
		BOOL endless = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameEndless"];
		int laststate = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameState"];
		[self initWithState:laststate :endless :player];
		
	}
	return self;
}

- (id)initWithState:(int)laststate:(BOOL)endless:(int)player
{
	m_state = laststate;
	m_endless = endless;
	m_player = player;
	
	 
	m_bXXX = false;
	[self initSound];
	[self initItem:m_state];
	return self;
}
-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"begin.aif"];
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"howtoplay.aif" ];
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"coming.aif"];

}

- (void)initItem:(int)m_stage
{
	if (isPad)
	{
		CCSprite *haha = [CCSprite spriteWithFile:@"123456.png"];
		[self addChild:haha z:11];
		[haha setPosition:ccp(384,512)];
	}
	
	switch (m_stage)
	{
		case kStage_1:
		{
			m_picIndex = 0;
			if (!isPad) {
				p_Story[0] = [CCSprite spriteWithFile:@"story_1_1.png"];
			}
			else {
				p_Story[0] = [CCSprite spriteWithFile:@"story_1_1_ipad.png"];
			}

			[p_Story[0] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[0] z:1];
			p_Story[0].opacity = 0;
			[p_Story[0] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"begin.aif" loop:YES];
			
			if (!isPad) 
			{
				p_lightting = [CCSprite spriteWithFile:@"lt_01.png"];
				[p_lightting setPosition:ccp(480,-480)];	
			}
			else 
			{
				p_lightting = [CCSprite spriteWithFile:@"lt_ipad_01.png"];
				
				[p_lightting setPosition:convertPoint(ccp(480, 480))];
			}

			[self addChild:p_lightting z:2];
			p_lightting.visible = YES;
			
			if (isPad) 
			{
				m_pBlackLayerTop = [CCLayerColor layerWithColor:ccc4(0,0,0,255) width:640.0f height:266.5f];
				m_pBlackLayerTop.position = ccp(64,480-450);
				
				m_pBlackLayerBotttom = [CCLayerColor layerWithColor:ccc4(0,0,0,255) width:640.0f height:266.5f];
				m_pBlackLayerBotttom.position = ccp(64,480+247);
				
				[self addChild:m_pBlackLayerTop z:8];
				[self addChild:m_pBlackLayerBotttom z:8];
			}
		}
			break;
			
		
		case kStage_2:
		{
			m_picIndex = 12;
			p_Story[12] = [CCSprite spriteWithFile:@"story_1_13.png"];
			[p_Story[12] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[12] z:1];
			p_Story[12].opacity = 0;
			[p_Story[12] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
			
		}
			break;
		
		case kStage_3:
		{
			m_picIndex = 14;
			p_Story[14] = [CCSprite spriteWithFile:@"story_1_15.png"];
			[p_Story[14] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[14] z:1];
			p_Story[14].opacity = 0;
			[p_Story[14] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
		}
			break;
	
		case kStage_4:
		{
			m_picIndex = 15;
			p_Story[15] = [CCSprite spriteWithFile:@"story_1_16.png"];
			[p_Story[15] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[15] z:1];
			p_Story[15].opacity = 0;
			[p_Story[15] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
		}
			break;
			
		case kStage_5:
		{
			m_picIndex = 16;
			p_Story[16] = [CCSprite spriteWithFile:@"story_1_17.png"];
			[p_Story[16] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[16] z:1];
			p_Story[16].opacity = 0;
			[p_Story[16] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
		}
			break;
		case kStage_6:
		{
			m_picIndex = 17;
			p_Story[17] = [CCSprite spriteWithFile:@"story_1_18.png"];
			[p_Story[17] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[17] z:1];
			p_Story[17].opacity = 0;
			[p_Story[17] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
		}
			break;
			
		case kStage_7:
		{
			m_picIndex = 18;
		}
			break;
		case kStage_8:
		{
			m_picIndex = 19;
			p_Story[19] = [CCSprite spriteWithFile:@"story_1_20.png"];
			[p_Story[19] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[19] z:1];
			p_Story[19].opacity = 0;
			[p_Story[19] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
		}
			break;
			
		case kStage_9:
		{
			m_picIndex = 20;
			p_Story[20] = [CCSprite spriteWithFile:@"story_1_21.png"];
			[p_Story[20] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[20] z:1];
			p_Story[20].opacity = 0;
			[p_Story[20] runAction:[CCFadeIn actionWithDuration:0.5]];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
			
			
		}
			break;
		default:
			break;
	}
	
	
	p_lb = [CCMenuItemImage itemFromNormalImage:@"lb_01.png"
									  selectedImage:@"lb_02.png"
											 target:self
										   selector:@selector(skipPic:)];
	
	p_lb2 = [CCMenuItemImage itemFromNormalImage:@"lb_L.png"
								   selectedImage:@"lb_L.png"
										  target:self
										selector:@selector(skipL:)];
	[p_lb2 setPosition:convertPoint(ccp(10,210))];
	p_lb3 = [CCMenuItemImage itemFromNormalImage:@"lb_R.png"
								   selectedImage:@"lb_R.png"
										  target:self
										selector:@selector(skipR:)];
	[p_lb3 setPosition:convertPoint(ccp(10,-210))];
	p_menu = [CCMenu menuWithItems:p_lb,p_lb2,p_lb3,nil];	
	if (isPad) {
		p_menu.position = ccp(314,482);
	}
	[self addChild:p_menu z:3];
	
	// lable flag
	if (m_state == 1)
	{
		if (!isPad) 
		{
			[p_lb setPosition:convertPoint(ccp(-140,-187))];
			[p_lb3 setPosition:convertPoint(ccp(10,-220))];
			[p_lb2 setPosition:convertPoint(ccp(10,220))];	
		}
		else 
		{
			[p_lb setPosition:convertPoint(ccp(100,-90))];
			[p_lb2 setPosition:convertPoint(ccp(-140,0))];
			[p_lb3 setPosition:convertPoint(ccp(140,0))];
			p_lb.rotation = 270.0f;
			p_lb2.rotation = 270.0f;
			p_lb3.rotation = 270.0f;
		}
	}
    else
	{
		[p_lb setPosition:convertPoint(ccp(100,-210))];
		[p_lb2 setPosition:convertPoint(ccp(-110,208))];
		[p_lb3 setPosition:convertPoint(ccp(110,208))];
		p_lb.rotation = 270.0f;
		p_lb2.rotation = 270.0f;
		p_lb3.rotation = 270.0f;
	}
	if (m_state == 3 || m_state == 4 || m_state == 5 || m_state == 6 || m_state == 8 || m_state == 9) 
	{
		p_lb2.visible = NO;
		p_lb3.visible = NO;
	}
	if (m_state == 1 || m_state == 2 || m_state == 7)
	{
		p_lb2.visible = NO;
	}
}

- (void)skipL:(id)sender
{
	switch (m_state)
	{
		case kStage_1:
		{
			if (m_picIndex <= 0) 
				return;
			if (m_picIndex == 9) 
				return;
			break;
		}
		case kStage_2:
		{
			if (m_picIndex <= 12) 
				return;
			break;
		}
		case kStage_3:
		{
			if (m_picIndex <= 14) 
				return;
			break;
		}
		case kStage_4:
		{
			if (m_picIndex <= 15) 
				return;
			break;
		}
		case kStage_5:
		{
			if (m_picIndex <= 16) 
				return;
			break;
		}
		case kStage_6:
		{
			if (m_picIndex <= 17) 
				return;
			break;
		}
		case kStage_7:
		{
			if (m_picIndex <= 18) 
				return;
			break;
		}
		case kStage_8:
		{
			if (m_picIndex <= 19) 
				return;
			break;
		}
		case kStage_9:
		{
			if (m_picIndex <= 20) 
				return;
			break;
		}
		default:
			break;
	}
	m_bLeftTurnPage = true;
	[self skipTotur];
}

- (void)skipR:(id)sender
{
	m_bLeftTurnPage = false;
	[self skipTotur];
}

- (void)skipPic:(id)sender
{
	m_bIsTuched = true;
	switch (m_state)
	{
		case 1:
		{
			if (m_picIndex < 9)
			{
				m_bSkipAll  = true;
				m_bLeftTurnPage = true;
				[self skipTotur];
			}
            else
			{
				m_picIndex = STAGE1_MAX_INDEX;
				[self startGame];
			}
		}
			break;
		case 2:
		{
			m_picIndex = STAGE2_MAX_INDEX;
			[self startGame];
		}
			break;
		case 3:
		{
			m_picIndex = STAGE3_MAX_INDEX;
			[self startGame];
		}
			break;
		case 4:
		{
			m_picIndex = STAGE4_MAX_INDEX;
			[self startGame];
		}
			break;
		case 5:
		{
			m_picIndex = STAGE5_MAX_INDEX;
			[self startGame];
		}
			break;
		case 6:
		{
			m_picIndex = STAGE6_MAX_INDEX;
			[self startGame];
			
		}
			break;
		case 7:
		{
			m_picIndex = STAGE7_MAX_INDEX;
			[self startGame];
			
		}
			break;
		case 8:
		{
			m_picIndex = STAGE8_MAX_INDEX;
			[self startGame];
		}
			break;
		case 9:
		{
			m_picIndex = STAGE9_MAX_INDEX;
			[self startGame];
		}
			break;
			
		default:
			break;
	}

}

-(void)startGame
{
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	gameStart = YES;
	[SimpleAudioEngine end];
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	[[CCDirector sharedDirector] purgeCachedData];
	[[CCDirector sharedDirector] replaceScene: [GameScene scene]];
}

#pragma mark RELEASE 
- (void)dealloc
{
	[[CCTextureCache sharedTextureCache] removeAllTextures];
    [super dealloc];
}

#pragma mark TOUCHE_LOGIC
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)movelightting:(id)sender
{
	float fTime = 0.5f;
	if (isPad) 
		fTime = 0.25f;
	[p_lightting runAction:[CCSequence actions:[CCMoveTo actionWithDuration:fTime position:convertPoint(ccp(160,240))],
							[CCCallFuncN actionWithTarget:self selector:@selector(showlightting:)],nil]];
}

- (void)showlightting:(id)sender
{
	//if (!m_bXXX) {
	m_bIsTuched = true;
	[self skipR:(id)FUCK_SENDER];
	return;
	//}
	CCAnimation *light_lt = [CCAnimation animationWithName:@"lighting" delay:0.5f];
	for (int i=1; i<3; i++) 
	{
		[light_lt addFrameWithFilename:[NSString stringWithFormat:@"lt_%02d.png", i]];
	}
	id action = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:light_lt restoreOriginalFrame:NO]];
	[p_lightting runAction:action];
	m_bIsTuched = true;
}

-(void)xxx:(id)sender
{
	m_bXXX = true;
	
}

-(void)touchedCallBack:(id)sender
{
	m_bIsTuched = true;
}

- (void)FadeCallBack:(id)sender
{
	if (m_picIndex >= STORY_PIC_COUNT) return;
	
	m_picIndex = m_picIndex+(m_bLeftTurnPage?-1:1);
	if (m_bSkipAll)
	{
		m_picIndex = 9;
		m_bSkipAll = false;
	}
	
	// judge 
	if (m_picIndex >= 9)
	{
		[p_lb setPosition:convertPoint(ccp(100,-210))];
		[p_lb2 setPosition:convertPoint(ccp(-110,208))];
		[p_lb3 setPosition:convertPoint(ccp(110,208))];
		p_lb.rotation = 270.0f;
		p_lb2.rotation = 270.0f;
		p_lb3.rotation = 270.0f;
		
		if (m_picIndex == 9)
		{
			[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"howtoplay.aif" loop:YES];
		}
	}
	else 
	{
		if (!isPad) 
		{
			[p_lb3 setPosition:convertPoint(ccp(10,-220))];
			[p_lb setPosition:convertPoint(ccp(-140,-187))];
			[p_lb2 setPosition:convertPoint(ccp(10,220))];
			p_lb.rotation = 0.0f;
			p_lb2.rotation = 0.0f;
			p_lb3.rotation = 0.0f;	
		}
		else 
		{
			[p_lb setPosition:convertPoint(ccp(100,-80))];
			[p_lb2 setPosition:convertPoint(ccp(-140,0))];
			[p_lb3 setPosition:convertPoint(ccp(140,0))];
			p_lb.rotation = 270.0f;
			p_lb2.rotation = 270.0f;
			p_lb3.rotation = 270.0f;
		}
	}
	// judge left Arraw
	if (m_state == 1) 
	{
		if (m_picIndex == 0 || m_picIndex == 9) 
		{
			p_lb2.visible = NO;
		}
		else 
		{
			p_lb2.visible = YES;
		}
		
		if (m_picIndex < 6) 
		{
			m_pBlackLayerTop.visible = YES;
			m_pBlackLayerBotttom.visible = YES;
		}
		else 
		{
			m_pBlackLayerTop.visible = NO;
			m_pBlackLayerBotttom.visible = NO;
		}

	}
	else 
	{
		if (m_picIndex == 0) 
		{
			p_lb2.visible = NO;
		}
		else 
		{
			p_lb2.visible = YES;
		}
	}
	
	
	p_Story[m_picIndex].visible = YES;
	if (m_picIndex != 4 && m_picIndex != 5) 
	{
		[p_Story[m_picIndex] runAction:[CCFadeIn actionWithDuration:0.25]];
		[p_Story[m_picIndex] runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.25],
										[CCCallFuncN actionWithTarget:self selector:@selector(touchedCallBack:)],nil]];
	}
	else 
	{
		int nDistance = 0;
		if (!isPad) 
		{
			CGSize szContent = p_Story[m_picIndex].contentSize;
			nDistance = szContent.width-320;
			[p_Story[m_picIndex] setPosition:convertPoint(ccp(160+(m_picIndex==4?-nDistance:nDistance)/2,240))];	
		}
		else 
		{
			if (m_picIndex == 4) 
			{
				[p_Story[m_picIndex] setPosition:ccp(384,512-213.5f)];
			}
			else if (m_picIndex == 5)
			{
				[p_Story[m_picIndex] setPosition:ccp(384,512+213.5f)];
			}	
		}
		
		
		if (m_picIndex == 4)
		{
			if (!isPad) 
			{
				[p_Story[m_picIndex] runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.25],
												[CCMoveBy actionWithDuration:2.75 position:ccp(nDistance,0)],
												[CCCallFuncN actionWithTarget:self selector:@selector(xxx:)],
												[CCCallFuncN actionWithTarget:self selector:@selector(touchedCallBack:)],nil]];	
			}
			else 
			{
				[p_Story[m_picIndex] runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.25],
												[CCMoveBy actionWithDuration:2.75 position:ccp(0,429)],
												[CCCallFuncN actionWithTarget:self selector:@selector(xxx:)],
												[CCCallFuncN actionWithTarget:self selector:@selector(touchedCallBack:)],nil]];
			}
		}
		else if (m_picIndex == 5) 
		{
			if (!isPad)
			{
				[p_Story[m_picIndex] runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.25],
												[CCMoveBy actionWithDuration:3.75 position:ccp(-nDistance,0)],
												[CCCallFuncN actionWithTarget:self selector:@selector(touchedCallBack:)],nil]];	
			}
			else 
			{
				[p_Story[m_picIndex] runAction:[CCSequence actions:[CCFadeIn actionWithDuration:0.25],
												[CCMoveBy actionWithDuration:3.75 position:ccp(0,-490)],
												[CCCallFuncN actionWithTarget:self selector:@selector(touchedCallBack:)],nil]];
			}
		}		
	}	
	if (p_lb.visible) [p_lb runAction:[CCFadeIn actionWithDuration:0.25]];
	if (p_lb2.visible) [p_lb2 runAction:[CCFadeIn actionWithDuration:0.25]];
	if (p_lb3.visible) [p_lb3 runAction:[CCFadeIn actionWithDuration:0.25]];
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (m_state == 1) 
	{
		if (m_picIndex >= 9) [self skipR:(id)FUCK_SENDER];
		return;
	}
	if (m_state == 2 || m_state == 3 || m_state == 4 || m_state == 5 || m_state == 6 || m_state == 8 || m_state == 9) 
	{
		[self skipR:(id)FUCK_SENDER];
	}
}


- (void)skipTotur
{
	if (m_bIsTuched) {
		m_bIsTuched = false;
		if (!m_bLeftTurnPage) {
			if (m_state == 1) {
				if (m_picIndex == STAGE1_MAX_INDEX) {
					[self startGame];
					return;
				}
			}else if (m_state == 2) {
				if (m_picIndex == STAGE2_MAX_INDEX) {
					[self startGame];
					return;
				}
			}else if (m_state == 3) {
				if (m_picIndex == STAGE3_MAX_INDEX) {
					[self startGame];
					return;
				}
			}
			else if (m_state == 4) {
				if (m_picIndex == STAGE4_MAX_INDEX) {
					[self startGame];
					return;
				}
			}
			else if (m_state == 5) {
				if (m_picIndex == STAGE5_MAX_INDEX) {
					[self startGame];
					return;
				}
			}
			else if (m_state == 6) {
				if (m_picIndex == STAGE6_MAX_INDEX) {
					[self startGame];
					return;
				}
			}
			else if (m_state == 7) {
				if (m_picIndex == STAGE7_MAX_INDEX) {
					[self startGame];
					return;
				}
				
			}
			else if (m_state == 8) {
				if (m_picIndex == STAGE8_MAX_INDEX) {
					[self startGame];
					return;
				}
				
			}
			else if (m_state == 9) {
				if (m_picIndex == STAGE9_MAX_INDEX) {
					[self startGame];
					return;
				}
				
			}	
		}
		
		if (m_picIndex == 4) {
			if (!m_bIsShowLight)
			{
				m_bIsShowLight = true;
				[self movelightting:(id)FUCK_SENDER];
				return;
			}
			else
			{
				p_lightting.visible = NO;
			}
		}
		else if (m_picIndex == 5 || m_picIndex == 3){
			if (m_bLeftTurnPage) {
				m_bIsShowLight = false;
				m_bXXX = false;
			}
		}
		
		while (true){
			int nTempNextPic = m_picIndex+(m_bLeftTurnPage?-1:1);
			if (m_bSkipAll) {
				nTempNextPic = 9;
			}
			if (nil != p_Story[nTempNextPic]) {
				break;
			}
			
			int nTempStateIndex = 0;
			if (m_state <= 9)
			{
				nTempStateIndex = 1;
			}
			
			
			NSString *strResPath = nil;
			if (!isPad || nTempNextPic >= 9) {
				strResPath = [NSString stringWithFormat:@"story_%d_%d.png",nTempStateIndex,nTempNextPic+1];	
			}
			else {
				strResPath = [NSString stringWithFormat:@"story_%d_%d_ipad.png",nTempStateIndex,nTempNextPic+1];
			}

			p_Story[nTempNextPic] = [CCSprite spriteWithFile:strResPath];
			[p_Story[nTempNextPic] setPosition:convertPoint(ccp(160,240))];
			[self addChild:p_Story[nTempNextPic] z:1];
			
			
			if (nTempNextPic == 4 || nTempNextPic == 5) 
			{
				CGSize szContent;
				memset(&szContent,0,sizeof(szContent));
				int nBlank = 0;
				
				if (!isPad) 
				{
					if (nTempNextPic == 4)
					{
						szContent = p_Story[nTempNextPic].contentSize;
						nBlank = szContent.width-320;
						[p_Story[nTempNextPic] setPosition:convertPoint(ccp(160-nBlank/2,240))];
					}
					else 
					{
						szContent = p_Story[nTempNextPic].contentSize;
						nBlank = szContent.width-320;
						[p_Story[nTempNextPic] setPosition:convertPoint(ccp(160+nBlank/2,240))];
						[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
						[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"coming.aif" loop:YES];
					}	
				}
				else
				{
					if (nTempNextPic == 4) 
					{
						[p_Story[nTempNextPic] setPosition:ccp(384,512-213.5f)];
					}
					else
					{
						[p_Story[nTempNextPic] setPosition:ccp(384,512+231.5f)];
						[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
						[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"coming.aif" loop:YES];
					}
				}

				
			}
			p_Story[nTempNextPic].visible = NO;
		}
		
		[p_Story[m_picIndex] runAction:[CCSequence actions:[CCFadeOut actionWithDuration:0.25],
										[CCCallFuncN actionWithTarget:self selector:@selector(FadeCallBack:)],nil]];
		if (p_lb.visible) [p_lb runAction:[CCFadeOut actionWithDuration:0.25]];
		if (p_lb2.visible) [p_lb2 runAction:[CCFadeOut actionWithDuration:0.25]];
		if (p_lb3.visible) [p_lb3 runAction:[CCFadeOut actionWithDuration:0.25]];
	}
}

@end
