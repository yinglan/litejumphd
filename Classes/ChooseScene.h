//
//  ChooseScene.h
//  FatJumper
//
//  Created by in-blue  on 10-10-12.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MainScene.h"
#import "SimpleAudioEngine.h"
#import "GameDataDefined.h"
#define MAX_PLAYER 5
typedef struct
{
	CCSprite *p_player;
	BOOL islock;
	BOOL choosen;
	int price;

}PlayerInfo;

typedef struct
{
	CCSprite *p_bg;

	
}Player_BGInfo;

@interface ChooseScene : CCLayer 
{
	int m_index;
	PlayerInfo m_player[MAX_PLAYER];
	Player_BGInfo m_player_bg[MAX_PLAYER];
	int player;
	int m_num;
	int m_price;
	CCSprite *choose;
	CCSprite *lock;
	int m_ac;
	CCSprite *price;
	BOOL firstTime;

	CCMenuItem *p_select;
	CCMenuItem *p_unlock;
	CCMenuItem *p_backto;
	CCLabelAtlas *numLabel;
	
	NSString *p_Str;
	
	CCMenuItem *p_arrowL;
	CCMenuItem *p_arrowR;
	
	
	CCSprite *p_backSprite;
	NSMutableArray *p_backNum;
	int m_callbackCount_back;
	int m_mapCurrentIndex_back;
	CCSpeed *p_backSpeed;
	CCNode *p_backmapNode;
	
	CCSprite *p_bg;
	CCSprite *p_bg_front_1;
	CCSprite *p_bg_front_2;
	CCSprite *p_bg_front_3;
	
	CCSprite *p_select_icon;
	CCSprite *p_lock_icon;
	
	CCSprite *p_green_board;
	CCSprite *p_yellow_board;
	
	CCMenuItem *p_purchase_unlock;
	CCMenuItem *p_purchase_buy;
	CCMenuItem *p_purchase_back1;
	CCMenuItem *p_purchase_back2;
	CCSprite *p_getcoins;
	CCLabelAtlas *coinsLabel;
	NSString *p_Str2;
	
	CCLabelAtlas *coinsLabel2;
	NSString *p_Str3;
	
	UIAlertView *unlockAlert;
	UIAlertView *buyAlert;
	
	CCSprite *p_small_lock;
	CCSprite *p_small_open;
	CCSprite *p_small_select;
	
	CCSprite *p_black;
	
	CCSprite *unlockText;
	
}
+(id) scene;
- (id)initWithNum:(int)num;
- (void)initSound;
- (void)initItem;
- (void)InitBackground;
- (void)judge:(int)m_tag;
- (void)showPlayer:(int)m_tag;
- (void)showPrice:(int)m_tag;
- (void)callback:(id)sender;
@end
