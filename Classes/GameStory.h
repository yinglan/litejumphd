//
//  GameStory.h
//  FatJumper
//
//  Created by  apple on 10-9-7.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#define kTouchDistance 80
#define kStage_1 1
#define kStage_2 2
#define kStage_3 3
#define kStage_4 4
#define kStage_5 5
#define kStage_6 6
#define kStage_7 7
#define kStage_8 8
#define kStage_9 9

#define kStage_10 10
#define kStage_11 11
#define kStage_12 12

#define STORY_PIC_COUNT 25
#define STAGE1_MAX_INDEX 11
#define STAGE2_MAX_INDEX 13
#define STAGE3_MAX_INDEX 14
#define STAGE4_MAX_INDEX 15
#define STAGE5_MAX_INDEX 16
#define STAGE6_MAX_INDEX 17
#define STAGE7_MAX_INDEX 18
#define STAGE8_MAX_INDEX 19
#define STAGE9_MAX_INDEX 20
#define FUCK_SENDER 55

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameScene.h"
#import "GameDataDefined.h"
#import "SimpleAudioEngine.h"
//#import "OpenFeint.h"
//#import "OFHighScoreService.h"
//#import "SampleOFDelegate.h"

@interface GameStory : CCLayer
{
	CCSprite *p_Story[STORY_PIC_COUNT];
	float firstpoint_x;
	float endpoint_x;
	int m_picIndex;
	int m_state;
	bool slideOnce;//用来判断每次滑动只执行一次
	bool gameStart;//用来判断进入游戏 YES时可以进入
	BOOL m_endless;
	CCMenuItem *p_lb;
	CCSprite *p_lightting;
    CCMenu *p_menu;
	
	
	// 小新
	bool				m_bIsTuched;		// 是否能被触摸
	bool				m_bIsShowLight;
	int					m_player;
	bool				m_bXXX;
	CCMenuItem*			p_lb2;
	CCMenuItem*			p_lb3;
	bool				m_bLeftTurnPage;
	bool				m_bSkipAll;
	CCLayerColor		*m_pBlackLayerTop;
	CCLayerColor		*m_pBlackLayerBotttom;
}

+(id) scene;
- (id)initWithState:(int)laststate:(BOOL)endless:(int)player;
- (void)initItem:(int)m_stage;
- (void)startGame;
- (void)skipTotur;
-(void)initSound;
@end
