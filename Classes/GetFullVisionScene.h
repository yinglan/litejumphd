//
//  GetFullVisionScene.h
//  PopoJump
//
//  Created by o0402 on 11-8-22.
//  Copyright 2011 inblue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GetFullVisionScene : CCLayer 
{
	CCSprite	*visionShow;
	CCMenu		*menu1;
	CCMenuItem			*leftButton;
	CCMenuItem			*rightButton;
	CCSprite    *show[6];
	int			m_nCurrentShowIndex;
}

+(id) scene;

@end
