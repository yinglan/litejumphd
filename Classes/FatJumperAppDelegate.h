//
//  PopoJumpAppDelegate.h
//  PopoJump
//
//  Created by in-blue  on 11-2-9.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h> 
#import <GameKit/GameKit.h>
#import "GameDataDefined.h" 

@class RootViewController;

@interface FatJumperAppDelegate : NSObject <UIApplicationDelegate> 
{
	UIWindow			*window;
	RootViewController	*viewController;
	BOOL firsttime;
	
}
- (void)initializeOpenfeint;
@property (nonatomic, retain) UIWindow *window;

@end
