//
//  GameScene.mm
//  FatJumper
//
//  Created by in-blue  on 11-2-11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameScene.h" 
#import "GetFullVisionScene.h"

@interface GameScene(PrivateMethods)
-(void) comboJudge:(unsigned int)nComboCount;
@end

static GameScene *singleInstance = nil;
@implementation GameScene

#pragma mark -
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameScene *layer = [GameScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	
	
	// return the scene
	return scene;
}

+(GameScene *) sendTopause
{
	return singleInstance;
} 


#pragma mark -

- (id)init 
{
	if((self=[super init])) 
	{
		singleInstance = self;
		
		int player = [[NSUserDefaults standardUserDefaults] integerForKey: @"playerKey"];
		BOOL endless = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameEndless"];
		int laststate = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameState"];
		[self initWithdata:endless :player :laststate];
	}
	return self;

}
#pragma mark -
- (id)initWithdata:(BOOL)endless:(int)player:(int)state
{
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	
	m_x1 = 0;
	m_x2 = 0;
	showtag = NO;
	gameEndless = endless;
	srand(time(0));
	m_player = player;
	m_state = state;
	gameSuspended = YES;
	gameWillStart = NO;
	createDone = NO;
	rains1 = NO;
	rains2 = NO;
	rains3 = NO;
	
	m_nTimeCountLeft = 0;
	m_nTimeCountRight = 0;
	
	[self initSound];
	
	if (isPad)
	{
		CCSprite *haha = [CCSprite spriteWithFile:@"123456.png"];
		[self addChild:haha z:11];
		[haha setPosition:ccp(384,512)];
	}
	
	p_loading_bg = [CCSprite spriteWithFile:@"game_loading_BG.png"];
	[p_loading_bg setPosition:convertPoint(ccp(160,240))];
	[self addChild:p_loading_bg z:4];

	
	p_loading_teeth = [CCSprite spriteWithFile:@"logo_loading_teeth.png"];
	[p_loading_teeth setPosition:convertPoint(ccp(64,215))];
	[p_loading_teeth setTextureRect:CGRectMake(0,0,24,62)];
	if (isPad) {
		[p_loading_teeth setTextureRect:CGRectMake(0,0,24*2,62*2)];
	}
	
	[self addChild:p_loading_teeth z:4];
	
	
	p_loading_mouth = [CCSprite spriteWithFile:@"logo_loading_mouth.png"];
	[p_loading_mouth setPosition:convertPoint(ccp(159,233))];
	[self addChild:p_loading_mouth z:4];
	
	p_loading_eyes = [CCSprite spriteWithFile:@"game_loading_eyes_01.png"];
	[p_loading_eyes setPosition:convertPoint(ccp(160,295))];
	[self addChild:	p_loading_eyes z:4];

	CCAnimation *p_Animation = [CCAnimation animationWithName:@"Animation" delay:0.3];
	for (int i=1; i<12; i++)
	{
		[p_Animation addFrameWithFilename:[NSString stringWithFormat:@"game_loading_eyes_%02d.png", i]];
	}
	id action = [CCAnimate actionWithAnimation:p_Animation restoreOriginalFrame:NO];
	
	[p_loading_eyes runAction:[CCRepeatForever actionWithAction:action]]; 
	
	

	
	item_tap = [CCMenuItemImage itemFromNormalImage:@"touch_to_go_bg.png"
								 selectedImage:@"touch_to_go_bg.png"
										target:self
									  selector:@selector(gameStart:)];
	item_tap.position = ccp(0,0);
	item_tap.visible = NO;
	[item_tap setIsEnabled:NO];
	tapmenu = [CCMenu menuWithItems: item_tap,nil];
	[self addChild:tapmenu z:4];
	
	p_tap = [CCSprite spriteWithFile:@"touch_to_go_text.png"];
	[p_tap setPosition:convertPoint(ccp(160,178))];
	[self addChild:	p_tap z:4];
	p_tap.visible = NO;

	
	
	
	self.isTouchEnabled = YES;
  
	musiccount = -1;
	m_nComboCount = -1;
	moneys = NO;
	
	[self schedule:@selector(updateGameResource:)];
	
	return self;
}
#pragma mark -
- (void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"Stage1.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"Stage2.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"Stage3.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ready.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"go.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"loading.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Block.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"DeathGhost.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"1.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"2.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"3.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"4.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"5.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"6.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"7.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"8.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"9.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"10.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bomb_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bomb_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bomb_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bomb_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"frozen_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"frozen_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"frozen_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"frozen_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ghost_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ghost_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ghost_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ghost_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Unfrozen.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bolt_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bolt_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bolt_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"bolt_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dash_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dash_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dash_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dash_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dead_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dead_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dead_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"dead_doc.aif"];

	[[SimpleAudioEngine sharedEngine] preloadEffect:@"fly_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"fly_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"fly_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"fly_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"wing_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"wing_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"wing_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"wing_doc.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"shoe_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"shoe_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"shoe_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"shoe_doc.aif"];

	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ballon_bro.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ballon_old.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ballon_rock.aif"];
	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ballon_doc.aif"];
}
#pragma mark -
- (void)initMan
{

	switch (m_player)
	{
		case 0:
		{
			switch (m_state)
			{
				case 1:
					
				{
					p_man = [CCSprite spriteWithFile:@"Bro_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				
				{
					p_man = [CCSprite spriteWithFile:@"Bro_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 7:
				{
					p_man = [CCSprite spriteWithFile:@"Bro_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 8:
				case 9:	
				{
					p_man = [CCSprite spriteWithFile:@"Bro_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 10:
					
				{
					p_man = [CCSprite spriteWithFile:@"Bro_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 11:
					
				{
					p_man = [CCSprite spriteWithFile:@"Bro_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 12:
				{
					p_man = [CCSprite spriteWithFile:@"Bro_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				default:
					break;
			}
			
		
			p_tf2 = [CCSprite spriteWithFile:@"tf_ballon_01.png"];
			[self addChild:p_tf2 z:1];
			p_tf2.visible = NO;
			
			p_tf4 = [CCSprite spriteWithFile:@"tf_wing_01.png"];
			[self addChild:p_tf4 z:1];
			p_tf4.visible = NO;
			
			
			p_tf5 = [CCSprite spriteWithFile:@"tf_dash_01.png"];
			[self addChild:p_tf5 z:1];
			p_tf5.visible = NO;

			
			p_tf3 = [CCSprite spriteWithFile:@"tf_shoe_01.png"];
			[self addChild:p_tf3 z:1];
			p_tf3.visible = NO;
		
		 
			p_tfrain = [CCSprite spriteWithFile:@"Bro_boltDeath_01.png"];
			[self addChild:p_tfrain z:1];
			p_tfrain.visible = NO;
			
			p_tffrozen = [CCSprite spriteWithFile:@"Bro_frozen_particle_01.png"];
			[self addChild:p_tffrozen z:1];
			p_tffrozen.visible = NO;
		}
			
			break;
		case 1:
		{
			
			switch (m_state)
			{
				case 1:
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 7:
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 8:
				case 9:	
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 10:
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 11:	
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 12:
				{
					p_man = [CCSprite spriteWithFile:@"Grandma_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				default:
					break;
			}
			
			
			
			p_tf2 = [CCSprite spriteWithFile:@"Grandma_tf_ballon_01.png"];
			[self addChild:p_tf2 z:1];
			p_tf2.visible = NO;
			
			p_tf4 = [CCSprite spriteWithFile:@"Grandma_tf_wing_01.png"];
			[self addChild:p_tf4 z:1];
			p_tf4.visible = NO;
			
			
			p_tf5 = [CCSprite spriteWithFile:@"Grandma_dash_01.png"];
			[self addChild:p_tf5 z:1];
			p_tf5.visible = NO;
			
			
			
			p_tf3 = [CCSprite spriteWithFile:@"Grandma_tf_shoe_01.png"];
			[self addChild:p_tf3 z:1];
			p_tf3.visible = NO;
			
			p_tfrain = [CCSprite spriteWithFile:@"Grandma_boltDeath_01.png"];
			[self addChild:p_tfrain z:1];
			p_tfrain.visible = NO;
		
			p_tffrozen = [CCSprite spriteWithFile:@"Grandma_frozen_particle_01.png"];
			[self addChild:p_tffrozen z:1];
			p_tffrozen.visible = NO;
		
		}
			break;	
		case 2:
		{
			switch (m_state)
			{
				case 1:
				{
					p_man = [CCSprite spriteWithFile:@"Rock_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					
				{
					p_man = [CCSprite spriteWithFile:@"Rock_up_03.png"];
					man_pos = convertPoint(ccp(160,p_man.contentSize.height/2));
					p_man.position = man_pos;
				}
					break;
				case 7:
				{
					p_man = [CCSprite spriteWithFile:@"Rock_stand.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 8:
				case 9:	
				{
					p_man = [CCSprite spriteWithFile:@"Rock_up_03.png"];
					man_pos = ccp(160,p_man.contentSize.height/2);
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 10:
				{
					p_man = [CCSprite spriteWithFile:@"Rock_stand.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 11:
					
				{
					p_man = [CCSprite spriteWithFile:@"Rock_up_03.png"];
					man_pos = convertPoint(ccp(160,p_man.contentSize.height/2));
					p_man.position = man_pos;
				}
					break;
				case 12:
				{
					p_man = [CCSprite spriteWithFile:@"Rock_stand.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				default:
					break;
			}
			
			
			p_tf2 = [CCSprite spriteWithFile:@"Rock_tf_ballon_01.png"];
			[self addChild:p_tf2 z:1];
			p_tf2.visible = NO;
			
			p_tf4 = [CCSprite spriteWithFile:@"Rock_tf_wing_01.png"];
			[self addChild:p_tf4 z:1];
			p_tf4.visible = NO;
			
			
			p_tf5 = [CCSprite spriteWithFile:@"Rock_dash_01.png"];
			[self addChild:p_tf5 z:1];
			p_tf5.visible = NO;
			
			
			
			p_tf3 = [CCSprite spriteWithFile:@"Rock_tf_shoe_01.png"];
			[self addChild:p_tf3 z:1];
			p_tf3.visible = NO;
			
			p_tfrain = [CCSprite spriteWithFile:@"Rock_boltDeath_01.png"];
			[self addChild:p_tfrain z:1];
			p_tfrain.visible = NO;
			
			p_tffrozen = [CCSprite spriteWithFile:@"Rock_frozen_particle_01.png"];
			[self addChild:p_tffrozen z:1];
			p_tffrozen.visible = NO;
			
			
		}
			break;	
		
		case 3:
		{
			switch (m_state)
			{
				case 1:
				{
					p_man = [CCSprite spriteWithFile:@"Doc_stand.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				{
					p_man = [CCSprite spriteWithFile:@"Doc_up_03.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 7:
				{
					p_man = [CCSprite spriteWithFile:@"Doc_stand.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 8:
				case 9:	
				{
					p_man = [CCSprite spriteWithFile:@"Doc_up_03.png"];
					man_pos = convertPoint(ccp(160,p_man.contentSize.height/2));
					p_man.position = man_pos;
				}
					break;
				case 10:
				{
					p_man = [CCSprite spriteWithFile:@"Doc_stand.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 11:	
				{
					p_man = [CCSprite spriteWithFile:@"Doc_up_03.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				case 12:
				{
					p_man = [CCSprite spriteWithFile:@"Doc_stand.png"];
					if (isPad) 
					{
						man_pos = ccp(384,p_man.contentSize.height/2+32);
					}
					p_man.position = man_pos;
				}
					break;
				default:
					break;
			}
			
			
			p_tf2 = [CCSprite spriteWithFile:@"Doc_tf_ballon_01.png"];
			[self addChild:p_tf2 z:1];
			p_tf2.visible = NO;
			
			p_tf4 = [CCSprite spriteWithFile:@"Doc_tf_wing_01.png"];
			[self addChild:p_tf4 z:1];
			p_tf4.visible = NO;
			
			
			p_tf5 = [CCSprite spriteWithFile:@"Doc_dash_01.png"];
			[self addChild:p_tf5 z:1];
			p_tf5.visible = NO;
			
			
			
			p_tf3 = [CCSprite spriteWithFile:@"Doc_tf_shoe_01.png"];
			[self addChild:p_tf3 z:1];
			p_tf3.visible = NO;
			
			p_tfrain = [CCSprite spriteWithFile:@"Doc_boltDeath_01.png"];
			[self addChild:p_tfrain z:1];
			p_tfrain.visible = NO;
			
			p_tffrozen = [CCSprite spriteWithFile:@"Doc_frozen_particle_01.png"];
			[self addChild:p_tffrozen z:1];
			p_tffrozen.visible = NO;
			
			
		}	
			
		default:
			break;
	}
	
	
	
	p_tfmoney = [CCSprite spriteWithFile:@"lose_money_01.png"];
	[self addChild:p_tfmoney z:1];
	p_tfmoney.visible = NO;
	
	p_tf = [CCSprite spriteWithFile:@"fly_fart_07.png"];
	[self addChild:p_tf z:1];
	p_tf.visible = NO;
	
	
	p_tftf = [CCSprite spriteWithFile:@"fly_fart_07.png"];
	[self addChild:p_tftf z:1];
	p_tftf.visible = NO;
	
	particleFly = [CCSprite spriteWithFile:@"fly_fart_07.png"];
	[self addChild:particleFly z:1];
	particleFly.visible = NO;
	
	
	particleGhost = [CCSprite spriteWithFile:@"ghost_particle_01.png"];
	[self addChild:particleGhost z:3];
	particleGhost.visible = NO;
	
	particleDash = [CCSprite spriteWithFile:@"dash_fart_01.png"];
	[self addChild:particleDash z:1];
	particleDash.visible = NO;
	
	particleBomb = [CCSprite spriteWithFile:@"bomb_death_01.png"];	
	[self addChild:particleBomb z:1];
	particleBomb.visible = NO;
	
	[self addChild:p_man z:2];
	
		
		
	p_ready = [CCSprite spriteWithFile:@"ready.png"];
	[self addChild:p_ready z:2];
	p_ready.position = convertPoint(ccp(-160,240));
	
	
	p_go = [CCSprite spriteWithFile:@"go.png"];
	[self addChild:p_go z:2];
	p_go.position = convertPoint(ccp(-160,240));
	

}

#pragma mark -
- (void)initpauseUI
{
	item_pause = [CCMenuItemImage itemFromNormalImage:@"pause_button.png"
								   selectedImage:@"pause_button_down.png"
										  target:self
										selector:@selector(pauseGame)];
	item_pause.position = convertPoint(ccp(135,-215));
	[item_pause setIsEnabled:NO];
	CCMenu *p_menu = [CCMenu menuWithItems: item_pause,nil];
	[self addChild:p_menu z:3];
	if (isPad) {
		p_menu.position = ccp(384-60,512-20);
	}
		
	
}

#pragma mark -
- (void)initItem
{
	
	tagdash = [CCSprite spriteWithFile:@"tagdash.png"];
	[tagdash setPosition:convertPoint(ccp(28,25))];
	[self addChild:	tagdash z:5];
	tagdash.visible = NO;
	
	
	tagwing = [CCSprite spriteWithFile:@"tagwing.png"];
	[tagwing setPosition:convertPoint(ccp(28,25))];
	[self addChild:	tagwing z:5];
	tagwing.visible = NO;
	
	tagballon = [CCSprite spriteWithFile:@"tagballon.png"];
	[tagballon setPosition:convertPoint(ccp(28,25))];
	[self addChild:	tagballon z:5];
	tagballon.visible = NO;
	
	tagshoe = [CCSprite spriteWithFile:@"tagshoe.png"];
	[tagshoe setPosition:convertPoint(ccp(28,25))];
	[self addChild:	tagshoe z:5];
	tagshoe.visible = NO;
	
	CCSprite *p_money_icon = [CCSprite spriteWithFile:@"money_icon.png"];
	[self addChild:p_money_icon z:3];
	p_money_icon.position = convertPoint(ccp(236,457));
	
	CCSprite *p_height_icon = [CCSprite spriteWithFile:@"height_icon.png"];
	[self addChild:p_height_icon z:3];
	p_height_icon.position = convertPoint(ccp(20,458));
	
	if (isPad) 
	{
		hightLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"height_number.png" itemWidth:32 itemHeight:78 startCharMap:'.'];
 	}
	else
	{
		hightLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"height_number.png" itemWidth:16 itemHeight:39 startCharMap:'.'];
	}
	hightLabel.position = convertPoint(ccp(39,429));
	[self addChild:hightLabel z:3 tag:kHightScoreLabel];
	
	
	if (isPad) 
	{
		numLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:32 itemHeight:78 startCharMap:'.'];
 	}
	else
	{
		numLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number.png" itemWidth:16 itemHeight:39 startCharMap:'.'];
 	}
	numLabel.position = convertPoint(ccp(265,429));
 	[self addChild:numLabel z:3 tag:kNumLabel];

 
	switch (m_state)
	{
		case 1:
		case 2:
		case 3:	
			
			break;
		case 4:
		case 5:
		case 6:	
		{
			emitter = [CCParticleRain node];
			[self addChild: emitter z:2];
			if (!isPad) 
			{
				emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"rains.png"];
				emitter.emissionRate = 100;	
			}
			else 
			{
				emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"rains-ipad.png"];	
				emitter.emissionRate = 80;	
			}

			
			emitter.visible = NO;
			emitter.speed = 520;
			emitter.startSize = 15.0;
			emitter.startSizeVar = 10.0;
			emitter.endSize = 15.0;
			emitter.endSizeVar = 10.0;
		}
			break;
			
		case 7:
		case 8:
		case 9:	
		case 10:
			
			break;
		case 11:	
		{
			emitter = [CCParticleRain node];
			[self addChild: emitter z:2];
			if (!isPad) 
			{
				emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"rains.png"];
				emitter.emissionRate = 100;	
			}
			else 
			{
				emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"rains-ipad.png"];	
				emitter.emissionRate = 80;	
			}
			emitter.visible = NO;
			emitter.speed = 520;
			emitter.startSize = 15.0;
			emitter.startSizeVar = 10.0;
			emitter.endSize = 15.0;
			emitter.endSizeVar = 10.0;
		}
			break;
	
		default:
			break;
	}
	tagIcon = [CCSprite spriteWithFile:@"tag_01.png"];
	[self addChild:tagIcon z:2];
	tagIcon.position = convertPoint(ccp(160,458));
	tagIcon.visible = NO;
	
	
	
	
	
}

#pragma mark -
- (void)initProp
{
	[self reloadEasy11Prop:0];
	[self reloadEasy1Prop:1];

}


#pragma mark -
- (void)loading
{
	if (isPad) 
	{
		switch (m_x1)
		{
			case 0:
			case 48:
			{
				[self initMap];
				
				
			}
				break;
			case 96:
			{
				 [self initpauseUI];
				
			}
				break;
			case 144:
			{
				 [self initItem];
				
			}
				break;
				
			case 192:
			{
				 [self initMan];
				
			}
				break;
			case 240:
			{
				 [self initProp];
				
			}
				break;
			case 288:
			case 336:
			case 384:
				
				break;
			default:
				break;
		}
		
		return;	
	}
	
	
	switch (m_x1)
	{
		case 0:
		case 24:
		{
			[self initMap];
			
			
		}
			break;
		case 48:
		{
			[self initpauseUI];
			
		}
			break;
		case 72:
		{
			[self initItem];
			
		}
			break;

		case 96:
		{
			[self initMan];
			
		}
			break;
		case 120:
		{
			[self initProp];
			
		}
			break;
		case 144:
		case 168:
		case 192:

			break;
		default:
			break;
	}

     

}

- (void)releaseLighting:(id)sender
{
	CCColorLayer *colorLayer = (CCColorLayer*)sender;
	colorLayer.visible = NO;
	[self removeChild:colorLayer cleanup:YES];
}

- (void)randomLightingAnim: (ccTime)t 
{
	[self unschedule:@selector(randomLightingAnim:)];
	
	if ((m_state >=4 && m_state <= 6) || m_state == 11)
	{
		// 闪电效果
		CCColorLayer *colorLayer = [CCColorLayer layerWithColor:ccc4(255,255,255,255)];
		//CCSprite* pSpriteLight = [CCSprite spriteWithFile:@"Lightning_witer.png"];
		//colorLayer.position  = convertPoint(ccp(160,240));
		[self addChild:colorLayer z:10];
		[[SimpleAudioEngine sharedEngine] playEffect:@"lightting.aif"];
		id FadeInAction = [CCFadeIn actionWithDuration:0.25f];
		id FadeOutAction = [CCFadeOut actionWithDuration:0.25f];
		CCCallFuncN *callback = [CCCallFuncN actionWithTarget:self selector:@selector(releaseLighting:)];
		[colorLayer runAction:[CCSequence actions:FadeInAction,FadeOutAction,callback,nil]];
		
		[self schedule:@selector(randomLightingAnim:) interval:rand()%10+5];	
	}
}


#pragma mark -
- (void)gameReady
{
	//[self unschedule:@selector(updateGameResource:)];
	
	item_tap.visible = YES;
	[item_tap setIsEnabled:YES];
	
	[p_tap runAction:[CCRepeatForever actionWithAction:[CCBlink actionWithDuration:1.0 blinks:1]]];
}
	 
#pragma mark -
- (void)gameStart:(id)sender
{
	[self removeChild:p_loading_bg cleanup:YES];
	[self removeChild:p_loading_teeth cleanup:YES];
	[self removeChild:p_loading_mouth cleanup:YES];
	[self removeChild:p_loading_eyes cleanup:YES];
	[self removeChild:tapmenu cleanup:YES];
	[self removeChild:item_tap cleanup:YES];
	[self removeChild:p_tap cleanup:YES];
	[item_pause setIsEnabled:YES];
	emitter.visible = YES;
	
	self.isAccelerometerEnabled = YES;
	
	[self schedule:@selector(randomLightingAnim:) interval:rand()%10+5];

	
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0/60.0)];
	
	[self schedule:@selector(logic:)];
	[p_ready runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.4 position:convertPoint(ccp(160,240))],
						[CCDelayTime actionWithDuration:0.5],
						[CCMoveTo actionWithDuration:0.4 position:convertPoint(ccp(480,240))],
						[CCCallFunc actionWithTarget:self selector:@selector(step)],nil]];
	[[SimpleAudioEngine sharedEngine] playEffect:@"ready.aif"];
	
	switch (m_state)
	{
		case 1:	
		case 2:
		case 3:
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"stage1.aif" loop:YES]; 	
			break;
		case 4:
		case 5:
		case 6:	
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"stage2.aif" loop:YES];
			
			break;
		case 7:
		case 8:
		case 9:	
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"stage3.aif" loop:YES];
			break;
		case 10:
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"stage1.aif" loop:YES]; 	
			break;
		case 11:
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"stage2.aif" loop:YES];	
			
			break;
		case 12:
			[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"stage3.aif" loop:YES];	
			break;	
			
		default:
			break;
	}

}

#pragma mark -
- (void)step
{

	[self removeChild:p_ready cleanup:YES];
	[p_go runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.4 position:convertPoint(ccp(160,240))],
					 [CCDelayTime actionWithDuration:0.5],
					 [CCMoveTo actionWithDuration:0.4 position:convertPoint(ccp(480,240))],
					 [CCCallFunc actionWithTarget:self selector:@selector(readyBody)],nil]];
	[[SimpleAudioEngine sharedEngine] playEffect:@"go.aif"];


}
#pragma mark -
- (void)readyBody
{
	gameWillStart = YES;
	[self removeChild:p_go cleanup:YES];
	[p_man stopAllActions];
	
	
	switch (m_state)
	{
		case 1:
		
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
				}
					
					break;
				case 1:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
					
				case 2:
			
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
				case 3:
			
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
				default:
					break;
			}
		
		}
			break;
		case 2:
		case 3:	
		case 4:
		case 5:	
		case 6:
		{
			[self startGame];
		}
			break;
	
		case 7:
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
				}
					
					break;
				case 1:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
					
				case 2:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
				case 3:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
				default:
					break;
			}
		}
			break;
			
		case 8:
		case 9:	
		{
			[self startGame];
		}
			break;		
		case 10:
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
				}
					
					break;
				case 1:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
					
				case 2:
					
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
				case 3:
					
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++)   
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
				default:
					break;
			}
		}
			break;
		case 11:
		{
			[self startGame];
		}
			break;
		case 12:
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
				}
					
					break;
				case 1:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++)  
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
					
				case 2:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
				case 3:
				{
					CCAnimation *p_readybody = [CCAnimation animationWithName:@"ready" delay:0.2];
					for (int i=1; i<4; i++) 
					{
						[p_readybody addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					id actionready = [CCSequence actions:[CCAnimate actionWithAnimation:p_readybody restoreOriginalFrame:NO],[CCCallFunc actionWithTarget:self selector:@selector(startGame)],nil];
					[p_man runAction:actionready];
					
				}
					break;
					
				default:
					break;
			}
		
		}
			break;
	
	
		default:
			break;
	}
	
	
		
}



#pragma mark -
- (void)startGame
{
	gameSuspended = NO;
	
	m_hightscore = 0;
	m_moneyscore = 0;
	m_hamburgcount = 0;
	[self rushEffect];
	[self resetMan];
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	
}

#pragma mark -
- (void)updateGameResource: (ccTime)t 
{
	m_x1 += 24;
	m_x2 += 12;
	
	int nEndPosition = 192;
	if (isPad) {
		nEndPosition = 192*2;
		m_x1 += 24;
	}
	
	if (m_x1 <= nEndPosition)
	{
		[p_loading_teeth setTextureRect: CGRectMake(0,0,m_x1+24,62)];
		if (isPad) {
			[p_loading_teeth setTextureRect: CGRectMake(0,0,m_x1+24*2,62*2)];
		}
		[p_loading_teeth setPosition:convertPoint(ccp(64+m_x2,215))];
		[[SimpleAudioEngine sharedEngine] playEffect:@"loading.aif"];
		[self loading];
	}
	else
	{
		[self unschedule:_cmd];
		[self gameReady];
	}
}

#pragma mark -
- (void)resetMan //重置人物
{
	switch (m_state)
	{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:	

		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:	
		{
			man_vel.x = 0.0f;
			
			
			man_acc.x = 0.0f;
			if (isPad)
			{
				man_acc.y = -1000;//-550.0f;
				man_vel.y = 1600.0f;
			}
			else
			{
				man_acc.y = -550/2;//-550.0f;
				man_vel.y = 800.0f;
			}

			
		}
			break;
	
		default:
			break;
	}
    
}


#pragma mark -
- (void)logic: (ccTime)dt 
{
	
	if (gameSuspended)
	{
		if(man_vel.x<-10.0f && manLookingRight && !gameWillStart) 
		{
			[p_man stopAllActions];
			manLookingRight = NO;
			p_man.scaleX = 1.0f;
			manWalking = YES;
			
			switch (m_state)
			{
				case 1:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
				}
					break;
				
				case 2:
				{
				
				}
					break;
				case 3:		
				{
					
				}
					break;
				case 4:
				{
					
				}
					break;
				case 5:
				{
					
				}
					break;
				case 6:	
				{
					
				}
				
					break;
				case 7:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
				
				}
					break;
				case 8:	
				{
					
				}
					
					break;	
				case 9:	
				{
					
				}
					
					break;	
				case 10:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
				
				}
					break;
	            case 11:
				{
				
				}
					break;
				case 12:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
					
				}
					break;
	
					
				default:
					break;
			}
			

			
		} 
		else if(man_vel.x>10.0f && !manLookingRight && !gameWillStart) 
		{
			[p_man stopAllActions];
			manLookingRight = YES;
			p_man.scaleX = -1.0f;
			manWalking = YES;
			
			switch (m_state)
			{
				case 1:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
				}
					break;
					
				case 2:
				{
					
				}
					break;
				case 3:		
				{
					
				}
					break;
				case 4:
				{
					
				}
					break;
				case 5:
				{
					
				}
					break;
				case 6:	
				{
					
				}
					
					break;
				case 7:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
					
				}
					break;
				case 8:	
				{
					
				}
					
					break;	
				case 9:	
				{
					
				}
					
					break;	
					
				case 10:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
					
				}
					break;	
				case 11:
				{
					
				}
					break;
				case 12:
				{
					switch (m_player)
					{
						case 0:
						{
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Bro_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 1:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Grandma_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 2:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Rock_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						case 3:
						{   
							CCAnimation *p_walkL = [CCAnimation animationWithName:@"walk" delay:0.05f];
							for (int i=1; i<8; i++ ) 
							{
								[p_walkL addFrameWithFilename:[NSString stringWithFormat:@"Doc_walk_%02d.png", i]];
							}
							id action1 = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:p_walkL restoreOriginalFrame:YES]];
							[p_man runAction:action1];
						}
							break;
						default:
							break;
					}
					
				}
					break;
					
					
				default:
					break;
			}
			

			
		}
		
		//行走中
		if (manWalking) 
		{
			p_man.position = man_pos;
			man_pos.x += man_vel.x * dt;
		}
		//边界处
		if (!isPad) 
		{
			if (man_pos.x > 320+p_man.contentSize.width/2)
			{
				man_pos.x = 0;
			}
			else if (man_pos.x < -p_man.contentSize.width/2)
			{
				man_pos.x = 320;
			}
		}
		else 
		{
			if (man_pos.x > (768+p_man.contentSize.width/2))
			{
				man_pos.x = 64;
			}
			else if (man_pos.x < (64-p_man.contentSize.width/2))
			{
				man_pos.x = 768-64;
			}
		}

	} 
	else
	{
//		if((man_vel.x < -300.0f ||  man_vel.x > 300.0f) && !manFly && !manFlow)
//		{
//			m_judge++;
//			if (m_judge > 600)
//			{
//				[self showGameover];
//			}
//			
//		}
		
		CGSize man_size = p_man.contentSize;
		float max_x = 320+man_size.width/2;
		float min_x = -man_size.width/2;
		if (isPad) 
		{
			max_x = 768+man_size.width/2;
			min_x = 64-man_size.width/2;
		}
		
		if(man_pos.x>max_x) man_pos.x = min_x;
		if(man_pos.x<min_x) man_pos.x = max_x;
		
		if (manGhost && (!manRain || !manFrozen))
		{
			man_pos.x -= man_vel.x * dt;
			man_vel.y += man_acc.y * dt;
			man_pos.y += man_vel.y * dt;
		}
		

		if ((manRain || manFrozen) && manGhost)
		{
		   man_vel.x = 0; 	
		   man_pos.x += man_vel.x * dt;
		   man_pos.y += 0;
		}
		
		if (!manRain && !manFrozen && !manGhost)
		{
			man_vel.y += man_acc.y * dt;
			man_pos.y += man_vel.y * dt;
			man_pos.x += man_vel.x * dt;
		}


		
		if(man_vel.x < -30.0f && manLookingRight && !manDash &&!manFly&& !manDead) 
		{
			manLookingRight = NO;
			p_man.scaleX = 1.0;
			
			
			p_tf3.scaleX = 1.0;
			p_tf4.scaleX = 1.0;
			p_tf5.scaleX = 1.0;
		
			p_tfrain.scaleX = 1.0;
			p_tffrozen.scaleX = 1.0;
		}
		if (man_vel.x > 30.0f && !manLookingRight && !manDash &&!manFly && !manDead) 
		{
			manLookingRight = YES;
			p_man.scaleX = -1.0;
		
			p_tf3.scaleX = -1.0;
			p_tf4.scaleX = -1.0;
			p_tf5.scaleX = -1.0;
			
			p_tfrain.scaleX = -1.0;
			p_tffrozen.scaleX = -1.0;
		}

		
		//道具的碰撞
		
		for(CCSprite *props in p_node0_array)
		{
			
			CGRect rect = CGRectMake(props.position.x,props.position.y+p_node_0.position.y,props.contentSize.width,props.contentSize.height);
			CGRect manrect = CGRectMake(man_pos.x,man_pos.y,p_man.contentSize.width/2,p_man.contentSize.height/2);
			if (CGRectIntersectsRect(rect, manrect))
			{
				
				
				switch (props.tag)
				{
					
					case kBomb:
					{
						if (props.visible == YES)
						{
							if (manFly)
							{
								m_flyTime = 9999;
							}
							
							if (manFlow)
							{
								m_flowTime = 9999;
							}
							
							if (manWing)
							{
								m_wingTime = 9999;
							}
							
							if (manShoes)
							{
								m_shoesTime = 9999;
							}
							
							props.visible = NO;	
							
							[self bombEffect];
							bombcount++;
						}
						
						
					}
						break;		
				
						
					case kFrozen:
					{
						if (props.visible == YES)
						{
							
							if(!manDash && !manFly)
							{
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								[self frozenEffect];
								frozencount++;
								
							}
							
							props.visible = NO;
							CCSprite *p_miss = [CCSprite spriteWithFile:@"frozen_disappear01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.02f];
								for (int i=1; i<6; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"frozen_disappear%02d.png", i]];
								}
								id actionBlock = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBlock,spriteDone,nil];
								
								[p_miss runAction:sequence];
							}
							
						
						}
						
						
						
					}	
						break;	
						
					case kBlock:
					{
						if (props.visible == YES)
						{
							
							if(!manDash && !manFly)
							{
								
								if (man_vel.y > 0)
								{
									props.visible = NO;
									if (manFlow)
									{
										m_flowTime = 9999;
									}
									
									if (manWing)
									{
										m_wingTime = 9999;
									}
									
									if (manShoes)
									{
										m_shoesTime = 9999;
									}
									[self blockEffect];
									blockcount++;
									
									
								}
								
						        else
								{
									props.visible = NO;
									if (manFlow)
									{
										m_flowTime = 9999;
									}
									
									if (manWing)
									{
										m_wingTime = 9999;
									}
									
									if (manShoes)
									{
										m_shoesTime = 9999;
									}
									[self jumpagainEffect];
									hatcount++;
								}
                              
								
							}
							[[SimpleAudioEngine sharedEngine] playEffect:@"Block.aif"];
							CCSprite *p_miss = [CCSprite spriteWithFile:@"block_miss_01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.02f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"block_miss_%02d.png", i]];
								}
								id actionBlock = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBlock,spriteDone,nil];
								
								[p_miss runAction:sequence];
							}
							
						}
					}
	
						break;		
		
					case kGhost:
					{
						if (props.visible == YES)
						{
							if(!manDash && !manFly)
							{
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								[self ghostEffect];
								ghostcount++;
							}
							
							props.visible = NO;
							CCSprite *p_miss = [CCSprite spriteWithFile:@"ghost_miss_01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = ccp(p_man.position.x,props.position.y+60);
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.02f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"ghost_miss_%02d.png", i]];
								}
								id actionBlock = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBlock,spriteDone,nil];
								
								[p_miss runAction:sequence];
							}
							[[SimpleAudioEngine sharedEngine] playEffect:@"DeathGhost.aif"];
							
						}
						
						
						
					}	
						break;		
						
						
					case kDash:
					{
						if (props.visible == YES)
						{
							if(!manDash && !manFly)
							{
								
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								
								
								[self dashEffect];
								dashcount++;
							}
							
							props.visible = NO;
						}
						
					}
						
						break;
		
					case kRain:
					{
						if (props.visible == YES)
						{
							if(!manDash && !manFly)
							{
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								m_moneyscore -= 5;
								if (m_moneyscore<0)
								{
									m_moneyscore = 0;
								}
								[self rainEffect];
								boltcount++;
							}
							
							
							props.visible = NO;
							CCSprite *p_miss = [CCSprite spriteWithFile:@"bolt_miss_01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							
							
							
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"bolt_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
							}

							
						}
		
					}	
						break;	
						
					case kHamburg:
					{
						
						   
						if (props.visible == YES)
						{
							[self moneyEffect];
							props.visible = NO;
							if(man_vel.y > 0)
							{
								if (manFly  || manFlow || manDash)
								{
									
									
									
								}
								else 
								{
									
									[self jumpEffect];
								}
								
								
							}
							
							if(man_vel.y < 0)
							{
								
								[self jumpagainEffect];
							}
							if (!manFly) 
							{
								m_hamburgcount++;
							}
							
							
						 
							
							CCSprite *p_miss = [CCSprite spriteWithFile:@"hamburg_miss_01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							
							
							
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<6; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"hamburg_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
							}
							
						}
						
						
						
						
					}
						break;
						
					case kMoney:
					{	
						if (props.visible == YES)
						{
							props.visible = NO;
							[[SimpleAudioEngine sharedEngine] playEffect:@"money.aif"];
							CCSprite *p_miss = [CCSprite spriteWithFile:@"money_miss_01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							
							
							if (!manFly)
							{
								m_moneyscore += 1;
								
							}
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<6; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"money_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
								
								
								
							}
							
						}
	
						
					}
						break;
						
					case kBag:
					{	
						if (props.visible == YES)
						{
							props.visible = NO;
							
							CCSprite *p_miss = [CCSprite spriteWithFile:@"bag_miss_01.png"];
							[p_node_0 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							
							
							[[SimpleAudioEngine sharedEngine] playEffect:@"bag.aif"];
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"bag_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
								m_moneyscore += 10;
							}
							
						}

					}
						break;
						
					case kWing:
					{
						if (props.visible == YES)
						{
							if (!manWing && !manFly)
							{
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								if (manDash)
								{
									m_dashTime = 9999;
								}
								
								
								[self wingEffect];
								wingcount++;
							}
							props.visible = NO;
							
						}
						
						
					}
						break;
						
					case kShoes:
					{
						if (props.visible == YES)
						{
							
							if (!manShoes && !manFly)
							{
								
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manDash)
								{
									m_dashTime = 9999;
								}
								
								
								[self shoesEffect];
								shoecount++;
							}
							props.visible = NO;
						}
						
					}
						break;
						
					case kBallon:
					{
						if (props.visible == YES)
						{
							if (!manFlow && !manFly)
							{
								
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								
								if (manDash)
								{
									m_dashTime = 9999;
								}
								
								[self flowEffect];
								balloncount++;
							}
							
							
							props.visible = NO;
							
						}
						
						
					}
						break;
						
						
					default:
						break;
				}
				
				
				
			}	
			
		}
		
		
		for(CCSprite *props in p_node1_array)
		{
			
			CGRect rect = CGRectMake(props.position.x,props.position.y+p_node_1.position.y,props.contentSize.width,props.contentSize.height);
			CGRect manrect = CGRectMake(man_pos.x,man_pos.y,p_man.contentSize.width/2,p_man.contentSize.height/2);
			if (CGRectIntersectsRect(rect, manrect))
			{
				
				
				switch (props.tag)
				{
			
					case kBomb:
					{
						if (props.visible == YES)
						{
							if (manFly)
							{
								m_flyTime = 9999;
							}
							
							if (manFlow)
							{
								m_flowTime = 9999;
							}
							
							if (manWing)
							{
								m_wingTime = 9999;
							}
							
							if (manShoes)
							{
								m_shoesTime = 9999;
							}
							
							props.visible = NO;	
							
							[self bombEffect];
							bombcount++;
						}
						
						
					}
						break;
					case kFrozen:
					{
						if (props.visible == YES)
						{
							if(!manDash && !manFly)
							{
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								[self frozenEffect];
								frozencount++;
								
							}
							
							props.visible = NO;
							CCSprite *p_miss = [CCSprite spriteWithFile:@"frozen_disappear01.png"];
							[p_node_1 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.02f];
								for (int i=1; i<6; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"frozen_disappear%02d.png", i]];
								}
								id actionBlock = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone1:)];
								id sequence = [CCSequence actions:actionBlock,spriteDone,nil];
								
								[p_miss runAction:sequence];
							}
							
							
						}
						
						
						
					}	
						break;		
						
					case kGhost:
					{
						
						if(!manDash && !manFly)
						{
							if (manFlow)
							{
								m_flowTime = 9999;
							}
							
							if (manWing)
							{
								m_wingTime = 9999;
							}
							
							if (manShoes)
							{
								m_shoesTime = 9999;
							}
							[self ghostEffect];
							ghostcount++;
							
						}
						
						if (props.visible == YES)
						{
							props.visible = NO;
							CCSprite *p_miss = [CCSprite spriteWithFile:@"ghost_miss_01.png"];
							[p_node_1 addChild:p_miss z:3];
							p_miss.position = ccp(p_man.position.x,props.position.y+60);
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.02f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"ghost_miss_%02d.png", i]];
								}
								id actionBlock = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone1:)];
								id sequence = [CCSequence actions:actionBlock,spriteDone,nil];
								
								[p_miss runAction:sequence];
							}
							[[SimpleAudioEngine sharedEngine] playEffect:@"DeathGhost.aif"];
							
						}

						
					}	
						break;		
						
					case kBlock:
					{
						if (props.visible == YES)
						{
							 
							if(!manDash && !manFly)
							{
								
								if (man_vel.y > 0)
								{ 
									props.visible = NO;
									if (manFlow)
									{
										m_flowTime = 9999;
									}
									
									if (manWing)
									{
										m_wingTime = 9999;
									}
									
									if (manShoes)
									{
										m_shoesTime = 9999;
									}
									
									[self blockEffect];
									blockcount++;
									
								}
								
						        else
								{
									props.visible = NO;
									if (manFlow)
									{
										m_flowTime = 9999;
									}
									
									if (manWing)
									{
										m_wingTime = 9999;
									}
									
									if (manShoes)
									{
										m_shoesTime = 9999;
									}
									[self jumpagainEffect];
									hatcount++;
									
								}
								
								
							}
							
							[[SimpleAudioEngine sharedEngine] playEffect:@"Block.aif"];
							CCSprite *p_miss = [CCSprite spriteWithFile:@"block_miss_01.png"];
							[p_node_1 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.02f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"block_miss_%02d.png", i]];
								}
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone1:)];
								id actionBlock = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id sequence = [CCSequence actions:actionBlock,spriteDone,nil];
								
								[p_miss runAction:sequence];
							}
						}
					}						
						break;
						
						
					case kDash:
					{
						if (props.visible == YES)
						{
							if(!manDash && !manFly)
							{
								
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								
								
								[self dashEffect];
								dashcount++;
							}
							
							props.visible = NO;
						}
						
					}
						
						break;

						
						
					case kRain:
					{
						
							if (props.visible == YES)
							{
								if(!manDash && !manFly)
								{
									if (manFlow)
									{
										m_flowTime = 9999;
									}
									
									if (manWing)
									{
										m_wingTime = 9999;
									}
									
									if (manShoes)
									{
										m_shoesTime = 9999;
									}
						
									[self rainEffect];
									boltcount++;
									
									m_moneyscore -= 5;
									if (m_moneyscore<0)
									{
										m_moneyscore = 0;
									}
									
								}
								
								
								
								props.visible = NO;
								CCSprite *p_miss = [CCSprite spriteWithFile:@"bolt_miss_01.png"];
								[p_node_1 addChild:p_miss z:3];
								p_miss.position = props.position;
								
								if ([p_miss numberOfRunningActions] == 0) 
								{
									p_miss.visible = YES;
									CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
									for (int i=1; i<7; i++)
									{
										[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"bolt_miss_%02d.png", i]];
									}
									id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
									id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
									id sequence = [CCSequence actions:actionBad, spriteDone, nil];
									
									[p_miss runAction:sequence];
								}
								
							}
							
							
							
						
						
						
						
					}
						break;	
						
						
					case kHamburg:
					{
						
						
						if (props.visible == YES)
						{
							[self moneyEffect];
							props.visible = NO;
							if(man_vel.y > 0)
							{
								if (manFly  || manFlow || manDash)
								{
									
									
									
								}
								else 
								{
									
									[self jumpEffect];
								}
								
								
							}
							
							if(man_vel.y < 0)
							{
								
								[self jumpagainEffect];
							}
							if (!manFly) 
							{
								m_hamburgcount++;
							}
							
							
							
							
							CCSprite *p_miss = [CCSprite spriteWithFile:@"hamburg_miss_01.png"];
							[p_node_1 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							
							
							
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<6; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"hamburg_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
							}
							
						}
						
						
						
						
					}
						break;
						
					case kMoney:
					{	
						if (props.visible == YES)
						{
							props.visible = NO;
							
							[[SimpleAudioEngine sharedEngine] playEffect:@"money.aif"];
							
							CCSprite *p_miss = [CCSprite spriteWithFile:@"money_miss_01.png"];
							[p_node_1 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							if (!manFly)
							{
								m_moneyscore += 1;
								
							}
							
							
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<6; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"money_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
								
								
								
							}
							
						}
						
						
						
						
						
						
					}
						break;
						
					case kBag:
					{	
						if (props.visible == YES)
						{
							props.visible = NO;
							
							CCSprite *p_miss = [CCSprite spriteWithFile:@"bag_miss_01.png"];
							[p_node_1 addChild:p_miss z:3];
							p_miss.position = props.position;
							
							[[SimpleAudioEngine sharedEngine] playEffect:@"bag.aif"];
							
							
							
							if ([p_miss numberOfRunningActions] == 0) 
							{
								p_miss.visible = YES;
								CCAnimation *p_disappear = [CCAnimation animationWithName:@"disappear" delay:0.1f];
								for (int i=1; i<7; i++)
								{
									[p_disappear addFrameWithFilename:[NSString stringWithFormat:@"bag_miss_%02d.png", i]];
								}
								id actionBad = [CCAnimate actionWithAnimation:p_disappear restoreOriginalFrame:NO];
								id spriteDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteDone:)];
								id sequence = [CCSequence actions:actionBad, spriteDone, nil];
								
								[p_miss runAction:sequence];
								m_moneyscore += 10;
							}
							
						}
						
						
					}
						break;
						
					case kWing:
					{
						if (props.visible == YES)
						{
							if (!manWing && !manFly)
							{
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								if (manDash)
								{
									m_dashTime = 9999;
								}
								
								
								[self wingEffect];
								wingcount++;
							}
							props.visible = NO;
							
						}
						
						
					}
						break;
						
					case kShoes:
					{
						if (props.visible == YES)
						{
							
							if (!manShoes && !manFly)
							{
								
								if (manFlow)
								{
									m_flowTime = 9999;
								}
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manDash)
								{
									m_dashTime = 9999;
								}
								
								
								[self shoesEffect];
								shoecount++;
							}
							props.visible = NO;
						}
						
					}
						break;
						
					case kBallon:
					{
						if (props.visible == YES)
						{
							if (!manFlow && !manFly)
							{
								
								
								if (manWing)
								{
									m_wingTime = 9999;
								}
								
								if (manShoes)
								{
									m_shoesTime = 9999;
								}
								
								if (manDash)
								{
									m_dashTime = 9999;
								}
								
								[self flowEffect];
								balloncount++;
							}
							
							
							props.visible = NO;
							
						}
						
						
					}
						break;
						
						
					default:
						break;
				}
				
				
				
			}	
			
		}
		
		if (manFlow)
		{
			m_flowTime++;
			man_vel.y = 200.0f;
			if (isPad)
			{
				man_vel.y = 400;
				
			}
			[p_tf2 setPosition:ccp(p_man.position.x,p_man.position.y)];
			if (m_flowTime > 500)
			{
				
				
				manFlow = NO;
				m_flowTime = 0;
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				if (manFly || manDash || manShoes|| manWing)
				{
					[p_man stopAllActions];
					tagballon.visible = NO;
					[self removeChild:p_power_ballon cleanup:YES];
				}
				
				else
				{
					[p_man stopAllActions];
					tagballon.visible = NO;
					[self removeChild:p_power_ballon cleanup:YES];
				}

				
			}
			
		}
		

		
		
		if (manDash)
		{
			p_man.scaleX = 1.0;
			[p_tf5 setPosition:ccp(p_man.position.x,p_man.position.y)];
			[particleDash setPosition:ccp(p_man.position.x,p_man.position.y - 50)];
			man_vel.y = 800.0f;
			if (isPad)
			{
				man_vel.y = 1600.0f ;
				
			}
			m_dashTime++;
			if (m_dashTime > kMaxFlowTime/3)
			{
				
				manDash = NO;
				m_dashTime = 0;
				particleDash.visible = NO;
				if (manFly || manFlow || manShoes || manWing)
				{
					[p_man stopAllActions];
					tagdash.visible = NO;
					[self removeChild:p_power_dash cleanup:YES];
				}
				
				else
				{
					[p_man stopAllActions];
					tagdash.visible = NO;
					[self removeChild:p_power_dash cleanup:YES];
				}
				
			} 
			
		}

		
		if (manFly)
		{
			man_vel.y = 800.0f;
			if (isPad)
			{
				man_vel.y = 1600.0f ;
				
			}
			p_man.scaleX = 1.0;
			[p_tf setPosition:ccp(p_man.position.x,p_man.position.y)];
			[p_tftf setPosition:ccp(p_man.position.x-1,p_man.position.y-85)];
			if (isPad)
			{
				[particleFly setPosition:ccp(p_man.position.x+1,p_man.position.y - 260)];
 			}
			else 
			{
				[particleFly setPosition:ccp(p_man.position.x+1,p_man.position.y - 130)];
 			}

			m_flyTime ++;
	
			
			if (m_flyTime > kMaxFlowTime/2) 
			{
				
				manFly = NO;
				m_flyTime = 0;
				p_tftf.visible = NO;
				p_tf2.visible = NO;
				p_tf4.visible = NO;	
				p_tf3.visible = NO;
				
				[p_tf2 stopAllActions];
				[p_tf3 stopAllActions];
				[p_tf4 stopAllActions];
				particleFly.visible = NO;
				[particleFly stopAllActions];
				
				tagdash.visible = NO;
				[self removeChild:p_power_dash cleanup:YES];
				
				tagshoe.visible = NO;
				[self removeChild:p_power_shoe cleanup:YES];
				
				tagwing.visible = NO;
				[self removeChild:p_power_wing cleanup:YES];
				
				tagballon.visible = NO;
				[self removeChild:p_power_ballon cleanup:YES];
				
			
				
				if (manDash|| manFlow || manShoes|| manWing)
				{
					
					[self jumpEffect];
				}
				
				else
				{
					[self jumpEffect];
					
				}

			}
		    
		}

		if (manWing)
		{
			m_wingTime ++;
			[p_tf4 setPosition:ccp(p_man.position.x,p_man.position.y)];
			
			if (m_wingTime > kMaxFlowTime*2) 
			{
				
				manWing = NO;
				m_wingTime = 0;	
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;

				}
				if (manDash|| manFlow || manShoes|| manFly)
				{
					[p_man stopAllActions];
					tagwing.visible = NO;
					[self removeChild:p_power_wing cleanup:YES];
				}
				
				else
				{
					[p_man stopAllActions];
					tagwing.visible = NO;
					[self removeChild:p_power_wing cleanup:YES];
				}
				
			}
			
			if (man_vel.y < 0)
			{
				man_vel.y = 0;
			}
			
		}
		
		if (moneys)
		{
			m_moneyTime++;
			if (m_moneyTime > 240)
			{
				moneys = NO;
				m_moneyTime = 0;
			}
			
		}
		

       if (manGhost)
	   {
		   [particleGhost setPosition:ccp(p_man.position.x,p_man.position.y)];
		   m_ghostTime++;
		   if (m_ghostTime > kMaxFlowTime/5) 
		   {
			   
			   manGhost = NO;
			   particleGhost.visible = NO;
			   m_ghostTime = 0;	
			   
		   }
		
	   }
		
		if (manRain)
		{
	
	     	[p_tfrain setPosition:ccp(p_man.position.x,p_man.position.y)];	
			p_tf2.visible = NO;
			p_tf4.visible = NO;	
			p_tf3.visible = NO;
			
			[p_tf2 stopAllActions];
			[p_tf3 stopAllActions];
			[p_tf4 stopAllActions];
	
		}
//		

		
		if (manShoes)
		{
			m_shoesTime ++;
			
			[p_tf3 setPosition:ccp(p_man.position.x,p_man.position.y)];
			
			if (m_shoesTime > kMaxFlowTime*2) 
			{
				
				manShoes = NO;
				man_vel.y = 200;
				if (isPad)
				{
					man_vel.y = 400.0f ;
					
				}
				m_shoesTime = 0;	
				if (manDash|| manFlow || manWing|| manFly)
				{
					[p_man stopAllActions];
					tagshoe.visible = NO;
					[self removeChild:p_power_shoe cleanup:YES];
				}
				
				else
				{
					[p_man stopAllActions];
					tagshoe.visible = NO;
					[self removeChild:p_power_shoe cleanup:YES];
				}
				
			}
			
		}

		if (manFrozen)
		{
			[p_tffrozen setPosition:ccp(p_man.position.x,p_man.position.y)];
			p_tf2.visible = NO;
			p_tf4.visible = NO;	
			p_tf3.visible = NO;
			
		   [p_tf2 stopAllActions];
			[p_tf3 stopAllActions];
			[p_tf4 stopAllActions];
			
		}


		
		if(man_vel.y <= 0)
		{
			manrush = NO;

			p_BGSpeed.speed = 5;
			//p_MDSpeed.speed = 5;
			p_FRSpeed.speed = 5;
			p_LT2Speed.speed = 5;
			if (willstop == NO) 
			{
				p_LTSpeed.speed = 0;
			}
		
			//m_judge = 0;
			CGPoint pos0 = p_node_0.position;
			CGPoint pos1 = p_node_1.position;
			
			
			if (manWing)
			{
				pos0 = ccp(p_node_0.position.x,p_node_0.position.y + 2);
				p_node_0.position = pos0;
				
				pos1 = ccp(p_node_1.position.x,p_node_1.position.y + 2);
				p_node_1.position = pos1;
				

				
			}
			
			if (manFrozen || manRain)
			{
				pos0 = ccp(p_node_0.position.x,p_node_0.position.y);
				p_node_0.position = pos0;
				
				pos1 = ccp(p_node_1.position.x,p_node_1.position.y);
				p_node_1.position = pos1;
			}
			
			if (!manFrozen && !manRain && !manWing)
			{
				pos0 = ccp(p_node_0.position.x,p_node_0.position.y+5);
				p_node_0.position = pos0;
				
				pos1 = ccp(p_node_1.position.x,p_node_1.position.y+5);
				p_node_1.position = pos1;
			}
			
			m_lastTime++;
			
			if (!manWing) 
			{
				if (30<m_lastTime && m_lastTime<80)
				{
					man_vel.y = 0;
				
					
				}
				
				if (manWillDead && m_lastTime>80)
				{
					[self deadEffect];
				
				}
			}
			
			else
			{
				if (90<m_lastTime && m_lastTime<240)
				{
					man_vel.y = 0;
					
					
				}
				
				if (manWillDead && m_lastTime>240)
				{
					[self deadEffect];
					
				}
				
			}
			
			
			
			if(man_pos.y < -p_man.contentSize.height/2)
			{
				
				[self showGameover];
				return;
			}
			
			if (manWillFall)
			{
				[self fallEffect];
				
			}
			

		}
		

		int nMaxDistance = 240;
		if (isPad)
		{
			nMaxDistance = 512;
		}
	    if(man_pos.y > nMaxDistance)
		{
			
			
			float delta = man_pos.y - nMaxDistance;
			man_pos.y = nMaxDistance;
					
			if (man_vel.y >0)
			{
				switch (m_state)
				{
						
					case 1:
					case 2:
					case 3:	
					{
						if (manFrozen || manRain)
						{
							p_BGSpeed.speed = 0;
							p_MDSpeed.speed = 0;
							p_FRSpeed.speed = 0;
						//	p_LTSpeed.speed = 0;
							
						} 
						else
						{
							p_BGSpeed.speed = 20;
							p_MDSpeed.speed = 25;
							p_FRSpeed.speed = 50;
							if (isPad)
							{
								p_BGSpeed.speed = 20*2;
								p_MDSpeed.speed = 25*2;
								p_FRSpeed.speed = 50*2; 
							}
						}
						
						
						
					}
						break;
					case 4:
					case 5:
					case 6:	
					{
						if (manFrozen || manRain)
						{
							p_BGSpeed.speed = 0;
							p_MDSpeed.speed = 0;
							p_FRSpeed.speed = 0;
						//	p_LTSpeed.speed = 0;
							
						} 
						
						else
						{
							p_BGSpeed.speed = 20;
							p_MDSpeed.speed = 25;
							p_FRSpeed.speed = 50;
							if (isPad)
							{
								p_BGSpeed.speed = 20*2;
								p_MDSpeed.speed = 25*2;
								p_FRSpeed.speed = 50*2; 
							}
						}
						
						
					}
						break;
					case 7:
					case 8:
					case 9:	
					{
						if (manFrozen || manRain)
						{
							p_BGSpeed.speed = 0;
							p_MDSpeed.speed = 0;
							p_FRSpeed.speed = 0;
							//p_LTSpeed.speed = 0;
							p_LT2Speed.speed = 0;
							
						} 
						else
						{
							p_BGSpeed.speed = 20;
							p_MDSpeed.speed = 20;
							p_LT2Speed.speed = 20;
							if (isPad)
							{
								p_BGSpeed.speed = 20*2;
								p_MDSpeed.speed = 20*2;
								p_LT2Speed.speed = 20*2; 
							}
							
							if (willstop == NO)
							{
								p_LTSpeed.speed = 12;
								if (isPad)
								{
									p_LTSpeed.speed = 12*2;
								}
							}
							
							if (willstop == YES)
							{
								//
								
							}
						}

					}
						break;
					case 10:	
					{
						if (manFrozen || manRain)
						{
							p_BGSpeed.speed = 0;
							p_MDSpeed.speed = 0;
							p_FRSpeed.speed = 0;
						//	p_LTSpeed.speed = 0;
							
						} 
						else
						{
							p_BGSpeed.speed = 20;
							p_MDSpeed.speed = 25;
							p_FRSpeed.speed = 50;
							if (isPad)
							{
								p_BGSpeed.speed = 20*2;
								p_MDSpeed.speed = 25*2;
								p_FRSpeed.speed = 50*2; 
							}
						}
						
						
						
					}
						break;
					case 11:	
					{
						if (manFrozen || manRain)
						{
							p_BGSpeed.speed = 0;
							p_MDSpeed.speed = 0;
							p_FRSpeed.speed = 0;
							//p_LTSpeed.speed = 0;
							
						} 
						
						else
						{
							p_BGSpeed.speed = 20;
							p_MDSpeed.speed = 25;
							p_FRSpeed.speed = 50;
							if (isPad)
							{
								p_BGSpeed.speed = 20*2;
								p_MDSpeed.speed = 25*2;
								p_FRSpeed.speed = 50*2; 
							}
						}
						
						
					}
						break;
					case 12:	
					{
						if (manFrozen || manRain)
						{
							p_BGSpeed.speed = 0;
							p_MDSpeed.speed = 0;
							p_FRSpeed.speed = 0;
							//p_LTSpeed.speed = 0;
							p_LT2Speed.speed = 0;
							
						} 
						else
						{
							p_BGSpeed.speed = 20;
							//p_MDSpeed.speed = 20;
							p_LT2Speed.speed = 20;
							if (isPad)
							{
								p_BGSpeed.speed = 20*2;
								//p_MDSpeed.speed = 20*2;
								p_LT2Speed.speed = 20*2; 
							}
							
							if (willstop == NO)
							{
								p_LTSpeed.speed = 12;
								if (isPad)
								{
									p_LTSpeed.speed = 12*2;
								}
							}
							
							if (willstop == YES)
							{
								//
								
							}
						}
			}
						break;
					default:
						break;
						
						
						
						
				}
			}
			
			CGPoint pos0 = p_node_0.position;
			pos0 = ccp(p_node_0.position.x,p_node_0.position.y - delta);
			CGPoint pos1 = p_node_1.position;
			pos1 = ccp(p_node_1.position.x,p_node_1.position.y - delta);
			
			CGPoint pos2 = p_mapNode_LT.position;
//			pos2 = ccp(p_mapNode_LT.position.x,p_mapNode_LT.position.y - delta/4);
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			if ((m_state==6) && haha > 5700)
			{
				pos2 = ccp(p_mapNode_LT.position.x,p_mapNode_LT.position.y - delta/4);
				p_mapNode_LT.position = pos2;
				if (isPad)
				{
					if (p_mapNode_LT.position.y<440)
					{
						cross = YES;
						man_vel.x = 0.0f;
						man_vel.y = 100.0f;
						if (isPad)
						{
							man_vel.y = 200.0f ;
							
						}
						
						man_acc.x = 0.0f;
						man_acc.y = 0;//-550.0f;
						if (p_mapNode_LT.position.y<240)
						{
							[self continueGame];
							return;
						}
						
					}
				}
				else
				{
					if (p_mapNode_LT.position.y<220)
					{
						cross = YES;
						man_vel.x = 0.0f;
						man_vel.y = 100.0f;
						if (isPad)
						{
							man_vel.y = 200.0f  ;
							
						}
						man_acc.x = 0.0f;
						man_acc.y = 0;//-550.0f;
						if (p_mapNode_LT.position.y<120)
						{
							[self continueGame];
							return;
						}
						
					}
				}

				
			}
			
			int nTrueScreenHeight = 0;
			if (isPad) 
			{
				nTrueScreenHeight = 960;
			}
			else 
			{
				nTrueScreenHeight = 480; 
			}

			if (pos0.y < - nTrueScreenHeight)
			{	
				
				for (CCSprite *prop in p_node0_array)
				{
					prop.visible = NO;
					
					[p_node_0 removeChild:prop cleanup:YES];
					//[[CCDirector sharedDirector] purgeCachedData];
					
					prop = nil; 
				}
				[p_node0_array removeAllObjects];
				createDone = NO;
				p_node_0.position = ccp(0,p_node_1.position.y + nTrueScreenHeight);
				if (!gameEndless)
				{
					int haha = m_hightscore/5;
					if (isPad)
					{
						haha = haha/2;
					}
					if (haha < 6000&& cross == NO)
					{
						[self judgenext:0];
						if ([p_node0_array count] == 0)
						{
							[self initbigNormalmuch:0:haha:m_state];
						}
					}
				}	
				
				else
				{
					[self judgenext:0];
					if ([p_node0_array count] == 0)
					{
						[self initbigNormalmuch:0:haha:m_state];
					}
				} 
			}
			else
			{
				p_node_0.position = pos0;
			}

			
			if (pos1.y < -nTrueScreenHeight )
			{	
				
				for (CCSprite *prop in p_node1_array)
				{
					prop.visible = NO;
					
					[p_node_1 removeChild:prop cleanup:YES];
					//[[CCDirector sharedDirector] purgeCachedData];
					
					prop = nil; 
				}
				[p_node1_array removeAllObjects];		
				
				p_node_1.position = ccp(0,p_node_0.position.y + nTrueScreenHeight);
				
				if (!gameEndless)
				{
					int haha = m_hightscore/5;
					if (isPad)
					{
						haha = haha/2;
					}
					if (haha < 6000&& cross == NO)
					{
						[self judgenext:1];
						if ([p_node1_array count] == 0)
						{
							[self initbigNormalmuch:1:haha:m_state];
						}
					}
				}	
				 
				else
				{
					[self judgenext:1];
					if ([p_node1_array count] == 0)
					{
						[self initbigNormalmuch:1:haha:m_state];
					}
				} 
			}
			else
			{
				p_node_1.position = pos1;
			}

			//分数的刷新
			m_hightscore += (int)delta; 
			NSString *hightcount = nil;
 			if (isPad)
			{
				hightcount = [NSString stringWithFormat:@"%03d",m_hightscore/10]; 
 			}
			else 
			{
				hightcount = [NSString stringWithFormat:@"%03d",m_hightscore/5]; 
			}

 			hightLabel = (CCLabelAtlas*)[self getChildByTag:kHightScoreLabel];
			[hightLabel setString:hightcount];	

			if (gameEndless)
			{
				 [self unlockAC];
			}
			  
			haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			switch (m_state)
			{
 
				case 1:
				{
				
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}

				
				}
					
				case 2:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}
					
					
				}
				case 3:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
					
					
							[self continueGame];
						return;

					}
					
					
				}
					break;
				case 4:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}
					
					
				}
					break;
				case 5:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}
					
					
				}
					break;
				case 6:
				{
					
//					if (m_hightscore/5 > 6000 && !gameEndless) 
//					{
//						
//						
//						[self continueGame];
//						return;
//						
//					}
					
					
				}
				break;
				case 7:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}
					
					
				}
					break;
				case 8:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}
					
					
				}
					break;
				case 9:
				{
					
					if (haha > 6000 && !gameEndless) 
					{
						
						
						[self continueGame];
						return;
						
					}
					
					
				}
					break;
				default:
					break;
			}
			NSString *numcount = [NSString stringWithFormat:@"%03d",m_moneyscore];
			numLabel = (CCLabelAtlas*)[self getChildByTag:kNumLabel];
			[numLabel setString:numcount];
			
			

			
		}

		p_man.position = man_pos;

		
		
		if ((m_hamburgcount>=80 && showtag == NO) && m_state>=4)
		{
			
			[self tagEffect];
			
		}
		
		
	}

}

-(void) comboCallBack:(id)sender
{ 
	CCSprite *sprite = (CCSprite*)sender;
	sprite.visible = NO;
    [self removeChild:sprite cleanup:YES];
}

-(void) comboJudge:(unsigned int)nComboCount
{
	NSString* buffer = [NSString stringWithFormat:@"%d_combo.png",nComboCount];
	CCSprite* pSprite = [CCSprite spriteWithFile:buffer];
	pSprite.position  = p_man.position;
	pSprite.scale = 0.1f;
	[self addChild:pSprite z:10];
	
	id scaleAction1 = [CCScaleTo actionWithDuration:0.2f scale:1.5f];
	id scaleAction2 = [CCScaleTo actionWithDuration:0.1f scale:0.8f];
	id scaleAction3 = [CCScaleTo actionWithDuration:0.1f scale:1.0f];
	//id moveByAction = [CCMoveBy actionWithDuration:1.0f position:ccp(-0,-240)];
	id fadeOutAction = [CCFadeOut actionWithDuration:0.1f];
	CCCallFuncN *callback = [CCCallFuncN actionWithTarget:self selector:@selector(comboCallBack:)];
	[pSprite runAction:[CCSequence actions:scaleAction1,scaleAction2,scaleAction3,[CCDelayTime actionWithDuration:1.0],fadeOutAction,callback,nil]];	
}


#pragma mark -
- (void)moneyEffect
{
	if (moneys)
	{
		musiccount++;
		if (musiccount>9)
		{
			musiccount = 0;
		}
	}
    else
	{
		musiccount = 0;
		moneys = YES;
		m_moneyTime = 0;
		
		//m_nComboCount = 0;
	}
	if (showtag == NO)
	{
		m_nComboCount++;
	}
	
	else
	{
		m_nComboCount = 0;	
	}

	
	switch (musiccount)
	{
		case 0:
			
			[[SimpleAudioEngine sharedEngine] playEffect:@"1.aif"];
			break;
		case 1:
			[[SimpleAudioEngine sharedEngine] playEffect:@"2.aif"];
			break;
		case 2:
			[[SimpleAudioEngine sharedEngine] playEffect:@"3.aif"];
			break;
		case 3:
			[[SimpleAudioEngine sharedEngine] playEffect:@"4.aif"];
			break;
		case 4:
			[[SimpleAudioEngine sharedEngine] playEffect:@"5.aif"];
			break;
			
		case 5:
			
			[[SimpleAudioEngine sharedEngine] playEffect:@"6.aif"];
			break;
		case 6:
			[[SimpleAudioEngine sharedEngine] playEffect:@"7.aif"];
			break;
		case 7:
			[[SimpleAudioEngine sharedEngine] playEffect:@"8.aif"];
			break;
		case 8:
			[[SimpleAudioEngine sharedEngine] playEffect:@"9.aif"];
			break;
		case 9:
			[[SimpleAudioEngine sharedEngine] playEffect:@"10.aif"];
			break;

		default:
			break;
	}
	
	switch (m_nComboCount) 
	{
	
		case TEN_COMBO:
		{
			[self comboJudge:TEN_COMBO];
			[[SimpleAudioEngine sharedEngine] playEffect:@"combo1.aif"];
			break;
		}
		case TWENTY_COMBO:
		{
			[self comboJudge:TWENTY_COMBO];
			[[SimpleAudioEngine sharedEngine] playEffect:@"combo2.aif"];
			break;
		}
		case FORTY_COMBO:
		{
			[self comboJudge:FORTY_COMBO];
			[[SimpleAudioEngine sharedEngine] playEffect:@"combo3.aif"];
			break;
		}
		case SIXTY_COMBO:
		{
			[self comboJudge:SIXTY_COMBO];
			[[SimpleAudioEngine sharedEngine] playEffect:@"combo4.aif"];
			break;
		}
		case EIGHTY_COMBO:
		{
			[self comboJudge:EIGHTY_COMBO];
			[[SimpleAudioEngine sharedEngine] playEffect:@"combo5.aif"];
			break;
		}
		default:
			break;
	}
	
}

#pragma mark -
- (void)bombEffect
{
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bomb_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bomb_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bomb_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bomb_doc.aif"];
		}
			break;
		default:
			break;
	}
	p_man.visible = NO;
	particleBomb.visible = YES;
	particleBomb.position = ccp(p_man.position.x,p_man.position.y);	
	CCAnimation *p_bombParticle = [CCAnimation animationWithName:@"bombParticle" delay:0.1f];
	for (int i=2; i<9; i++)
	{
		[p_bombParticle addFrameWithFilename:[NSString stringWithFormat:@"bomb_death_%02d.png",i]];
	}
	id actionbomb = [CCAnimate actionWithAnimation:p_bombParticle restoreOriginalFrame:NO];
	[particleBomb runAction:[CCSequence actions:actionbomb,[CCCallFuncN actionWithTarget:self selector:@selector(showGameover)],nil]];
	
}


#pragma mark -
- (void)rushEffect //人物冲刺动画
{
	[p_man stopAllActions];
	
	
	switch (m_state)
	{
		case 1:
		case 2:
		case 3:	
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					
					break;
					
				case 1:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 2:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 3:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				default:
					break;
			}
			
		}
			break;
			
			
		case 7:
		case 8:
		case 9:	
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					
					break;
					
				case 1:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 2:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 3:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				default:
					break;
			}
			
		}
			break;
		case 10:	
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					
					break;
					
				case 1:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 2:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 3:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				default:
					break;
			}
			
		}
			break;
		case 12:	
		{
			switch (m_player)
			{
				case 0:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Bro_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					
					break;
					
				case 1:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Grandma_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 2:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Rock_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				case 3:
				{
					CCAnimation *p_rush = [CCAnimation animationWithName:@"rush" delay:0.2f];
					for (int i=4; i<5; i++)
					{
						[p_rush addFrameWithFilename:[NSString stringWithFormat:@"Doc_jump_%02d.png", i]];
					}
					
					id actionrush = [CCAnimate actionWithAnimation:p_rush restoreOriginalFrame:NO];
					[p_man runAction:actionrush];
				}
					
					break;
					
				default:
					break;
			}
			
		}
			break;
			
		default:
			break;
	}
	
	
	
	manrush = YES;
	manWillFall = YES;
	
	
}

#pragma mark -
- (void)shakeEffect
{
	manshake = YES;
    
}

#pragma mark -
- (void)frozenEffect
{
	

	p_tf2.visible = NO;
	p_tf4.visible = NO;
	p_tf3.visible = NO;
	
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	
	[p_man stopAllActions];
	manFrozen = YES;
	p_tffrozen.visible = YES;
	p_man.visible = NO;
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"frozen_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"frozen_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"frozen_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"frozen_doc.aif"];
		}
			break;
		default:
			break;
	}
	switch (m_player)
	{
		case 0:
		{
		 	CCAnimation *p_frozenparticle = [CCAnimation animationWithName:@"frozenparticle" delay:0.1f];
			for (int i=1; i<11; i++)
			{
				[p_frozenparticle addFrameWithFilename:[NSString stringWithFormat:@"Bro_frozen_particle_%02d.png", i]];
			} 
			id actionfrozenparticle = [CCAnimate actionWithAnimation:p_frozenparticle restoreOriginalFrame:NO];
			[p_tffrozen runAction:[CCRepeatForever actionWithAction:actionfrozenparticle]]; 
			
		}
			break;
		case 1:
		{
			CCAnimation *p_frozenparticle = [CCAnimation animationWithName:@"frozenparticle" delay:0.1f];
			for (int i=1; i<11; i++)
			{
				[p_frozenparticle addFrameWithFilename:[NSString stringWithFormat:@"Grandma_frozen_particle_%02d.png", i]];
			} 
			id actionfrozenparticle = [CCAnimate actionWithAnimation:p_frozenparticle restoreOriginalFrame:NO];
			[p_tffrozen runAction:[CCRepeatForever actionWithAction:actionfrozenparticle]]; 
			
		}
			break;
		case 2:
		{
			CCAnimation *p_frozenparticle = [CCAnimation animationWithName:@"frozenparticle" delay:0.1f];
			for (int i=1; i<11; i++)
			{
				[p_frozenparticle addFrameWithFilename:[NSString stringWithFormat:@"Rock_frozen_particle_%02d.png", i]];
			} 
			id actionfrozenparticle = [CCAnimate actionWithAnimation:p_frozenparticle restoreOriginalFrame:NO];
			[p_tffrozen runAction:[CCRepeatForever actionWithAction:actionfrozenparticle]]; 
			
		}
			break;
		case 3:
		{
			CCAnimation *p_frozenparticle = [CCAnimation animationWithName:@"frozenparticle" delay:0.1f];
			for (int i=1; i<11; i++)
			{
				[p_frozenparticle addFrameWithFilename:[NSString stringWithFormat:@"Doc_frozen_particle_%02d.png", i]];
			} 
			id actionfrozenparticle = [CCAnimate actionWithAnimation:p_frozenparticle restoreOriginalFrame:NO];
			[p_tffrozen runAction:[CCRepeatForever actionWithAction:actionfrozenparticle]]; 
			
		}
			break;	
		default:
			break;
	}
	self.isAccelerometerEnabled = NO;
	

}

- (void)frozenparticledone
{
	CCAnimation *p_frozenparticle = [CCAnimation animationWithName:@"frozenparticle" delay:0.1f];
	for (int i=1; i<5; i++)
	{
		[p_frozenparticle addFrameWithFilename:[NSString stringWithFormat:@"ice_break_%02d.png", i]];
	} 
	id actionfrozenparticle = [CCAnimate actionWithAnimation:p_frozenparticle restoreOriginalFrame:NO];
	[p_tffrozen runAction:[CCSequence actions:actionfrozenparticle,[CCCallFuncN actionWithTarget:self selector:@selector(frozenparticle:)],nil]]; 
	
	manFrozen = NO;
	p_man.visible = YES;
	
	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
	self.isAccelerometerEnabled = YES;
	
	
}

- (void)frozenparticle:(id)sender
{
	p_tf2.visible = NO;
	p_tf4.visible = NO;
	p_tf3.visible = NO;
	
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	p_tffrozen.visible = NO;

}

#pragma mark -
- (void)blockEffect
{
   man_vel.y -= 250;
	if (isPad)
	{
		man_vel.y -= 125.0f;
		
	}
   
}


#pragma mark -
- (void)ghostEffect
{
	[p_man stopAllActions];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Ghost.aif"];
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ghoest_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ghoest_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ghoest_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ghoest_doc.aif"];
		}
			break;
		default:
			break;
	}
	manGhost = YES;
	particleGhost.visible = YES;
	switch (m_player)
	{
		case 0:
		{
		 	CCAnimation *p_ghostparticle = [CCAnimation animationWithName:@"ghostparticle" delay:0.1f];
			for (int i=1; i<7; i++)
			{
				[p_ghostparticle addFrameWithFilename:[NSString stringWithFormat:@"Bro_ghost_particle_%02d.png", i]];
			} 
			id actionghostparticle = [CCAnimate actionWithAnimation:p_ghostparticle restoreOriginalFrame:NO];
			[particleGhost runAction:[CCRepeatForever actionWithAction:actionghostparticle]]; 
			
		}
			break;
		case 1:
		{
			CCAnimation *p_ghostparticle = [CCAnimation animationWithName:@"ghostparticle" delay:0.1f];
			for (int i=1; i<7; i++)
			{
				[p_ghostparticle addFrameWithFilename:[NSString stringWithFormat:@"Grandma_ghost_particle_%02d.png", i]];
			} 
			id actionghostparticle = [CCAnimate actionWithAnimation:p_ghostparticle restoreOriginalFrame:NO];
			[particleGhost runAction:[CCRepeatForever actionWithAction:actionghostparticle]]; 
			
		}
			break;
		case 2:
		{
			CCAnimation *p_ghostparticle = [CCAnimation animationWithName:@"ghostparticle" delay:0.1f];
			for (int i=1; i<7; i++)
			{
				[p_ghostparticle addFrameWithFilename:[NSString stringWithFormat:@"Rock_ghost_particle_%02d.png", i]];
			} 
			id actionghostparticle = [CCAnimate actionWithAnimation:p_ghostparticle restoreOriginalFrame:NO];
			[particleGhost runAction:[CCRepeatForever actionWithAction:actionghostparticle]]; 
			
		}
			break;
		case 3:
		{
			CCAnimation *p_ghostparticle = [CCAnimation animationWithName:@"ghostparticle" delay:0.1f];
			for (int i=1; i<7; i++)
			{
				[p_ghostparticle addFrameWithFilename:[NSString stringWithFormat:@"Doc_ghost_particle_%02d.png", i]];
			} 
			id actionghostparticle = [CCAnimate actionWithAnimation:p_ghostparticle restoreOriginalFrame:NO];
			[particleGhost runAction:[CCRepeatForever actionWithAction:actionghostparticle]]; 
			
		}
			break;
			
		default:
			break;
	}
	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
   
}



#pragma mark -
- (void)showGameover
{
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[[NSUserDefaults standardUserDefaults] setInteger:m_hightscore forKey:@"GameScore"];
	[[NSUserDefaults standardUserDefaults] setInteger:m_moneyscore forKey:@"Gamemoney"];
	[[NSUserDefaults standardUserDefaults] setInteger:m_hamburgcount forKey:@"GameHamburg"];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [GameOver scene]];

}



#pragma mark -
- (void)continueGame
{
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[[NSUserDefaults standardUserDefaults] setInteger:m_hightscore forKey:@"GameScore"];
	[[NSUserDefaults standardUserDefaults] setInteger:m_moneyscore forKey:@"Gamemoney"];
	[[NSUserDefaults standardUserDefaults] setInteger:m_hamburgcount forKey:@"GameHamburg"];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];

	if (m_state < 3)
	{
		[[CCDirector sharedDirector] replaceScene:[GameWin scene]];
	}
	else 
	{
		//[self removeAllChildrenWithCleanup:YES];
		[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GetFullVisionScene scene] withColor:ccWHITE]];
	} 
}
#pragma mark -
- (void)pauseGame
{
	[[CCDirector sharedDirector] purgeCachedData];
	
	CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCRenderTexture* renderTexture = [CCRenderTexture renderTextureWithWidth:winSize.width height:winSize.height];
	
    [renderTexture begin];
    [self visit];
    [renderTexture end];
    [renderTexture setPosition:ccp(winSize.width/2, winSize.height/2)];
	
	CCScene* pauseScene = [CCScene node];
	CCLayerColor* colorLayer = [CCColorLayer layerWithColor:ccc4(0, 0, 0, 128)];
	
    // pauseScene 的组成：从底层向外层： 上一个界面截图，暗灰效果层, 新的弹出菜单层。
    [pauseScene addChild:renderTexture];
    [pauseScene addChild:colorLayer];
	CCNode *p = [PauseScene node];
    [pauseScene addChild:p];
	//[[CCDirector sharedDirector] pause];
    // 4. 直接 push 过去吧，方便存储当前场景状态之类的
	//    [HelloWorld pause];
	[[CCDirector sharedDirector] pushScene:pauseScene]; 
}


- (void)checkGameBreakLeft:(ccTime) delta
{
	m_nTimeCountLeft++;	
	if (m_nTimeCountLeft > 300 && m_nTimeCountLeft%100 == 0) 
	{
		m_nTimeCountLeft = 0;
		[self gameBreakRocket:5];
	}
}

- (void)checkGameBreakRight:(ccTime) delta
{
	m_nTimeCountRight++;  
	if (m_nTimeCountRight > 300 && m_nTimeCountRight%100 == 0) 
	{ 
		m_nTimeCountLeft = 0;
		[self gameBreakRocket:5];
	}
}

#pragma mark -
- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{ 
	if (acceleration.x < 0)
	{ 
		if (m_nTimeCountLeft <= 0) 
		{
			m_nTimeCountRight = 0;
			[self unschedule:@selector(checkGameBreakRight:)];
			[self schedule:@selector(checkGameBreakLeft:)];
		} 
	}
	else 
	{ 
		if (m_nTimeCountRight <= 0)
		{
			m_nTimeCountLeft = 0;
			[self unschedule:@selector(checkGameBreakLeft:)];
			[self schedule:@selector(checkGameBreakRight:)];
		}
	} 

	
	float accel_filter = 0.1f;
	if (isPad)
	{
		man_vel.x = man_vel.x * accel_filter + acceleration.x * (1.0f - accel_filter) * 1400;

	}
	else
	{
		man_vel.x = man_vel.x * accel_filter + acceleration.x * (1.0f - accel_filter) * 700;
	
	}

	if (fabsf(acceleration.z) > 2.0)
	{ 
		
		if(showtag == YES && m_state>=4 && m_state< 13)
		{
			m_hamburgcount -= m_hamburgcount;
			[self flyEffect];
			
		}
        
		if (manshake == NO && m_state>=7)
		{
			[self shakeEffect];
		}

	}
	
}



#pragma mark -
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (manFrozen)
	{
		[self frozenparticledone];
		[[SimpleAudioEngine sharedEngine] playEffect:@"Unfrozen.aif"];
	}
    
}


#pragma mark -
- (void)jumpEffect
{
	manDead = NO;
	[p_man stopAllActions];
	CCAnimation *p_jump = [CCAnimation animationWithName:@"jump" delay:0.1f];
	switch (m_player)
	{
		case 0:
		{
			
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}	
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:YES];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_shoeup_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f;
				if (isPad)
				{
					man_vel.y = 1300.0f ;
					
				}
				
				
				
			}
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f ;
					
				}
			}
			
		}
			
			break;
			
			
			
			
		case 1:
		{
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}	
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_shoe_up_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
	
				
			}
			
			if (!manWing && !manShoes  && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:YES];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f ;
					
				}
			}
			
		}	
			break;	
			
		case 2:
		{
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}	
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_shoeup_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
				
				
				
			}
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
		}	
			break;
			
			
		case 3:
		{
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}	
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_shoeup_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
				
				
				
			}
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f ;
					
				}
			}
			
		}	
			break;
			
		default:
			break;
	}
	

	
	
	
	manWillFall = YES;
	
	
	
}
#pragma mark -
- (void)jumpagainEffect
{
	manDead = NO;
	[p_man stopAllActions];
	CCAnimation *p_jump = [CCAnimation animationWithName:@"jump" delay:0.1f];
	switch (m_player)
	{
		case 0:
		{
			
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}	
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_shoeup_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
				
				
				
			}
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Bro_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f  ;
					
				}
			}
			
		}
			
			break;
		case 1:
		{
			
			
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}	
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_shoe_up_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
				
				
			}
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Grandma_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f;
					
				}
			}
		}
			
			break;
			
			
		case 2:
		{
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}	
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_shoeup_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
				
				
				
			}
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Rock_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f  ;
					
				}
			}
			
		}	
			break;
			
		case 3:
		{
			if (manWing)
			{
				
				
				for (int i=1; i<2; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_wingUp_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}	
			
			if (manShoes)
			{
				
				
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_shoeup_%02d.png", i]];
				}
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				
				man_vel.y = 650.0f+ fabsf(man_vel.x)/2;;
				if (isPad)
				{
					man_vel.y = 1300.0f + fabsf(man_vel.x)/2;
					
				}
				
				
			}
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_ghostup_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionjump]];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
			}
			
			
			if (!manWing && !manShoes && !manGhost) 
			{
				for (int i=1; i<3; i++)
				{
					[p_jump addFrameWithFilename:[NSString stringWithFormat:@"Doc_up_%02d.png", i]];
				}
				
				id actionjump = [CCAnimate actionWithAnimation:p_jump restoreOriginalFrame:NO];
				[p_man runAction:actionjump];
				man_vel.y = 450.0f + fabsf(man_vel.x)/2;
				if (isPad)
				{
					man_vel.y = 900.0f + fabsf(man_vel.x)/2;
					
				}
				
			}
			
			if (manrush)
			{
				man_vel.y = 600;
				if (isPad)
				{
					man_vel.y = 1200.0f ;
					
				}
			}
			
		}	
			break;	
			
		default:
			break;
	}
	
	particleFall = [CCSprite spriteWithFile:@"fart_01.png"];
	if (manLookingRight)
	{
		particleFall.scaleX = 1.0f;
	}
	else 
	{
		particleFall.scaleX = -1.0f;
	}
	particleFall.tag = MIN_FALL_INDEX+m_lFallParCount;
	m_lFallParCount++;
	[self addChild:particleFall z:1];
	particleFall.position = ccp(p_man.position.x,p_man.position.y-10);
	
	CCAnimation *p_jumpagainParticle = [CCAnimation animationWithName:@"fallParticle" delay:0.1f];
	for (int i=2; i<9; i++)
	{
		[p_jumpagainParticle addFrameWithFilename:[NSString stringWithFormat:@"fart_%02d.png",i]];
	}
	id actionjumpagain = [CCAnimate actionWithAnimation:p_jumpagainParticle restoreOriginalFrame:NO];
	[particleFall runAction:[CCSequence actions:actionjumpagain,[CCCallFunc actionWithTarget:self selector:@selector(spriteDone2:)],nil]];
	
	
	
	manWillFall = YES;
	
	
}




#pragma mark -
- (void)tagEffect
{
	showtag = YES;
	tagIcon.visible = YES;
	CCAnimation *p_tag = [CCAnimation animationWithName:@"tag" delay:0.1f];
	for (int i=1; i<11; i++)
	{
		[p_tag addFrameWithFilename:[NSString stringWithFormat:@"tag_%02d.png", i]];
	}
	id actiontag = [CCAnimate actionWithAnimation:p_tag restoreOriginalFrame:NO];
	[tagIcon runAction:actiontag];
	
	
	CCAnimation *p_tag2 = [CCAnimation animationWithName:@"tag" delay:0.1f];
	for (int i=9; i<11; i++)
	{
		[p_tag2 addFrameWithFilename:[NSString stringWithFormat:@"tag_%02d.png", i]];
	}
	id actiontag2 = [CCAnimate actionWithAnimation:p_tag2 restoreOriginalFrame:NO];
	[tagIcon runAction:[CCRepeatForever actionWithAction:actiontag2]];
	

}

#pragma mark -
- (void)tagmissEffect
{
	[tagIcon stopAllActions];
	showtag = NO;
	
	CCAnimation *p_tag = [CCAnimation animationWithName:@"tag" delay:0.1f];
	for (int i=11; i<21; i++)
	{
		[p_tag addFrameWithFilename:[NSString stringWithFormat:@"tag_%02d.png", i]];
	}
	id actiontag = [CCAnimate actionWithAnimation:p_tag restoreOriginalFrame:NO];
	[tagIcon runAction:actiontag];
	
	
}



#pragma mark -
- (void)fallEffect
{
	[p_man stopAllActions];
	CCAnimation *p_fall = [CCAnimation animationWithName:@"fall" delay:0.1f];
	switch (m_player)
	{
		case 0:
			
		{
			if (manWing)
			{
				
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Bro_wingDown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
				
			}
			
			if (manShoes)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Bro_shoedown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
				
				
			}
			
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Bro_ghostdown_%02d.png", i]];
				}
				
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:YES];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
			if (!manWing && !manShoes && !manGhost)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Bro_down_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
		}
			break;
			
		case 1:
		{
			if (manWing)
			{
				
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Grandma_wingDown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
				
			}
			
			if (manShoes)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Grandma_shoedown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
				
				
			}
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Grandma_ghostdown_%02d.png", i]];
				}
				
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:YES];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
			if (!manWing && !manShoes && !manGhost)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Grandma_down_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
		}
			
			break;
		case 2:
		{
			if (manWing)
			{
				
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Rock_wingDown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
				
			}
			
			if (manShoes)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Rock_shoedown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Rock_ghostdown_%02d.png", i]];
				}
				
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:YES];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
			if (!manWing && !manShoes && !manGhost)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Rock_down_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
		}
			break;
		case 3:
		{
			if (manWing)
			{
				
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Doc_wingDown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
				
			}
			
			if (manShoes)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Doc_shoedown_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			if (manGhost)
			{
				for (int i=1; i<3; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Doc_ghostdown_%02d.png", i]];
				}
				
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:YES];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
			if (!manWing && !manShoes && !manGhost)
			{
				
				for (int i=1; i<2; i++)
				{
					[p_fall addFrameWithFilename:[NSString stringWithFormat:@"Doc_down_%02d.png", i]];
				}
				id actionfall = [CCAnimate actionWithAnimation:p_fall restoreOriginalFrame:NO];
				[p_man runAction:[CCRepeatForever actionWithAction:actionfall]];
				
			}
			
			
		}
			break;	
			
		default:
			break;
	}
	
	manWillDead = YES;
	manWillFall = NO;
	m_lastTime = 0;
	

	
	
}

#pragma mark -
- (void)rainEffect
{
	[p_man stopAllActions];
	manRain = YES;
	
	p_tf2.visible = NO;
	p_tf4.visible = NO;	
	p_tf3.visible = NO;
	
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bolt_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bolt_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bolt_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"bolt_doc.aif"];
		}
			break;
		default:
			break;
	}
    p_tfrain.visible = YES;
	p_man.visible = NO;
   
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_rain = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=1; i<3; i++) 
			{
				[p_rain addFrameWithFilename:[NSString stringWithFormat:@"Bro_boltDeath_%02d.png",i]];
			}
			id actionrain = [CCAnimate actionWithAnimation:p_rain restoreOriginalFrame:NO];
			
			
			
			[p_tfrain runAction:[CCSequence actions:[CCRepeat actionWithAction:actionrain times:3],[CCCallFunc actionWithTarget:self selector:@selector(rainparticle:)],nil]];
			
			
		}
			break;
			
		case 1:
		{
			CCAnimation *p_rain = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=1; i<3; i++) 
			{
				[p_rain addFrameWithFilename:[NSString stringWithFormat:@"Grandma_boltDeath_%02d.png",i]];
			}
			id actionrain = [CCAnimate actionWithAnimation:p_rain restoreOriginalFrame:NO];
			[p_tfrain runAction:[CCSequence actions:[CCRepeat actionWithAction:actionrain times:3],[CCCallFunc actionWithTarget:self selector:@selector(rainparticle:)],nil]];
			
			
			
		}
			break;
			
			
		case 2:
		{
			CCAnimation *p_rain = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=1; i<3; i++) 
			{
				[p_rain addFrameWithFilename:[NSString stringWithFormat:@"Rock_boltDeath_%02d.png",i]];
			}
			id actionrain = [CCAnimate actionWithAnimation:p_rain restoreOriginalFrame:NO];
			
			[p_tfrain runAction:[CCSequence actions:[CCRepeat actionWithAction:actionrain times:3],[CCCallFunc actionWithTarget:self selector:@selector(rainparticle:)],nil]];
			
			
		}
			break;	
			
		case 3:
		{
			CCAnimation *p_rain = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=1; i<3; i++) 
			{
				[p_rain addFrameWithFilename:[NSString stringWithFormat:@"Doc_boltDeath_%02d.png",i]];
			}
			id actionrain = [CCAnimate actionWithAnimation:p_rain restoreOriginalFrame:NO];
			
			[p_tfrain runAction:[CCSequence actions:[CCRepeat actionWithAction:actionrain times:3],[CCCallFunc actionWithTarget:self selector:@selector(rainparticle:)],nil]];
			
			
		}
			break;		
		default:
			break;
	}
	p_tfmoney.visible = YES;
	p_tfmoney.position = ccp(p_man.position.x,p_man.position.y);
	CCAnimation *p_lose = [CCAnimation animationWithName:@"fly" delay:0.1f];
	for (int i=1; i<9; i++) 
	{
		[p_lose addFrameWithFilename:[NSString stringWithFormat:@"lose_money_%02d.png",i]];
	}
	id actionmoney = [CCAnimate actionWithAnimation:p_lose restoreOriginalFrame:NO];	
	[p_tfmoney runAction:actionmoney];

	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

}

- (void)rainparticle:(id)sender
{
   
	p_tfrain.visible = NO;
	p_man.visible = YES;
	
	manRain = NO;

}




#pragma mark -
- (void)dashEffect
{
	manDead = NO;
	[p_man stopAllActions];
	manDash = YES;
	p_tf2.visible = NO;
	p_tf4.visible = NO;	
	p_tf3.visible = NO;
	
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	particleDash.visible = YES;
	p_man.visible = NO;
	p_tf5.visible = YES;
	

	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dash_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dash_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dash_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dash_doc.aif"];
		}
			break;
		default:
			break;
	}
	
	p_power_dash = [CCSprite spriteWithFile:@"power_01.png"];
	[p_power_dash setPosition:convertPoint(ccp(60,25))];
	[self addChild:	p_power_dash z:4];
	
	CCAnimation *p_powereffet = [CCAnimation animationWithName:@"powereffet" delay:0.8f];
	
	for (int i=2; i<7; i++) 
	{
		[p_powereffet addFrameWithFilename:[NSString stringWithFormat:@"power_%02d.png",i]];
	}
	id powereffet = [CCAnimate actionWithAnimation:p_powereffet restoreOriginalFrame:NO];
	
	[p_power_dash runAction:powereffet];
	
	tagwing.visible = NO;
	tagballon.visible = NO;
	tagshoe.visible = NO;
	
	if (tagdash.visible == NO)
	{
		tagdash.visible = YES;
	}
	
	
	
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_dash = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=2; i<9; i++) 
			{
				[p_dash addFrameWithFilename:[NSString stringWithFormat:@"tf_dash_%02d.png",i]];
			}
			id actiondash = [CCAnimate actionWithAnimation:p_dash restoreOriginalFrame:NO];
			
			[p_tf5 runAction:[CCSequence actions:actiondash,[CCCallFunc actionWithTarget:self selector:@selector(dashparticle:)],nil]];

		
		}
			break;
		
		case 1:
		{
			CCAnimation *p_dash = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=2; i<9; i++) 
			{
				[p_dash addFrameWithFilename:[NSString stringWithFormat:@"Grandma_dash_%02d.png",i]];
			}
			id actiondash = [CCAnimate actionWithAnimation:p_dash restoreOriginalFrame:NO];
			
			[p_tf5 runAction:[CCSequence actions:actiondash,[CCCallFunc actionWithTarget:self selector:@selector(dashparticle:)],nil]];
	
		
		}
			break;
	
		
		case 2:
		{
			CCAnimation *p_dash = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=2; i<9; i++) 
			{
				[p_dash addFrameWithFilename:[NSString stringWithFormat:@"Rock_dash_%02d.png",i]];
			}
			id actiondash = [CCAnimate actionWithAnimation:p_dash restoreOriginalFrame:NO];
			
			[p_tf5 runAction:[CCSequence actions:actiondash,[CCCallFunc actionWithTarget:self selector:@selector(dashparticle:)],nil]];
			
			
		}
			break;	
			
		case 3:
		{
			CCAnimation *p_dash = [CCAnimation animationWithName:@"fly" delay:0.1f];
			
			for (int i=2; i<9; i++) 
			{
				[p_dash addFrameWithFilename:[NSString stringWithFormat:@"Doc_dash_%02d.png",i]];
			}
			id actiondash = [CCAnimate actionWithAnimation:p_dash restoreOriginalFrame:NO];
			
			[p_tf5 runAction:[CCSequence actions:actiondash,[CCCallFunc actionWithTarget:self selector:@selector(dashparticle:)],nil]];
			
			
		}
			break;		
		default:
			break;
	}
	
	CCAnimation *p_dashparticle2 = [CCAnimation animationWithName:@"dashparticle" delay:0.1f];
	for (int i=2; i<8; i++)
	{
		[p_dashparticle2 addFrameWithFilename:[NSString stringWithFormat:@"dash_fart_%02d.png", i]];
	} 
	id actiondashparticle2 = [CCAnimate actionWithAnimation:p_dashparticle2 restoreOriginalFrame:NO];
	
	[particleDash runAction:[CCRepeatForever actionWithAction:actiondashparticle2]]; 

   
}


- (void)dashparticle:(id)sender
{
	p_man.visible = YES;
	p_tf5.visible = NO;
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_dashparticle = [CCAnimation animationWithName:@"dashparticle" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dashparticle addFrameWithFilename:[NSString stringWithFormat:@"Bro_dashing_%02d.png", i]];
			} 
			id actiondashparticle = [CCAnimate actionWithAnimation:p_dashparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondashparticle]]; 
		
		}
			break;
		case 1:
		{
			CCAnimation *p_dashparticle = [CCAnimation animationWithName:@"dashparticle" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dashparticle addFrameWithFilename:[NSString stringWithFormat:@"Grandma_dashing_%02d.png", i]];
			} 
			id actiondashparticle = [CCAnimate actionWithAnimation:p_dashparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondashparticle]]; 
			
		}
			break;
			
			
		case 2:
		{
			CCAnimation *p_dashparticle = [CCAnimation animationWithName:@"dashparticle" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dashparticle addFrameWithFilename:[NSString stringWithFormat:@"Rock_dashing_%02d.png", i]];
			} 
			id actiondashparticle = [CCAnimate actionWithAnimation:p_dashparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondashparticle]]; 
			
		}	
			break;
		case 3:
		{
			CCAnimation *p_dashparticle = [CCAnimation animationWithName:@"dashparticle" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dashparticle addFrameWithFilename:[NSString stringWithFormat:@"Doc_dashing_%02d.png", i]];
			} 
			id actiondashparticle = [CCAnimate actionWithAnimation:p_dashparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondashparticle]]; 
			
		}	
			break;
		default:
			break;
	}
   
}


#pragma mark -
- (void)deadEffect
{
	[p_man stopAllActions];
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dead_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dead_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dead_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"dead_doc.aif"];
		}
			break;
		default:
			break;
	}
	manDead =YES;
	manWing = NO;
	p_tf2.visible = NO;
	p_tf4.visible = NO;	
	p_tf3.visible = NO;
	
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_dead = [CCAnimation animationWithName:@"dead" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dead addFrameWithFilename:[NSString stringWithFormat:@"Bro_death_%02d.png", i]];
			} 
			id actiondead = [CCAnimate actionWithAnimation:p_dead restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondead]];
		}
			
			break;
			
		case 1:
		{
			CCAnimation *p_dead = [CCAnimation animationWithName:@"dead" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dead addFrameWithFilename:[NSString stringWithFormat:@"Grandma_death_%02d.png", i]];
			} 
			id actiondead = [CCAnimate actionWithAnimation:p_dead restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondead]];
		}
			
			
			break;
			
		case 2:
		{
			CCAnimation *p_dead = [CCAnimation animationWithName:@"dead" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dead addFrameWithFilename:[NSString stringWithFormat:@"Rock_death_%02d.png", i]];
			} 
			id actiondead = [CCAnimate actionWithAnimation:p_dead restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondead]];
		}
			break;	
		case 3:
		{
			CCAnimation *p_dead = [CCAnimation animationWithName:@"dead" delay:0.1f];
			for (int i=1; i<3; i++)
			{
				[p_dead addFrameWithFilename:[NSString stringWithFormat:@"Doc_death_%02d.png", i]];
			} 
			id actiondead = [CCAnimate actionWithAnimation:p_dead restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actiondead]];
		}
			break;
			
			
		default:
			break;
	}
	
	
	manWillDead = NO;
	//self.isAccelerometerEnabled = NO;
	

}

#pragma mark -
- (void)flyEffect
{
	manDead = NO;
	[p_man stopAllActions];
	manFly = YES;

	p_man.visible = NO;
	p_tf.visible = YES;
	
	p_tf2.visible = NO;
	p_tf4.visible = NO;	
	p_tf3.visible = NO;
	
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	

	
	[p_tf runAction:[CCSequence actions:[CCCallFunc actionWithTarget:self selector:@selector(flyparticle:)],nil]];
	
//
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"fly_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"fly_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"fly_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"fly_doc.aif"];
		}
			break;
		default:
			break;
	}

	[self tagmissEffect];
	
}
#pragma mark -
- (void)flyparticle:(id)sender
{

	
	[p_tf stopAllActions];
	p_tf.visible = NO;
	p_tftf.visible = YES;
	p_man.visible = YES;
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_flyparticle = [CCAnimation animationWithName:@"flyparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flyparticle addFrameWithFilename:[NSString stringWithFormat:@"Bro_rocket_%02d.png", i]];
			} 
			id actionflyparticle = [CCAnimate actionWithAnimation:p_flyparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflyparticle]]; 
		}
			break;
		case 1:
		{
			CCAnimation *p_flyparticle = [CCAnimation animationWithName:@"flyparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flyparticle addFrameWithFilename:[NSString stringWithFormat:@"Grandma_rocket_%02d.png", i]];
			} 
			id actionflyparticle = [CCAnimate actionWithAnimation:p_flyparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflyparticle]]; 
		}
			break;
		case 2:
		{
			CCAnimation *p_flyparticle = [CCAnimation animationWithName:@"flyparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flyparticle addFrameWithFilename:[NSString stringWithFormat:@"rock_rocket_%02d.png", i]];
			} 
			id actionflyparticle = [CCAnimate actionWithAnimation:p_flyparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflyparticle]]; 
		}
			break;
		case 3:
		{
			CCAnimation *p_flyparticle = [CCAnimation animationWithName:@"flyparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flyparticle addFrameWithFilename:[NSString stringWithFormat:@"doc_rocket_%02d.png", i]];
			} 
			id actionflyparticle = [CCAnimate actionWithAnimation:p_flyparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflyparticle]]; 
		}
			break;
		default:
			break;
	}

	CCAnimation *p_flyparticle2 = [CCAnimation animationWithName:@"flyparticle" delay:0.1f];
	for (int i=7; i<14; i++)
	{
		[p_flyparticle2 addFrameWithFilename:[NSString stringWithFormat:@"fly_fart_%02d.png", i]];
	} 
	id actionflyparticle2 = [CCAnimate actionWithAnimation:p_flyparticle2 restoreOriginalFrame:NO];
	[p_tftf runAction:[CCSequence actions:actionflyparticle2,[CCCallFunc actionWithTarget:self selector:@selector(flyparticle2:)],nil]];
	
	
   


}
#pragma mark -
- (void)flyparticle2:(id)sender
{
	p_tftf.visible = NO;
	particleFly.visible = YES;
	CCAnimation *p_flyparticle3 = [CCAnimation animationWithName:@"flyparticle" delay:0.1f];
	for (int i=14; i<17; i++)
	{
		[p_flyparticle3 addFrameWithFilename:[NSString stringWithFormat:@"fly_fart_%02d.png", i]];
	} 
	id actionflyparticle3 = [CCAnimate actionWithAnimation:p_flyparticle3 restoreOriginalFrame:NO];
	[particleFly runAction:[CCRepeatForever actionWithAction:actionflyparticle3]]; 
	
	

}


#pragma mark -
- (void)flowEffect
{
	[p_man stopAllActions];
	manFlow = YES;	
	p_man.visible = NO;
	p_tf2.visible = YES;
	p_tf4.visible = NO;
	p_tf3.visible = NO;
	[p_tf3 stopAllActions];
	[p_tf4 stopAllActions];
	
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ballon_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ballon_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ballon_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ballon_doc.aif"];
		}
			break;
		default:
			break;
	}
	p_power_ballon = [CCSprite spriteWithFile:@"power_01.png"];
	[p_power_ballon setPosition:convertPoint(ccp(60,25))];
	[self addChild:	p_power_ballon z:4];
	
	CCAnimation *p_powereffet = [CCAnimation animationWithName:@"powereffet" delay:2.0f];
	
	for (int i=2; i<7; i++) 
	{
		[p_powereffet addFrameWithFilename:[NSString stringWithFormat:@"power_%02d.png",i]];
	}
	id powereffet = [CCAnimate actionWithAnimation:p_powereffet restoreOriginalFrame:NO];
	
	[p_power_ballon runAction:powereffet];
	
	tagwing.visible = NO;
	tagdash.visible = NO;
	tagshoe.visible = NO;
	if (tagballon.visible == NO)
	{
		tagballon.visible = YES;
	}
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_flow = [CCAnimation animationWithName:@"flow" delay:0.1f];
			for (int i=2; i<8; i++) 
			{
				[p_flow addFrameWithFilename:[NSString stringWithFormat:@"tf_ballon_%02d.png", i]];
			}
			id actionflow = [CCAnimate actionWithAnimation:p_flow restoreOriginalFrame:NO];
			
			[p_tf2 runAction:[CCSequence actions:actionflow,[CCCallFunc actionWithTarget:self selector:@selector(flowparticle:)],nil]];
			
			
		}
			break;
			
		case 1:
		{
			CCAnimation *p_flow = [CCAnimation animationWithName:@"flow" delay:0.1f];
			for (int i=2; i<8; i++) 
			{
				[p_flow addFrameWithFilename:[NSString stringWithFormat:@"Grandma_tf_ballon_%02d.png", i]];
			}
			id actionflow = [CCAnimate actionWithAnimation:p_flow restoreOriginalFrame:NO];
			
			[p_tf2 runAction:[CCSequence actions:actionflow,[CCCallFunc actionWithTarget:self selector:@selector(flowparticle:)],nil]];
			
			
		}
			break;
			
			
		case 2:
		{
			CCAnimation *p_flow = [CCAnimation animationWithName:@"flow" delay:0.1f];
			for (int i=2; i<8; i++) 
			{
				[p_flow addFrameWithFilename:[NSString stringWithFormat:@"Rock_tf_ballon_%02d.png", i]];
			}
			id actionflow = [CCAnimate actionWithAnimation:p_flow restoreOriginalFrame:NO];
			
			[p_tf2 runAction:[CCSequence actions:actionflow,[CCCallFunc actionWithTarget:self selector:@selector(flowparticle:)],nil]];
			
			
		}
			break;
		
		case 3:
		{
			CCAnimation *p_flow = [CCAnimation animationWithName:@"flow" delay:0.1f];
			for (int i=2; i<8; i++) 
			{
				[p_flow addFrameWithFilename:[NSString stringWithFormat:@"Doc_tf_ballon_%02d.png", i]];
			}
			id actionflow = [CCAnimate actionWithAnimation:p_flow restoreOriginalFrame:NO];
			
			[p_tf2 runAction:[CCSequence actions:actionflow,[CCCallFunc actionWithTarget:self selector:@selector(flowparticle:)],nil]];
			
			
		}
			break;	
			
		default:
			break;
	}




}


- (void)flowparticle:(id)sender
{
	
	p_tf2 .visible = NO;
	p_man.visible = YES;
	
	
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_flowparticle = [CCAnimation animationWithName:@"flowparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flowparticle addFrameWithFilename:[NSString stringWithFormat:@"Bro_ballon_%02d.png", i]];
			} 
			id actionflowparticle = [CCAnimate actionWithAnimation:p_flowparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflowparticle]]; 
		
		}
			break;
		case 1:
		{
			CCAnimation *p_flowparticle = [CCAnimation animationWithName:@"flowparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flowparticle addFrameWithFilename:[NSString stringWithFormat:@"Grandma_ballon_%02d.png", i]];
			} 
			id actionflowparticle = [CCAnimate actionWithAnimation:p_flowparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflowparticle]]; 
			
		}
			break;
			
			
		case 2:
		{
			CCAnimation *p_flowparticle = [CCAnimation animationWithName:@"flowparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flowparticle addFrameWithFilename:[NSString stringWithFormat:@"Rock_ballon_%02d.png", i]];
			} 
			id actionflowparticle = [CCAnimate actionWithAnimation:p_flowparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflowparticle]]; 
			
		}
			break;
			
		case 3:
		{
			CCAnimation *p_flowparticle = [CCAnimation animationWithName:@"flowparticle" delay:0.1f];
			for (int i=1; i<5; i++)
			{
				[p_flowparticle addFrameWithFilename:[NSString stringWithFormat:@"Doc_ballon_%02d.png", i]];
			} 
			id actionflowparticle = [CCAnimate actionWithAnimation:p_flowparticle restoreOriginalFrame:NO];
			[p_man runAction:[CCRepeatForever actionWithAction:actionflowparticle]]; 
			
		}
			break;
			
		default:
			break;
	}

	
	
	
}


#pragma mark -
- (void)wingEffect
{
	manDead = NO;
	[p_man stopAllActions];
	manWing = YES;
	p_man.visible = NO;
	p_tf4.visible = YES;
	p_tf2.visible = NO;
	p_tf3.visible = NO;
	[p_tf2 stopAllActions];
	[p_tf3 stopAllActions];
	man_vel.y = 450.0f + fabsf(man_vel.x)/2;
	if (isPad)
	{
		man_vel.y = 900.0f + fabsf(man_vel.x)/2;
		
	}
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"wing_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"wing_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"wing_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"wing_doc.aif"];
		}
			break;
		default:
			break;
	}
	p_power_wing = [CCSprite spriteWithFile:@"power_01.png"];
	[p_power_wing setPosition:convertPoint(ccp(60,25))];
	[self addChild:	p_power_wing z:4];
	
	CCAnimation *p_powereffet = [CCAnimation animationWithName:@"powereffet" delay:4.0f];
	
	for (int i=2; i<7; i++) 
	{
		[p_powereffet addFrameWithFilename:[NSString stringWithFormat:@"power_%02d.png",i]];
	}
	id powereffet = [CCAnimate actionWithAnimation:p_powereffet restoreOriginalFrame:NO];
	
	[p_power_wing runAction:powereffet];
	
	tagdash.visible = NO;
	tagballon.visible = NO;
	tagshoe.visible = NO;
	if (tagwing.visible == NO)
	{
		tagwing.visible = YES;
	}
	
	
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_wings = [CCAnimation animationWithName:@"wing" delay:0.1f];
			for (int i=2; i<10; i++)
			{
				[p_wings addFrameWithFilename:[NSString stringWithFormat:@"tf_wing_%02d.png", i]];
			} 
			id actionwing = [CCAnimate actionWithAnimation:p_wings restoreOriginalFrame:NO];
			
			[p_tf4 runAction:[CCSequence actions:actionwing,[CCCallFunc actionWithTarget:self selector:@selector(wingparticle:)],nil]];
		
		}
			break;
	
		case 1:
		{
			CCAnimation *p_wings = [CCAnimation animationWithName:@"wing" delay:0.1f];
			for (int i=2; i<10; i++)
			{
				[p_wings addFrameWithFilename:[NSString stringWithFormat:@"Grandma_tf_wing_%02d.png", i]];
			} 
			id actionwing = [CCAnimate actionWithAnimation:p_wings restoreOriginalFrame:NO];
			
			[p_tf4 runAction:[CCSequence actions:actionwing,[CCCallFunc actionWithTarget:self selector:@selector(wingparticle:)],nil]];
			
		}
			break;
			
		case 2:
		{
			CCAnimation *p_wings = [CCAnimation animationWithName:@"wing" delay:0.1f];
			for (int i=2; i<10; i++)
			{
				[p_wings addFrameWithFilename:[NSString stringWithFormat:@"Rock_tf_wing_%02d.png", i]];
			} 
			id actionwing = [CCAnimate actionWithAnimation:p_wings restoreOriginalFrame:NO];
			
			[p_tf4 runAction:[CCSequence actions:actionwing,[CCCallFunc actionWithTarget:self selector:@selector(wingparticle:)],nil]];
			
		}
			break;
			
		case 3:
		{
			CCAnimation *p_wings = [CCAnimation animationWithName:@"wing" delay:0.1f];
			for (int i=2; i<10; i++)
			{
				[p_wings addFrameWithFilename:[NSString stringWithFormat:@"Doc_tf_wing_%02d.png", i]];
			} 
			id actionwing = [CCAnimate actionWithAnimation:p_wings restoreOriginalFrame:NO];
			
			[p_tf4 runAction:[CCSequence actions:actionwing,[CCCallFunc actionWithTarget:self selector:@selector(wingparticle:)],nil]];
			
		}
			break;
		default:
			break;
	}
	

	man_vel.y = 350.0f + fabsf(man_vel.x);
	if (isPad)
	{
		man_vel.y = 700.0f + fabsf(man_vel.x)/2;
		
	}


}

- (void)wingparticle:(id)sender
{
	
	p_tf4.visible = NO;
	p_man.visible = YES;
	
	
}


#pragma mark -
- (void)shoesEffect
{
	manDead = NO;
	[p_man stopAllActions];
	manShoes = YES;
	man_vel.y = 450.0f + fabsf(man_vel.x)/2;
	if (isPad)
	{
		man_vel.y = 900.0f + fabsf(man_vel.x)/2;
		
	}
	p_tf3.visible = YES;
	p_tf2.visible = NO;
	p_tf4.visible = NO;
	[p_tf2 stopAllActions];
	[p_tf4 stopAllActions];
	p_man.visible = NO;
	switch (m_player)
	{
		case 0:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"shoe_bro.aif"];
		}
			break;
		case 1:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"shoe_old.aif"];
		}
			break;
		case 2:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"shoe_rock.aif"];
		}
			break;
		case 3:
		{
			[[SimpleAudioEngine sharedEngine] playEffect:@"shoe_doc.aif"];
		}
			break;
		default:
			break;
	}
	p_power_shoe = [CCSprite spriteWithFile:@"power_01.png"];
	[p_power_shoe setPosition:convertPoint(ccp(60,25))];
	[self addChild:	p_power_shoe z:4];
	
	CCAnimation *p_powereffet = [CCAnimation animationWithName:@"powereffet" delay:4.0f];
	
	for (int i=2; i<7; i++) 
	{
		[p_powereffet addFrameWithFilename:[NSString stringWithFormat:@"power_%02d.png",i]];
	}
	id powereffet = [CCAnimate actionWithAnimation:p_powereffet restoreOriginalFrame:NO];
	
	[p_power_shoe runAction:powereffet];
	tagwing.visible = NO;
	tagballon.visible = NO;
	tagdash.visible = NO;
	if (tagshoe.visible == NO)
	{
		tagshoe.visible = YES;
	}
	switch (m_player)
	{
		case 0:
		{
			CCAnimation *p_shoess = [CCAnimation animationWithName:@"shoe" delay:0.15f];
			for (int i=2; i<6; i++)
			{
				[p_shoess addFrameWithFilename:[NSString stringWithFormat:@"tf_shoe_%02d.png", i]];
			} 
			id actionshoes = [CCAnimate actionWithAnimation:p_shoess restoreOriginalFrame:NO];
			[p_tf3 runAction:[CCSequence actions:actionshoes,[CCCallFunc actionWithTarget:self selector:@selector(shoeparticle:)],nil]];
		}
			break;
	
		case 1:
		{
			CCAnimation *p_shoess = [CCAnimation animationWithName:@"shoe" delay:0.15f];
			for (int i=2; i<6; i++)
			{
				[p_shoess addFrameWithFilename:[NSString stringWithFormat:@"Grandma_tf_shoe_%02d.png", i]];
			} 
			id actionshoes = [CCAnimate actionWithAnimation:p_shoess restoreOriginalFrame:NO];
			[p_tf3 runAction:[CCSequence actions:actionshoes,[CCCallFunc actionWithTarget:self selector:@selector(shoeparticle:)],nil]];
		}
			break;
			
		case 2:
		{
			CCAnimation *p_shoess = [CCAnimation animationWithName:@"shoe" delay:0.15f];
			for (int i=2; i<6; i++)
			{
				[p_shoess addFrameWithFilename:[NSString stringWithFormat:@"Rock_tf_shoe_%02d.png", i]];
			} 
			id actionshoes = [CCAnimate actionWithAnimation:p_shoess restoreOriginalFrame:NO];
			[p_tf3 runAction:[CCSequence actions:actionshoes,[CCCallFunc actionWithTarget:self selector:@selector(shoeparticle:)],nil]];
		}
			break;	
		
		case 3:
		{
			CCAnimation *p_shoess = [CCAnimation animationWithName:@"shoe" delay:0.15f];
			for (int i=2; i<6; i++)
			{
				[p_shoess addFrameWithFilename:[NSString stringWithFormat:@"Doc_tf_shoe_%02d.png", i]];
			} 
			id actionshoes = [CCAnimate actionWithAnimation:p_shoess restoreOriginalFrame:NO];
			[p_tf3 runAction:[CCSequence actions:actionshoes,[CCCallFunc actionWithTarget:self selector:@selector(shoeparticle:)],nil]];
		}
			break;		
			
		default:
			break;
	}
	

	man_vel.y = 350.0f + fabsf(man_vel.x);
	if (isPad)
	{
		man_vel.y = 700.0f + fabsf(man_vel.x)/2;
		
	}
	
	

}
- (void)shoeparticle:(id)sender
{
	
	p_tf3.visible = NO;
	p_man.visible = YES;
	
}

#pragma mark -
- (void)spriteDone:(id)sender
{
	CCSprite *sprite = (CCSprite *)sender;
	sprite.visible = NO;
	[p_node_0 removeChild:sprite cleanup:YES];
	
}

- (void)spriteDone1:(id)sender
{
	CCSprite *sprite = (CCSprite *)sender;
	sprite.visible = NO;
	[p_node_1 removeChild:sprite cleanup:YES];
	
}

- (void)spriteDone2:(id)sender
{

	for (int i = 0; i < m_lFallParCount; i++) {
		CCSprite *p = nil;
		p = (CCSprite*)[self getChildByTag:MIN_FALL_INDEX+i];
		if (nil != p) {
			p.visible = NO;
			[self removeChild:p cleanup:YES];	
		}
	}
}

#pragma mark -
- (void)reloadEasy1Prop:(int)count//e
{

	int a = easyjudge;
	while (true)
	{
		easyjudge = arc4random()%8;
		if (a != easyjudge) break;
	}
	
	

	switch (easyjudge)
	{
		case 0:
			[self Type26:count];
			break;
		case 1:
			[self Type25:count];
			break;
		case 2:
			[self Type11:count];
			break;
		case 3:
			[self Type10:count];
			break;
		case 4:
			[self Type9:count];
			break;
		case 5:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type6:count:(int)haha];
		}
 
			break;
		case 6:
			[self Type8:count];
			break;
		case 7:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type3:count:(int)haha];
		}
			 
			break;
		
		default:
			break;
	}

}
- (void)reloadEasy11Prop:(int)count//e
{

	
	int a = easyjudge;
	while (true)
	{
		easyjudge = arc4random()%8;
		if (a != easyjudge) break;
	}
	
	
	switch (easyjudge)
	{

		case 0:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type4:count:(int)haha];
		}
 			break;
		case 1:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type5:count:(int)haha];
		}
 			break;
		case 2:
			[self initShape4:count];
			break;
		case 3:
			[self initShape2:count];
			break;
		case 4:
			[self initShape6:count];
			break;
		case 5:
			[self initShape8:count];
			break;
		case 6:
			[self initShape1:count];
			break;
		case 7:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self initbigNormalmuch:count:(int)haha:m_state];
		}
 			break;
		default:
			break;
	}
}

- (void)reloadMedium1Prop:(int)count//e
{
	
	int a = mediumjudge;
	while (true)
	{
		mediumjudge = arc4random()%5;
		if (a != mediumjudge) break;
	}


	switch (mediumjudge)
	{
		case 0:
			[self Fun1:count];
			break;
		case 1:
			[self Fun2:count];
			break;
		case 2:
			[self Fun3:count];
			break;
		case 3:
			[self Fun4:count];
			break;
		case 4:
			[self Fun5:count];
			break;
	

		default:
		break;
	}			

}

- (void)reloadMedium11Prop:(int)count
{
	int a = mediumjudge;
	while (true)
	{
		mediumjudge = arc4random()%6;
		if (a != mediumjudge) break;
	}
	
	switch (mediumjudge)
	{

		case 0:
			[self Fun6:count];
			break;
		case 1:
			[self Fun7:count];
			break;
		case 2:
			[self Fun8:count];
			break;
		case 3:
			[self Fun9:count];
			break;
		case 4:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self initsmallNormalless:count:(int)haha:m_state];
		}
 			break;
		case 5:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self initmixedNormalmuch:count:(int)haha:m_state];
		}
 			break;
		default:
			break;
	}
}

- (void)reloadHard1Prop:(int)count//e
{
	switch (continuousjudge1)
	{
			
		case 1:
		{
			[self A1:count];
			
		}
			break;
		case 2:
		{
			[self A2:count];
			
		}
			break;
		case 3:
		{
			[self A3:count];
			
		}
			break;
		case 4:
		{
			[self A4:count];
			
		}
			break;
		case 5:
		{
			[self A5:count];
			
		}
			break;
		case 6:
		{
			[self A6:count];
			
		}
			break;
		case 7:
		{
			[self A7:count];
			
		}
			break;
		case 8:
		{
			[self A8:count];
			
		}
			break;
		default:
			break;
	}
	
}			 



#pragma mark -
- (void)reloadEasy2Prop:(int)count//e
{

	
	int a = easyjudge;
	while (true)
	{
		easyjudge = arc4random()%6;
		if (a != easyjudge) break;
	}
	
	switch (easyjudge)
	{
		case 0:
			[self Type19:count];
			break;
		case 1:
			[self Type12:count];
			break;
		case 2:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type1:count:(int)haha ];
		}
 			break;
		case 3:
			[self initShape11:count];;
			break;
		case 4:
			[self initmovedNormabig:count:m_state];
			break;
		case 5:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self initbigNormalless:count :(int)haha :m_state];
		}
			
 			break;
		default:
			break;
	}
	
	
	
}

- (void)reloadMedium2Prop:(int)count//
{
	

	int a = mediumjudge;
	while (true)
	{
		mediumjudge = arc4random()%2;
		if (a != mediumjudge) break;
	}
	switch (mediumjudge)
	{
		case 0:
			[self initShape10:count];
			break;
		case 1:
			[self initShape7:count];
			break;

	}			
	
}
- (void)reloadcontinuous2Prop:(int)count//e
{
   	switch (continuousjudge2)
	{
		
		case 1:
		{
			[self A13:count];
			
		}
			break;
		case 2:
		{
			[self A14:count];
			
		}
			break;
		case 3:
		{
			[self A15:count];
			
		}
			break;
		case 4:
		{
			[self A16:count];
			
		}
			break;
		case 5:
		{
			[self A17:count];
			
		}
			break;
		case 6:
		{
			[self A18:count];
			
		}
			break;
		case 7:
		{
			[self A19:count];
			
		}
			break;
		case 8:
		{
			[self A20:count];
			
		}
			break;
		default:
			break;
	}

}

- (void)reloadHard2Prop:(int)count//e
{

	int a = hardjudge;
	while (true)
	{
		hardjudge = arc4random()%6;
		if (a != hardjudge) break;
	}
	


	switch (hardjudge)
	{
		case 0:
			[self initShape9:count];
			break;
		case 1:
			[self initShape10:count];
			break;
		case 2:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self initsmallNormalless:count :(int)haha  :m_state];
		}
 			break;
		case 3:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self initmixedNormalless:count :(int)haha:m_state];
		}
 			break;
		case 4:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type2:count:(int)haha ];
		}
 			break;
		case 5:
			[self Type15:count];
			break;

		default:
			break;
	}	
	
}

#pragma mark -
- (void)reloadEasy3Prop:(int)count//e
{
	
	int a = easyjudge;
	while (true)
	{
		easyjudge = arc4random()%2;
		if (a != easyjudge) break;
	}
	

	
	switch (easyjudge)
	{

		case 0:
		{
			int haha = m_hightscore/5;
			if (isPad)
			{
				haha = haha/2;
			}
			[self Type1:count:(int)haha ];
		}
 			break;
		case 1:
			[self initShape11:count];;
			break;
			
		default:
			break;
	}

}
- (void)reloadMedium3Prop:(int)count//e
{

	int a = mediumjudge;
	while (true)
	{
		mediumjudge = arc4random()%2;
		if (a != mediumjudge) break;
	}
	


	switch (mediumjudge)
	{
		case 0:
			[self initShape10:count];
			break;
		case 1:
			[self initShape7:count];
			break;
			
	}			
	
}


- (void)reloadcontinuous3Prop:(int)count//e
{
   	switch (continuousjudge3)
	{
			
		case 1:
		{
			[self A9:count];
			
		}
			break;
		case 2:
		{
			[self A10:count];
			
		}
			break;
		case 3:
		{
			[self A11:count];
			
		}
			break;
		case 4:
		{
			[self A12:count];
			
		}
			break;

		default:
			break;
	}
	
}


- (void)reloadHard3Prop:(int)count//e
{
	int a = hardjudge;
	while (true)
	{
		hardjudge = arc4random()%4;
		if (a != hardjudge) break;
	}
	


	switch (hardjudge)
	{
		case 0:
			[self Type16:count];
			break;
		case 1:
			[self Type17:count :m_state];
			break;
		case 2:
			[self Type23:count :m_state];
			break;
		case 3:
			[self Type24:count :m_state];
			break;

		default:
			break;
	}	
	
}

- (void)reloadmediumRocketProp:(int)count
{
	switch (rocketcount1)
	{
		case 0:
			[self rocket9:count:rocketjudge];
			break;
		case 1:
			[self rocket10:count:rocketjudge];
			break;
		case 2:
			[self rocket11:count:rocketjudge];
			break;
		case 3:
			[self rocket12:count:rocketjudge];
			break;
		case 4:
			[self rocket13:count:rocketjudge];
			break;
		case 5:
			[self rocket14:count:rocketjudge];
			break;
		case 6:
			[self rocket19:count:rocketjudge];
			break;
		case 7:
			[self rocket20:count:rocketjudge];
			break;
		case 8:
			[self rocket21:count:rocketjudge];
			break;
		case 9:
			[self rocket22:count:rocketjudge];
			break;
		case 10:
			[self rocket23:count:rocketjudge];
			break;
		case 11:
			[self rocket24:count:rocketjudge];
			break;
		case 12:
			[self rocket25:count:rocketjudge];
			break;
		case 13:
			[self rocket29:count:rocketjudge];
			break;
		case 14:
			[self rocket30:count:rocketjudge];
			break;
		case 15:
			[self rocket28:count:rocketjudge];
			break;
			
		default:
			break;
	}
	
}

- (void)reloadhardRocketProp:(int)count//e
{
   	switch (rocketcount2)
	{
		case 1:
			[self rocket15:count:rocketjudge];
			break;
		case 2:
			[self rocket16:count:rocketjudge];
			break;
		case 3:
			[self rocket17:count:rocketjudge];
			break;
		case 4:
			[self rocket18:count:rocketjudge];
			break;
			
		case 5:
			[self rocket26:count:rocketjudge];
			break;
		case 6:
			[self rocket27:count:rocketjudge];
			break;
			
		case 7:
			[self rocket7:count:rocketjudge];
			break;
		case 8:
			[self rocket8:count:rocketjudge];
			break;
		default:
			break;
	}
	
}


#pragma mark -
- (void)reloadeasy9block:(int)count//e
{
	switch (blockscount1)
	{
		case 0:
			[self ABlock2:count];
			break;
		case 1:
			[self ABlock3:count];
			break;
		case 2:
			[self ABlock4:count];
			break;
		case 3:
			[self ABlock5:count];
			break;
		case 4:
			[self ABlock6:count];
			break;
		case 5:
			[self ABlock7:count];
			break;
		case 6:
			[self ABlock8:count];
			break;
		case 7:
			[self ABlock9:count];
			break;
		case 8:
			[self ABlock18:count];
			break;
		case 9:
			[self Fun2:count];
			break;
		default:
			break;
	}

}
- (void)reloadmedium9block:(int)count
{

	switch (blockscount2)
	{
		case 0:
			[self Fun4:count];
			break;
		case 1:
			[self Fun5:count];
			break;
		case 2:
			[self ABlock12:count];
			break;
		case 3:
			[self ABlock13:count];
			break;
		case 4:
			[self ABlock14:count];
			break;
		case 5:
			[self ABlock15:count];
			break;
		case 6:
			[self ABlock16:count];
			break;
		case 7:
			[self ABlock17:count];
			break;
	
		default:
			break;
	}

}

- (void)reloadhard9block:(int)count
{
	switch (blockscount3)
	{
		case 0:
			[self Fun3:count];
			break;
		case 1:
			[self ABlock21:count];
			break;
		case 2:
			[self ABlock22:count];
			break;
		case 3:
			[self ABlock23:count];
			break;
		case 4:
			[self ABlock24:count];
			break;
		case 5:
			[self ABlock1:count];
			break;
	
		default:
			break;
	}

}

#pragma mark -
- (void)reloadeasy5rain:(int)count
{
    switch (rainscount1)
	{
		case 0:
			[self clouds6:count];
			break;
		case 1:
			[self clouds7:count];
			break;
		case 2:
			[self clouds8:count];
			break;
		case 3:
			[self clouds19:count];
			break;
		case 4:
			[self clouds20:count];
			break;
		case 5:
			[self clouds21:count];
			break;
		case 6:
			[self clouds22:count];
			break;
		default:
			break;
	}

}
- (void)reloadmedium5rain:(int)count
{
	switch (rainscount2)
	{
		case 0:
			[self clouds12:count];
			break;
		case 1:
			[self clouds13:count];
			break;
		case 2:
			[self Fun6:count];
			break;
		case 3:
			[self clouds15:count];
			break;
		case 4:
			[self clouds16:count];
			break;
		case 5:
			[self clouds17:count];
			break;
		case 6:
			[self clouds18:count];
			break;
		case 7:
			[self clouds9:count];
			break;
		case 8:
			[self clouds10:count];
			break;
		case 9:
			[self clouds11:count];
			break;
		default:
			break;
	}


}



- (void)reloahard5rain:(int)count
{
   	switch (rainscount3)
	{
		case 0:
			[self Fun7:count];
			break;

		default:
			break;
	}

}
#pragma mark -
- (void)reloadGhost:(int)count
{
   ghostsjudge = arc4random()%6;
   switch (ghostsjudge)
	{
	    case 0:
			[self Ghost1:count];
			break;
		case 1:
			[self Ghost2:count];
			break;
		case 2:
			[self Ghost3:count];
			break;
		case 3:
			[self Ghost4:count];
			break;
		case 4:
			[self Ghost5:count];
			break;
		case 5:
			[self Ghost6:count];
			break;
	   default:
		   break;
   }
}

#pragma mark -
- (void)reloadeasy6:(int)count
{
	
	int a = easyjudge;
	while (true)
	{
		easyjudge = arc4random()%4;
		if (a != easyjudge) break;
	}
	
 
	rocketjudge = 4;
	switch (easyjudge)
	{
			

		case 0:
			[self Blink4:count];
			break;
		case 1:
			[self Blink5:count];
			break;	
			
		case 2:
			[self Fun1:count];
			break;
		case 3:
			[self Fun9:count];
			break;		
		default:
			break;
	}

}
- (void)reloadmedium6:(int)count
{
   switch (blinkcount2) 
	{
			
		case 0:
		{
			[self Blink14:count];
		}
			break;	
		case 1:
		{
			[self Blink15:count];
		}
		   break;
		case 2:
		{
			[self Blink16:count];
		}
			break;
		case 3:
		{
			[self Blink17:count];
		}
			break;
	
	   default:
		   break;
   }

}
- (void)reloadhard6:(int)count
{
    switch (blinkcount3) 
	{
			
		case 0:
		{
			[self Blink12:count];
		}
			break;	
		case 1:
		{
			[self Blink13:count];
		}
			break;
		case 2:
		{
			[self Blink10:count];
		}
			break;
		case 3:
		{
			[self Blink11:count];
		}
			break;
			
		default:
			break;
	}
}

#pragma mark -
- (void)judgenext:(int)count 
{
	int height = (int)m_hightscore/5;
 	if (isPad)
	{
		height = height/2;
	}
	
	int judgenum = arc4random()%100;
	
	if (!gameEndless)
	{
		switch (m_state)
		{
		   case 1:
			{
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <50&& !continuous1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=50 && judgenum <100&& !continuous1)
					{
						[self reloadEasy11Prop:count];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <25&& !continuous1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=25 && judgenum <50&& !continuous1)
					{
						[self reloadMedium1Prop:count];
					}	
					if (judgenum>=50 && judgenum <75&& !continuous1)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=75 && judgenum <100&& !continuous1)
					{
						[self reloadHard1Prop:count];
					}	
				}
				
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <40&& !continuous1)
					{
						[self reloadMedium1Prop:count];
					}
					if (judgenum>=40 && judgenum <60&& !continuous1)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=60 && judgenum <80&& !continuous1)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=80 && judgenum <100&& !continuous1)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
	
								[self reloadHard1Prop:count];
								continuousjudge1--;
	
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
				}
			
			}
			   break;

			case 2:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <33&& !continuous1 && !continuous2)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=33 && judgenum <70&& !continuous1 && !continuous2)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=70 && judgenum <100&& !continuous1 && !continuous2)
					{
						[self reloadEasy2Prop:count];
					}
					
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2)
					{
						[self reloadMedium1Prop:count];
					}
					if (judgenum>=60 && judgenum <80&& !continuous1 && !continuous2)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=80 && judgenum <85&& !continuous1 && !continuous2)
					{
						[self reloadMedium2Prop:count];
					}
					
					
					if (judgenum>=85 && judgenum <100&& !continuous1 && !continuous2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
							
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}

								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <10&& !continuous1 && !continuous2)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=20 && judgenum <30&& !continuous1 && !continuous2)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2)
					{
						[self reloadMedium1Prop:count];
					}
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2)
					{
						[self reloadMedium11Prop:count];
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2)
					{
						[self reloadMedium2Prop:count];
					}
					
					
					
					if (judgenum>=60 && judgenum <80&& !continuous1 && !continuous2)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
					
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2)
					{
						
						[self reloadHard2Prop:count];

					}
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
				
				}
				
				
			}
				break;
		
			case 3:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=25 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=75 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium11Prop:count];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=25 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium11Prop:count];
					}
					
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium3Prop:count];
					}
					
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						rockets1 = YES;
						totalcount = arc4random()%6+6;
						rocketcount1 = arc4random()%16;
						rocketjudge = 0;
					}
					
		
					if (judgenum>=75 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=85 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (rockets1) 
					{
						switch (rocketcount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 8 && rocketcount1 != 14) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								if (rocketcount1 == 3 && rocketcount1 == 9 && rocketcount1 == 11 && rocketcount1 == 7 && rocketcount1 == 13) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets1 = NO;
							rocketjudge = 0;
						}
					}
					
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
		
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
	
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <5&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy1Prop:count];//e
					}
					if (judgenum>=5 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy2Prop:count];//e
					}
					
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy3Prop:count];//e
					}
					
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=25 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium2Prop:count];//e
					}
					
					if (judgenum>=30 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium3Prop:count];//e
					}
					
					
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;//e
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						rockets1 = YES;//e
						totalcount = arc4random()%6+6;
						rocketcount1 = arc4random()%16;
						rocketjudge = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						rockets2 = YES;
						totalcount = arc4random()%6+6;
						rocketcount2 = arc4random()%8+1;
						rocketjudge = 0;
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						continuous3 = YES;//e
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					
					}
	
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
						
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2--;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (rockets2) 
					{
						switch (rocketcount2%2)
						{
							case 0:
							{
								if (rocketcount2 != 2) 
								{
									[self reloadhardRocketProp:count];
									rocketcount2++; 
								}
								
								else
								{
									[self reloadhardRocketProp:count];
									rocketcount2 = arc4random()%8+1;
								}

								
							}
								break;
							case 1:
							{

								[self reloadhardRocketProp:count];
								rocketcount2++; 
	
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets2 = NO;
							rocketjudge = 0;
						}
					}
					
					if (rockets1) 
					{
						switch (rocketcount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 8 && rocketcount1 != 14) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								if (rocketcount1 == 3 && rocketcount1 == 9 && rocketcount1 == 11 && rocketcount1 == 7 && rocketcount1 == 13) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets1 = NO;
							rocketjudge = 0;
						}
					}
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
						
						}
					}
					
				}
				
				
			}
				break;
				
				
			case 4:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <25&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=25 && judgenum <50&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=75 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <5&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=5 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=20 && judgenum <30&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadMedium11Prop:count];
					}
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadMedium1Prop:count];
					}
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=60 && judgenum <65&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=65 && judgenum <70&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium3Prop:count];
					}
					
					if (judgenum>=70 && judgenum <75&& !continuous1 && !continuous2&& !continuous3)
					{
						[self initmoveNormalsmall:count:m_state];
					}
					
					
					if (judgenum>=75 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 )
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}

					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <5&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=5 && judgenum <10&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 )
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=25 && judgenum <30&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium3Prop:count];
					}
					
					
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3)
					{
						int haha = m_hightscore/5;
						if (isPad)
						{
							haha = haha/2;
						}
						[self initmixedNormalmuch:count:(int)haha :m_state];
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3)
					{
						int haha = m_hightscore/5;
						if (isPad)
						{
							haha = haha/2;
						}
						[self initmixedNormalless:count:(int)haha :m_state];
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 )
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}

						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
	
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					

					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
		
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
				
			}
				break;
				
				
			case 5:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=60 && judgenum <80&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadMedium2Prop:count];
					}
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						[self reloadMedium3Prop:count];
					}
					
					if (judgenum>=70 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					
					if (judgenum>=85 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rains1)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rainscount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rainscount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rainscount1++; 
									
								}
		
								
							}
								break;
							case 1:
							{

								[self reloadeasy5rain:count];
								rainscount1++; 
		
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}

						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}

						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <5&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=5 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=10 && judgenum <15&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=15 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=25 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=30 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadMedium3Prop:count];
					}
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						[self reloadMedium11Prop:count];
					}
					
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						rains2 = YES;
						totalcount = arc4random()%6;
						rainscount2 = arc4random()%10;
						rainsjudge = 0;
					}
					
					if (judgenum>=70 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=75 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !rains1 && !rains2)
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rainscount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rainscount1++; 
									
								}
			
							}
								break;
							case 1:
							{
								
								[self reloadeasy5rain:count];
								rainscount1++; 
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					if (rains2) 
					{
						switch (rainscount2%2)
						{
							case 0:
							{
								if (rainscount2 != 0 || rainscount2 != 8) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
					
								
							}
								break;
							case 1:
							{
								
								if (rainscount2 == 9) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
			
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains2 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
		
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
		
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}

						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
				
			}
				break;
			case 6:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=60 && judgenum <80&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadeasy6:count];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}

					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=25 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadeasy6:count];
					}
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rainscount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rainscount1++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								[self reloadeasy5rain:count];
								rainscount1++; 
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
												continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
												continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2 && !blinks2 && !blinks3)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}

					
					if (judgenum>=20 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						blinks3 = YES;
						totalcount = arc4random()%8;
						blinkcount3 = arc4random()%4;
						blinkjudge = 0;
					}

					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						[self reloadEasy1Prop:count];
					}
					
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						rains2 = YES;
						totalcount = arc4random()%6;
						rainscount2 = arc4random()%10;
						rainsjudge = 0;
					}
					
					if (judgenum>=70 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=75 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
							continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !rains1 && !rains2&& !blinks2 && !blinks3 )
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (blinks3) 
					{
						switch (blinkcount3%2)
						{
							case 0:
							{

								[self reloadhard6:count];
								blinkcount3++; 
					
							}
								break;
							case 1:
							{

								[self reloadhard6:count];
								blinkcount3 = arc4random()%4;
		
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks3 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}

								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rainscount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rainscount1++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								[self reloadeasy5rain:count];
								rainscount1++; 
								
								
							}
								break;
							default:
								break;
						}
						
						rainscount1++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					if (rains2) 
					{
						switch (rainscount2%2)
						{
							case 0:
							{
								if (rainscount2 != 0 || rainscount2 != 8) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (rocketcount2 == 9) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains2 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
												continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
												continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
												continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
				
			}
				break;
				
				
			case 7:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <15&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=15 && judgenum <30&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=30 && judgenum <45&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=75 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						int haha = m_hightscore/5;
						if (isPad)
						{
							haha = haha/2;
						}
						[self initsmallNormalmuch:count:(int)haha:m_state];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadEasy11Prop:count];
					}
					
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadMedium3Prop:count];
					}
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=70 && judgenum <75&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
							[self reloadEasy1Prop:count];
					}
					
					
					if (judgenum>=75 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}

					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					
					if (judgenum>=20 && judgenum <30&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						blinks3 = YES;
						totalcount = arc4random()%8;
						blinkcount3 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=30 && judgenum <35&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						[self reloadMedium3Prop:count];
					}
					
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						[self reloadMedium11Prop:count];
					}
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=50 && judgenum <55&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
							[self reloadEasy2Prop:count];
					}
					if (judgenum>=55 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=60 && judgenum <65&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=65 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=70 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=75 && judgenum <80&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3 )
					{
						[self reloadMedium2Prop:count];
					}
					
					
					if (blinks3) 
					{
						switch (blinkcount3%2)
						{
							case 0:
							{
								
								[self reloadhard6:count];
								blinkcount3++; 
								
							}
								break;
							case 1:
							{
								
								[self reloadhard6:count];
								blinkcount3 = arc4random()%4;
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks3 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}


					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
				
			}
				break;
				
			case 8:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=70 && judgenum <90&& !continuous1 && !continuous2&& !continuous3)
					{
						int haha = m_hightscore/5;
						if (isPad)
						{
							haha = haha/2;
						}
						[self initsmallNormalmuch:count:(int)haha:m_state];
					}
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium11Prop:count];
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=20 && judgenum <30&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=50 && judgenum <70&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadGhost:count];
					}
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadEasy1Prop:count];
					}
					
					if (judgenum>=80 && judgenum <85&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=85 && judgenum <95&& !continuous1 && !continuous2&& !continuous3 && !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=95 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					
					if (judgenum>=20 && judgenum <30&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						blinks3 = YES;
						totalcount = arc4random()%8;
						blinkcount3 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						[self reloadMedium3Prop:count];
					}
					
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						[self reloadGhost:count];;
					}

					if (judgenum>=60 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=75 && judgenum <80&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !blinks2 && !blinks3)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !blinks2 && !blinks3 )
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (blinks3) 
					{
						switch (blinkcount3%2)
						{
							case 0:
							{
								
								[self reloadhard6:count];
								blinkcount3++; 
								
							}
								break;
							case 1:
							{
								
								[self reloadhard6:count];
								blinkcount3 = arc4random()%4;
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks3 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					
					
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
						}
					}
					
				}
				
				
			}
				break;
			case 9:
			{
				
				if(height>=0 && height<2000)
				{
					if (judgenum>=0 && judgenum <25&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=25 && judgenum <50&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
					}
					if (judgenum>=75 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadHard2Prop:count];
					}
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{
								
								[self reloadeasy9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}
				}
				
				if(height>=2000 && height<4000)
				{
					if (judgenum>=0 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
					}
					
					if (judgenum>=30 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks2 = YES;
						totalcount = arc4random()%8;
						blockscount2 = arc4random()%8;
						blocksjudge = 0;
						
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadMedium3Prop:count];
					}
					
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (blocks2) 
					{
						switch (blockscount2%2)
						{
							case 0:
							{
								
								[self reloadmedium9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
		
									[self reloadmedium9block:count];
									blockscount2 = arc4random()%8;

							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks2 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{

									[self reloadeasy9block:count];
									blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}

					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
						}
					}
					
				}
				if(height>=4000 && height<6000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
						
					}
					
					
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks2 = YES;
						totalcount = arc4random()%8;
						blockscount2 = arc4random()%8;
						blocksjudge = 0;
						
					}
					
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy2Prop:count];
					}
					
					
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks3 = YES;
						totalcount = arc4random()%8;
						blockscount3 = arc4random()%6;
						blocksjudge = 0;
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy1Prop:count];
					}
					
					if (judgenum>=70 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=75 && judgenum <80&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
					    [self reloadMedium1Prop:count];
					}
					


					
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{
								
								[self reloadeasy9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (blocks2) 
					{
						switch (blockscount2%2)
						{
							case 0:
							{
								
								[self reloadmedium9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadmedium9block:count];
								blockscount2 = arc4random()%8;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks2 = NO;
							blocksjudge = 0;
						}
					}
					
					if (blocks3) 
					{
						switch (blockscount3%2)
						{
							case 0:
							{
								
								[self reloadhard9block:count];
								blockscount3++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadhard9block:count];
								blockscount3 = arc4random()%6;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks3 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
				
			}
				break;	
		}
	
	
	} 
	else
	{ 
		switch (m_state)
		{
			case 10:
			{ 
				if(height>=0 && height<4000)
				{
					if (judgenum>=0 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=25 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=75 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy3Prop:count];
					}
				} 
				
				if(height>=4000 && height<8000)
				{
					if (judgenum>=0 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=25 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=30 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium11Prop:count];
					}
					
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						[self reloadMedium3Prop:count];
					}
					
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						rockets1 = YES;
						totalcount = arc4random()%6+6;
						rocketcount1 = arc4random()%16;
						rocketjudge = 0;
					}
					
					
					if (judgenum>=75 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=85 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1)
					{
						totalcount = arc4random()%6;
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (rockets1) 
					{
						switch (rocketcount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 8 && rocketcount1 != 14) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								if (rocketcount1 == 3 && rocketcount1 == 9 && rocketcount1 == 11 && rocketcount1 == 7 && rocketcount1 == 13) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets1 = NO;
							rocketjudge = 0;
						}
					}
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				} 
				if(height>=8000 && height<16000)
				{
					if (judgenum>=0 && judgenum <15&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy1Prop:count]; 
					}
					if (judgenum>=15 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy2Prop:count]; 
					}
					
					if (judgenum>=25 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy3Prop:count]; 
					}
					
					if (judgenum>=35 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium1Prop:count]; 
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium2Prop:count]; 
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium3Prop:count]; 
					}
					
					
					if (judgenum>=60 && judgenum <65&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;//e
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0; 
					}
					
					if (judgenum>=65 && judgenum <73&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						rockets1 = YES;
						totalcount = arc4random()%6+4;
						rocketcount1 = arc4random()%16;
						rocketjudge = 0; 
					}
					
					if (judgenum>=73 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						rockets2 = YES;
						totalcount = arc4random()%6+6;
						rocketcount2 = arc4random()%8+1;
						rocketjudge = 0; 
					}
					
					if (judgenum>=80 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;//e
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0; 
					}
					
					
					if (judgenum>=85 && judgenum <92&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0; 
					}
					
					if (judgenum>=92 && judgenum <98&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadHard2Prop:count]; 
					}
					
					if (judgenum>=98 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadHard3Prop:count]; 
					}
					
					
					if (continuous3)
					{ 
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--; 
							}
								break; 
							case 1: 
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;   
							}
								break; 
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0; 
						} 
					}
					
					if (continuous2)
					{ 
						switch (continuousjudge2%2)
						{
							case 0:
							{ 
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								} 
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}  
							}
								break;
								
							case 1:
							{ 
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0; 
						}
						
					}
					
					if (rockets2) 
					{
						switch (rocketcount2%2)
						{
							case 0:
							{
								if (rocketcount2 != 2) 
								{
									[self reloadhardRocketProp:count];
									rocketcount2++; 
								} 
								else
								{
									[self reloadhardRocketProp:count];
									rocketcount2 = arc4random()%8+1;
								} 
							}
								break;
							case 1:
							{
								
								[self reloadhardRocketProp:count];
								rocketcount2++;  
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets2 = NO;
							rocketjudge = 0;
						}
						
					}
					
					if (rockets1) 
					{
						switch (rocketcount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 8 && rocketcount1 != 14) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								if (rocketcount1 == 3 && rocketcount1 == 9 && rocketcount1 == 11 && rocketcount1 == 7 && rocketcount1 == 13) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets1 = NO;
							rocketjudge = 0;
						}
						
					}
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0; 
						} 
					} 
				}  
				if(height>=16000)
				{
					if (judgenum>=0 && judgenum <5&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=5 && judgenum <10&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy2Prop:count];
					}
					
					if (judgenum>=10 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=25 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=30 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadMedium3Prop:count];
					}
					
					
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						rockets1 = YES;
						totalcount = arc4random()%6+4;
						rocketcount1 = arc4random()%16;
						rocketjudge = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						rockets2 = YES;
						totalcount = arc4random()%6+4;
						rocketcount2 = arc4random()%8+1;
						rocketjudge = 0;
					}
					
					if (judgenum>=60 && judgenum <75&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=75 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=85 && judgenum <95&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadHard2Prop:count];
					}
					
					if (judgenum>=95 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rockets1&& !rockets2)
					{
						[self reloadHard3Prop:count];
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (rockets2) 
					{
						switch (rocketcount2%2)
						{
							case 0:
							{
								if (rocketcount2 != 2) 
								{
									[self reloadhardRocketProp:count];
									rocketcount2++; 
								}
								
								else
								{
									[self reloadhardRocketProp:count];
									rocketcount2 = arc4random()%8+1;
								}
								
								
							}
								break;
							case 1:
							{
								
								[self reloadhardRocketProp:count];
								rocketcount2++; 
								
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets2 = NO;
							rocketjudge = 0;
						}
					}
					
					if (rockets1) 
					{
						switch (rocketcount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 8 && rocketcount1 != 14) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								if (rocketcount1 == 3 && rocketcount1 == 9 && rocketcount1 == 11 && rocketcount1 == 7 && rocketcount1 == 13) 
								{
									[self reloadmediumRocketProp:count];
									rocketcount1++; 
								}
								
								else
								{
									[self reloadmediumRocketProp:count];
									rocketcount1 = arc4random()%16;
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rocketjudge++;					
						if (rocketjudge>totalcount)
						{
							totalcount = 0;
							rockets1 = NO;
							rocketjudge = 0;
						}
					}
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
			}
				break;
			case 11:
			{  
				if(height>=0 && height<4000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy11Prop:count];
					}
					if (judgenum>=60 && judgenum <80&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2&& !continuous3)
					{
						[self reloadMedium11Prop:count];
					}
				} 
				 if(height>=4000 && height<8000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=20 && judgenum <25&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadEasy3Prop:count];
					}
					if (judgenum>=25 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadHard3Prop:count];
					}
					if (judgenum>=40 && judgenum <45&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium11Prop:count];
					}
					
					if (judgenum>=45 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium2Prop:count];
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium1Prop:count];
					}
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						[self reloadMedium3Prop:count];
					}
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1&& !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !blinks2)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								if (blinkcount2 == 0) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount2 == 3) 
								{
									[self reloadmedium6:count];
									blinkcount2 = arc4random()%4;
								}
								
								else
								{
									[self reloadmedium6:count];
									blinkcount2++; 
									
								}
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rocketcount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rocketcount1++; 
									
								}
		
							}
								break;
							case 1:
							{
								
								[self reloadeasy5rain:count];
								rocketcount1++; 
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				} 
				
				 if(height>=8000 && height<16000)
				{
					if (judgenum>=0 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2 && !blinks2 && !blinks3)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					
					if (judgenum>=30 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						blinks3 = YES;
						totalcount = arc4random()%8;
						blinkcount3 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=50 && judgenum <55&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						[self reloadHard3Prop:count];
					}
					
					
					if (judgenum>=55 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=60 && judgenum <65&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					if (judgenum>=65 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						rains2 = YES;
						totalcount = arc4random()%6;
						rainscount2 = arc4random()%10;
						rainsjudge = 0;
					}
					
					if (judgenum>=70 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=85 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <96&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=96 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !rains1 && !rains2&& !blinks2 && !blinks3 )
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								
								[self reloadmedium6:count];
								blinkcount2++; 
								
							}
								break;
							case 1:
							{
								
								[self reloadmedium6:count];
								blinkcount2 = arc4random()%4;
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					if (blinks3) 
					{
						switch (blinkcount3%2)
						{
							case 0:
							{
								if (blinkcount3 == 0) 
								{
									[self reloadhard6:count];
									blinkcount3 = arc4random()%4;
								}
								
								else
								{
									[self reloadhard6:count];
									blinkcount3++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount3 == 3) 
								{
									[self reloadhard6:count];
									blinkcount3 = arc4random()%4;
								}
								
								else
								{
									[self reloadhard6:count];
									blinkcount3++; 
									
								}
								
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks3 = NO;
							blinkjudge = 0;
						}
					}

					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rocketcount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rocketcount1++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								[self reloadeasy5rain:count];
								rocketcount1++; 
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					if (rains2) 
					{
						switch (rainscount2%2)
						{
							case 0:
							{
								if (rainscount2 != 0 || rainscount2 != 8) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (rainscount2 == 9) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains2 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				} 
				if(height>=16000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2 && !blinks2 && !blinks3)
					{
						blinks2 = YES;
						totalcount = arc4random()%8;
						blinkcount2 = arc4random()%4;
						blinkjudge = 0;
					}
					
					
					if (judgenum>=20 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						blinks3 = YES;
						totalcount = arc4random()%8;
						blinkcount3 = arc4random()%4;
						blinkjudge = 0;
					}
					
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						[self reloadHard3Prop:count];
					}
					
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=50 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						rains1 = YES;
						totalcount = arc4random()%6;
						rainscount1 = arc4random()%7;
						rainsjudge = 0;
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						rains2 = YES;
						totalcount = arc4random()%6;
						rainscount2 = arc4random()%10;
						rainsjudge = 0;
					}
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=80 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <97&& !continuous1 && !continuous2&& !continuous3 && !rains1 && !rains2&& !blinks2 && !blinks3)
					{
					    [self reloadHard2Prop:count];
					}
					
					if (judgenum>=97 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !rains1 && !rains2&& !blinks2 && !blinks3 )
					{
					    [self reloadHard3Prop:count];
					}
					
					
					if (blinks2) 
					{
						switch (blinkcount2%2)
						{
							case 0:
							{
								
								[self reloadmedium6:count];
								blinkcount2++; 
								
							}
								break;
							case 1:
							{
								
								[self reloadmedium6:count];
								blinkcount2 = arc4random()%4;
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks2 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (blinks3) 
					{
						switch (blinkcount3%2)
						{
							case 0:
							{
								if (blinkcount3 == 0) 
								{
									[self reloadhard6:count];
									blinkcount3 = arc4random()%4;
								}
								
								else
								{
									[self reloadhard6:count];
									blinkcount3++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (blinkcount3 == 3) 
								{
									[self reloadhard6:count];
									blinkcount3 = arc4random()%4;
								}
								
								else
								{
									[self reloadhard6:count];
									blinkcount3++; 
									
								}
								
								
							}
								break;
							default:
								break;
						}
						
						blinkjudge++;					
						if (blinkjudge>totalcount)
						{
							totalcount = 0;
							blinks3 = NO;
							blinkjudge = 0;
						}
					}
					
					
					if (rains1) 
					{
						switch (rainscount1%2)
						{
							case 0:
							{
								if (rocketcount1 != 0 ) 
								{
									[self reloadeasy5rain:count];
									rocketcount1 = arc4random()%7;
								}
								
								else
								{
									[self reloadeasy5rain:count];
									rocketcount1++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								[self reloadeasy5rain:count];
								rocketcount1++; 
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains1 = NO;
							rainsjudge = 0;
						}
					}
					
					if (rains2) 
					{
						switch (rainscount2%2)
						{
							case 0:
							{
								if (rainscount2 != 0 || rainscount2 != 8) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
								
								
								
								
								
							}
								break;
							case 1:
							{
								
								if (rainscount2 == 9) 
								{
									[self reloadmedium5rain:count];
									rainscount2 = arc4random()%10;
								}
								
								else
								{
									[self reloadmedium5rain:count];
									rainscount2++; 
									
								}
								
								
								
								
							}
								break;
							default:
								break;
						}
						
						rainsjudge++;					
						if (rainsjudge>totalcount)
						{
							totalcount = 0;
							rains2 = NO;
							rainsjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				} 
				
			}
				break;
			case 12:
			{ 
				if(height>=0 && height<4000)
				{
					if (judgenum>=0 && judgenum <25&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy1Prop:count];
					}
					if (judgenum>=25 && judgenum <50&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy2Prop:count];
					}
					if (judgenum>=50 && judgenum <75&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
					}
					if (judgenum>=75 && judgenum <100&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadHard2Prop:count];
					}
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{
								
								[self reloadeasy9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}
				} 
				if(height>=4000 && height<8000)
				{
					if (judgenum>=0 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
					}
					
					if (judgenum>=30 && judgenum <35&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy3Prop:count];
					}
					
					if (judgenum>=35 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadMedium1Prop:count];
					}
					
					if (judgenum>=40 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks2 = YES;
						totalcount = arc4random()%8;
						blockscount2 = arc4random()%8;
						blocksjudge = 0;
						
					}
					
					if (judgenum>=60 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadMedium3Prop:count];
					}
					
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
					}
					
					if (judgenum>=80 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (blocks2) 
					{
						switch (blockscount2%2)
						{
							case 0:
							{
								
								[self reloadmedium9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadmedium9block:count];
								blockscount2 = arc4random()%8;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks2 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{
								
								[self reloadeasy9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}
					
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
				} 
				
				if(height>=8000 && height<16000)
				{
					if (judgenum>=0 && judgenum <30&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
						
					}
					
					
					if (judgenum>=30 && judgenum <55&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks2 = YES;
						totalcount = arc4random()%8;
						blockscount2 = arc4random()%8;
						blocksjudge = 0;
						
					}
					
					if (judgenum>=55 && judgenum <60&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy2Prop:count];
					}
					
					
					
					if (judgenum>=60 && judgenum <65&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=65 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks3 = YES;
						totalcount = arc4random()%8;
						blockscount3 = arc4random()%6;
						blocksjudge = 0;
					}
					
					if (judgenum>=70 && judgenum <80&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy1Prop:count];
					}
					
					if (judgenum>=80 && judgenum <85&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=85 && judgenum <90&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=90 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
					    [self reloadMedium1Prop:count];
					}
					
					
					
					
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{
								
								[self reloadeasy9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (blocks2) 
					{
						switch (blockscount2%2)
						{
							case 0:
							{
								
								[self reloadmedium9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadmedium9block:count];
								blockscount2 = arc4random()%8;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks2 = NO;
							blocksjudge = 0;
						}
					}
					
					if (blocks3) 
					{
						switch (blockscount3%2)
						{
							case 0:
							{
								
								[self reloadhard9block:count];
								blockscount3++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadhard9block:count];
								blockscount3 = arc4random()%6;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks3 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				} 
				if(height>=16000)
				{
					if (judgenum>=0 && judgenum <20&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks1 = YES;
						totalcount = arc4random()%8;
						blockscount1 = arc4random()%10;
						blocksjudge = 0;
						
					}
					
					
					if (judgenum>=20 && judgenum <40&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks2 = YES;
						totalcount = arc4random()%8;
						blockscount2 = arc4random()%8;
						blocksjudge = 0;
						
					}
					
					if (judgenum>=40 && judgenum <50&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy2Prop:count];
					}
					
					
					
					if (judgenum>=50 && judgenum <65&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous2 = YES;
						continuousjudge2 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=65 && judgenum <70&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						blocks3 = YES;
						totalcount = arc4random()%8;
						blockscount3 = arc4random()%6;
						blocksjudge = 0;
					}
					
					if (judgenum>=70 && judgenum <85&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						[self reloadEasy1Prop:count];
					}
					
					if (judgenum>=85 && judgenum <90&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous3 = YES;
						continuousjudge3 = arc4random()%4+1;
						continuouscount = 0;
						
					}
					
					
					if (judgenum>=90 && judgenum <98&& !continuous1 && !continuous2&& !continuous3&& !blocks1&& !blocks2&& !blocks3)
					{
						totalcount = arc4random()%6;
						
						continuous1 = YES;
						continuousjudge1 = arc4random()%8+1;
						continuouscount = 0;
					}
					
					if (judgenum>=98 && judgenum <100&& !continuous1 && !continuous2&& !continuous3 && !blocks1&& !blocks2&& !blocks3)
					{
					    [self reloadMedium1Prop:count];
					}
					
					
					
					
					
					if (blocks1) 
					{
						switch (blockscount1%2)
						{
							case 0:
							{
								
								[self reloadeasy9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								if (blockscount1 == 1) 
								{
									[self reloadeasy9block:count];
									blockscount1++; 
									
								}
								
								else
								{
									[self reloadeasy9block:count];
									blockscount1 = arc4random()%10;
									
								}
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks1 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (blocks2) 
					{
						switch (blockscount2%2)
						{
							case 0:
							{
								
								[self reloadmedium9block:count];
								blockscount1++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadmedium9block:count];
								blockscount2 = arc4random()%8;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks2 = NO;
							blocksjudge = 0;
						}
					}
					
					if (blocks3) 
					{
						switch (blockscount3%2)
						{
							case 0:
							{
								
								[self reloadhard9block:count];
								blockscount3++; 										
								
							}
								break;
							case 1:
							{
								
								
								[self reloadhard9block:count];
								blockscount3 = arc4random()%6;
								
							}
								break;
							default:
								break;
						}
						
						blocksjudge++;					
						if (blocksjudge>totalcount)
						{
							totalcount = 0;
							blocks3 = NO;
							blocksjudge = 0;
						}
					}
					
					
					if (continuous3)
					{
						
						
						switch (continuousjudge3%2)
						{
							case 0:
							{
								[self reloadcontinuous3Prop:count];
								continuousjudge3--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous3Prop:count];
								continuousjudge3++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge3 = arc4random()%4+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous3 = NO;
							continuouscount = 0;
							
						}
					}
					
					if (continuous2)
					{
						
						
						switch (continuousjudge2%2)
						{
							case 0:
							{
								
								if (continuousjudge2 == 2 || continuousjudge2 == 6)
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2++; 
								}
								
								
								else
								{
									[self reloadcontinuous2Prop:count];
									continuousjudge2 --;
								}
								
								
							}
								break;
								
							case 1:
							{
								
								[self reloadcontinuous2Prop:count];
								continuousjudge2++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge2 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous2 = NO;
							continuouscount = 0;
							
						}
					}
					
					
					
					if (continuous1)
					{
						
						
						switch (continuousjudge1%2)
						{
							case 0:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1--;
								
							}
								break;
								
							case 1:
							{
								
								[self reloadHard1Prop:count];
								continuousjudge1++;  
								
							}
								break;
								
							default:
								break;
						}
						
						
						
						
						continuouscount++;
						
						if (continuouscount>4)
						{
							continuousjudge1 = arc4random()%8+1;
						}
						
						if (continuouscount>totalcount)
						{
							totalcount = 0;
							continuous1 = NO;
							continuouscount = 0;
							
						}
					}
					
				}
				
			}
				break;
		} 
	}
}

#pragma mark -
- (void)dealloc 
{
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	[super dealloc];
	
}


@end
